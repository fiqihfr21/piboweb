# PiBo Web Frontend Rewrite

## Installation

Clone
```
git clone https://gitlab.com/fiqihfr21/piboweb.git
```

Install node_modules
```
yarn
```

## Run

Development
```
yarn dev
```

## Deployment
This will create `dist` static folder
```
yarn build

# run with serve
yarn start
```
