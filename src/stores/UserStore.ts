import { action, observable, runInAction } from 'mobx';
import client from '../libs/client';

export default class UserStore {
  @observable token: string | null = null;

  @action
  setToken(token: string | null) {
    if (token) {
      this.token = token;
      localStorage.setItem('token', token);
      client.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    }
  }

  @action
  clearToken() {
    this.token = null;
    localStorage.clear();
  }

  @action
  async readTokenFromLocal() {
    try {
      const token: string | null = await localStorage.getItem('token');
      runInAction(() => {
        this.setToken(token);
      });
    } catch (error) {
      console.log(error);
    }
  }
}
