import UserStore from './UserStore';
import ReaderStore from './ReaderStore';
import { configure } from 'mobx';
import CartStore from './CartStore';
import FooterStore from './FooterStore';

configure({ enforceActions: 'observed' });

const stores = {
  userStore: new UserStore(),
  readerStore: new ReaderStore(),
  cartStore: new CartStore(),
  footerStore: new FooterStore(),
};

export default stores;
