import { cartItem } from '../libs/types';
import { observable, action, runInAction } from 'mobx';
import client from '../libs/client';

export default class CartStore {
  @observable items: cartItem[] = [];
  @observable count: number = 0;
  @observable totalPrice: number = 0;
  @observable coupon?: string | null = null;

  @action
  async getCount() {
    try {
      const res = await client.get(`/cart`);
      runInAction(() => {
        this.count = res.data.items.length;
        this.items = res.data.items;
        this.totalPrice = res.data.total_price;
      });
    } catch (error) {
      console.log(error);
    }
  }

  @action
  setCoupon(coupon: string) {
    this.coupon = coupon;
  }

  @action
  async addToCart(id: number) {
    try {
      await client.post(`/cart`, {
        book_id: id,
        quantity: 1,
      });
      this.getCount();
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  @action async updateItem(book_id: number, quantity: number) {
    try {
      const res = await client.put(`/cart`, {
        book_id: book_id,
        quantity: quantity,
      });
      console.log(res.data);
      runInAction(() => {
        this.count = res.data.items.length;
        this.items = res.data.items;
        this.totalPrice = res.data.total_price;
      });
    } catch (error) {
      console.log(error);
    }
  }

  @action async deleteItem(book_id: number) {
    try {
      const res = await client.delete(`/cart`, { data: { book_id: book_id } });
      runInAction(() => {
        this.count = res.data.items.length;
        this.items = res.data.items;
        this.totalPrice = res.data.total_price;
      });
    } catch (error) {
      console.log(error);
    }
  }

  @action async checkVoucher(code: string) {
    try {
      await client.post('/coupon/check', {
        coupon: code,
      });
      runInAction(() => {
        this.coupon = code;
      });
    } catch (error) {
      console.log(error);
    }
  }
}
