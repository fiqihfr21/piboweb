import { action, observable, runInAction } from 'mobx';
import client from '../libs/client';

export interface FooterData {
  deskripsi: string;
  id: number;
  name: string;
  slug: string;
}

export default class FooterStore {
  @observable data: Array<FooterData>;

  @action
  async getFooterData() {
    if (!this.data) {
      try {
        const res = await client.get('/pages');
        runInAction(() => {
          this.data = res.data;
        });
      } catch (error) {
        console.log(error);
      }
    }
  }
}
