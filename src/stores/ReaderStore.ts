import { action, observable } from 'mobx';

export default class ReaderStore {
  @observable active: boolean;
  @observable bookUrl: string;

  @action close() {
    this.active = false;
    this.bookUrl = '';
  }

  @action read(url: string, cb: Function = () => {}) {
    this.active = true;
    this.bookUrl = url + `?bearer=${localStorage.getItem('token')}`;
    cb();
  }
}
