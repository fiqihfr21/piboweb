import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './style/main.scss';
// import UnderMaintenance from './UnderMaintenance';

if (process.env.NODE_ENV === 'production') {
  window.console.log = () => {};
}

ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<UnderMaintenance />, document.getElementById('root'));