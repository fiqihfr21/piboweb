import React from 'react';
import rSwal from '@sweetalert/with-react';
import { book } from './types';

interface TModal {
  book: book;
  onClick: (book: book) => void;
  onWishlist: (book: book) => void;
}

export default function showModal({ book, onClick,onWishlist }: TModal) {
  return rSwal({
    content: (
      <div>
        <div className="close-btn-container">
          <span
            className="close-btn"
            //@ts-ignore
            onClick={() => swal.close()}
          >
            &times;
          </span>
        </div>
        <div className="modal-container">
          <div className="top">
            <div className="img-container">
              <img
                src={
                  book.cover_image
                    ? book.cover_image
                    : require('../assets/placeholder.png')
                }
              />
            </div>
            <div className="detail-container">
              <div className="alert-book-title-container">
                <p className="alert-book-title">{book.name}</p>
              </div>
              {/* <p className="book-description">{book.long_description}</p> */}
              <div
                className="book-description"
                dangerouslySetInnerHTML={{ __html: book.long_description }}
              />
            </div>
          </div>
          <div className="bottom">
            <div className="extra">
              Halaman: {book.page} Halaman
              <br />
              Durasi Baca: {book.read_time} Menit
              <br />
            </div>
            <div className="extra-left">
              Penulis: {book.writer} <br />
              Illustrator: {book.illustrator} <br />
              Penerbit: {book.publisher} <br />
            </div>
            <div className="extra-right">
              
              <div
                id="book1"
                className={ book.wishlist == 1 ? "button-wishlist-active" : "button-wishlist"}
              >
                <img
                  src={require('../assets/navigation/star.png')}
                  width={40}
                  height={40}
                  style={{ cursor: 'pointer'}}
                  onClick={() => {
                    console.log('wishlist');
                    onWishlist(book);
                  }}
                />
              </div>
              <div
                className="button"
                onClick={() => {
                  onClick(book);
                  // @ts-ignore
                  swal.close();
                }}
              >
                BACA
              </div>
            </div>
          </div>
        </div>
      </div>
    ),
    button: false,
    className: 'custom-modal',
  });
}
