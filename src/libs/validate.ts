export const validateStudentLoginInput = (token: string): string => {
    let errors= '';

    if(!token) {
        errors = 'token_murid'
    }

    return errors;
}

export const validateNormalLoginInput = (email: string, password: string): string => {
    let errors= '';
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if (!email && !password) {
        errors = 'both';
    } else if (!password) {
        errors = 'password';
    } else if (!email || !re.test(String(email).toLowerCase())) {
        errors = 'email'
    }
    return errors;
}