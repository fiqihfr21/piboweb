import Axios from 'axios';

const client = Axios.create({
  // baseURL: 'https://dashboard.bacapibo.com/api',
  baseURL: 'https://api.bacapibo.com/api',
  headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
});

export default client;
