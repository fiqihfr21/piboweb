export interface registerInput {
  email: string;
  password: string;
  password_confirmation: string;
  name: string;
}

export interface category {
  id: number;
  name: string;
  image: string;
  slug: string;
}

export interface bookdet {
  id: number;
  name: string;
  long_description: string;
  type: string;
  regular_price: string;
  sales_price: string;
  sku: string;
  stock: number;
  sold: number;
  weight: number;
  length: number;
  width: number;
  height: number;
  writer: string;
  illustrator: string;
  publisher: string;
  isbn: string;
  page: number;
  read_time: number;
  orientation: string;
  trial_persentage: number;
  enable_review: number;
  author_id: number;
  created_at: Date;
  updated_at: Date;
  tags: Array<{
    id: number;
    name: string;
  }>;
  images: Array<{
    id: number;
    image: string;
  }>;
  category: category;
  cover_image: string;
  pdf_file: string;
}

export interface book {
  id: number;
  name: string;
  long_description: string;
  type: string;
  regular_price: string;
  sales_price: string;
  sku: string;
  stock: number;
  sold: number;
  weight: number;
  length: number;
  width: number;
  height: number;
  writer: string;
  illustrator: string;
  publisher: string;
  isbn: string;
  page: number;
  read_time: number;
  orientation: string;
  trial_persentage: number;
  enable_review: number;
  author_id: number;
  wishlist: number;
  created_at: Date;
  updated_at: Date;
  tags: Array<{
    id: number;
    name: string;
  }>;
  category: category;
  cover_image: string;
  pdf_file: string;
}

export interface books {
  current_page: number;
  data: Array<book>;
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url: string;
  to: number;
  total: number;
}

export interface cartItem {
  author_id: number;
  cover_image: string;
  enable_review: boolean;
  height: number;
  id: number;
  illustrator: string;
  isbn: string;
  length: number;
  long_description: string;
  name: string;
  orientation: string;
  page: number;
  publisher: string;
  quantity: number;
  read_time: number;
  regular_price: string;
  sales_price: string;
  short_description: string;
  sku: string;
  slug: string;
  sold: number;
  stock: number;
  trial_percentage: number;
  type: string;
  weight: number;
  width: number;
  writer: string;
}

export interface cart {
  items: Array<cartItem>;
  total_price: number;
}

export interface province {
  id: number;
  name: string;
}

export interface address {
  id: number;
  recipient_name: string;
  address: string;
  province: string;
  regency: string;
  district: string;
  village: string;
  zip_code: string;
  phone_number: string;
  user_id: number;
}

export interface paymentType {
  id: number;
  name: string;
  type: string;
}

export interface shipping {
  weight: number;
  options: Array<{
    code: string;
    name: string;
    costs: Array<{
      service: string;
      description: string;
      cost: Array<{ value: number; etd: string; note: string }>;
    }>;
  }>;
}

export interface order {
  id: number;
  real_price: string;
  coupon_amount: string;
  shipping_cost: string;
  total_price: string;
  payment_type: number;
  transaction_type: string;
  coupon_code: string;
  status: string;
  recipient_name: string;
  address: string;
  province: string;
  regency: string;
  district: string;
  village: string;
  zip_code: string;
  phone_number: string;
  user_id: number;
  created_at: string;
  updated_at: string;
  books: book[];
}

export interface Order {
  current_page: number;
  data: order[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url?: any;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
  subscribe: string;
}

export interface Banner {
  id: number;
  image: string;
  image_mobile: string;
  name: string;
  shop_id: string;
  slug: string;
  status: number;
  urllink: string | null;
  category_id: number;
  age_id: number;
  themes_id: number;
  is_pdf: number;
}

export interface EShopDataType {
  banner: Banner[];
  listbuku: Array<{
    data: string;
    listbuku: book[];
  }>;
  promo: Array<{
    data: {
      id: number;
      name: string;
      shop_id: string;
      slug: string;
      status: number;
    };
    listbuku: book[];
  }>;
}

export interface ELibraryDataType {
  subscribe: number;
  banner: Banner[];
  promo: Array<{
    data: {
      id: number;
      name: string;
      shop_id: string;
      slug: string;
      status: number;
    };
    listbuku: book[];
  }>;
  themes: Array<{
    id: number;
    name: string;
    image: string;
    slug: string;
    status: number;
  }>;
  age: Array<{ id: number; data: string }>;
  age1: Array<{ id: number; data: string }>;
  format: Array<{
    id: number;
    shop_id: string;
    name: string;
    image: string;
    slug: string;
  }>;
}

export interface Subscription {
  id: number;
  slug: string;
  name: string;
  bulan: number;
  regular_price: string;
  price: string;
  promo: string;
  hemat: string;
  desc: object[];
}
