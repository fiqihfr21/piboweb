export const color = {
  blue: '#1F3F72',
  red: '#E72846',
  yellow: '#FAB20B',
  // lightBlue: '#18C2F4',
  lightBlue: '#38c1de',
  darkBlue: '#32ABD0',
  darkYellow: '#D78E00',
  softPink: '#f4f4e6',
  pink: '#e73956',
  shopBackgroundLight: '#FFEF93',
  shopBackgroundDark: '#FEC340',
};
