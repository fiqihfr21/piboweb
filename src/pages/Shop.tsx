import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import '../style/Shop.scss';
import Carousel from '../components/Carousel';
import FetchComponent from '../components/FetchComponent';
import client from '../libs/client';
import { books, category, EShopDataType, book } from '../libs/types';
import { Link } from 'react-router-dom';
import BookCard from '../components/BookCard';
import Slider from '../components/Slider';
import LoadMore from '../components/LoadMore';
const color = require('../style/colors.scss');

interface Props {
  books: books;
  categories: Array<category>;
  data: EShopDataType;
}

class Shop extends Component<Props & RouteComponentProps, { tab: string }> {
  constructor(props) {
    super(props);
    this.state = {
      tab: '',
    };
  }

  componentDidMount() {
    this.setState({ tab: this.props.categories[0].slug });
  }

  render() {
    return (
      <div className="Container">
        <Carousel banners={this.props.data.banner} />
        {this.props.data.promo.map((val, index) => {
          return (
            <div
              className="Section"
              key={val.data.id + val.data.name + index}
              style={{
                background:
                  index % 2 === 0
                    ? color.shopBackgroundLight
                    : color.shopBackgroundDark,
              }}
            >
              <div className="section-child">
                <div className="title-container">
                  <h2 className="title">{val.data.name}</h2>
                  <Link
                    to={`/promo?slug=${val.data.slug}&type=${val.data.shop_id}`}
                  >
                    Lihat Semua
                  </Link>
                </div>
                <Slider
                  data={val.listbuku}
                  renderItem={val =>
                    index % 2 == 0 ? (
                      <BookCard
                        key={val.id + val.name + index}
                        bookId={val.id}
                        image={
                          val.cover_image
                            ? val.cover_image
                            : require('../assets/placeholder.png')
                        }
                        price_1={val.regular_price}
                        price={val.sales_price}
                        title={val.name}
                        type={val.type}
                      />
                    ) : (
                      <div key={val.id + val.name + index} className="Card">
                        <div
                          className="card-content"
                          onClick={() =>
                            this.props.history.push(`/book_detail?id=${val.id}`)
                          }
                        >
                          <img
                            style={{ background: color.red }}
                            src={
                              val.cover_image
                                ? val.cover_image
                                : require('../assets/placeholder.png')
                            }
                          />
                          <div className="detail">
                            <p className="title">{val.name}</p>

                            {val.sales_price && (
                              <p className="price_1">
                                IDR{' '}
                                {parseFloat(val.sales_price).toLocaleString()}
                              </p>
                            )}

                            <p className="price">
                              IDR{' '}
                              {parseFloat(
                                val.sales_price
                                  ? val.sales_price
                                  : val.regular_price
                              ).toLocaleString()}
                            </p>
                          </div>
                        </div>
                        <div className="button-container">
                          <button
                            onClick={async () => {
                              console.log('button click');
                              this.props.history.push(
                                `/book_detail?id=${val.id}`
                              );
                            }}
                          >
                            Lihat Detail
                          </button>
                        </div>
                      </div>
                    )
                  }
                />
              </div>
            </div>
          );
        })}
        <div className="Tab-Section">
          <div className="tab-button-container">
            {this.props.categories.map((val, index) => {
              return (
                <div className="tab-button" key={val.id + val.name + index}>
                  <button
                    key={val.slug + index}
                    className={
                      'tab-button-content' +
                      (this.state.tab == val.slug ? ' active' : '')
                    }
                    onClick={() => this.setState({ tab: val.slug })}
                  >
                    {val.name}
                  </button>
                </div>
              );
            })}
          </div>
          {/* {tabData !== undefined && <AllProduct books={tabData.listbuku} />} */}
          {this.state.tab !== '' && (
            <LoadMore
              url={`/bookeshop/${this.state.tab}`}
              container={props => (
                <div className="Grid">
                  <div className="grid-row">{props.children}</div>
                </div>
              )}
              renderItems={(item: book) => {
                return (
                  <div
                    className="grid-item"
                    key={item.id + item.name + Math.random()}
                  >
                    <BookCard
                      bookId={item.id}
                      image={
                        item.cover_image
                          ? item.cover_image
                          : require('../assets/placeholder.png')
                      }
                      price_1={item.regular_price}
                      price={item.sales_price}
                      title={item.name}
                      type={item.type}
                    />
                  </div>
                );
              }}
            />
          )}
        </div>
      </div>
    );
  }
}

export default props => {
  return (
    <FetchComponent
      doBeforeRender={async () => {
        try {
          const data = client.get('/eshop');
          const categories = client.get('/categories');
          const books = client.get(`/books`);
          const res = await Promise.all([data, categories, books]);
          return {
            data: res[0].data,
            categories: res[1].data,
            books: res[2].data.data,
          };
        } catch (error) {
          throw error;
        }
      }}
    >
      <Shop {...props} />
    </FetchComponent>
  );
};
