import React from 'react';

import '../style/about.scss';

const AboutUsSrc = require("../assets/AboutUs_01.jpg");
const AboutUs_eLibrary = require("../assets/AboutUs_eLibrary.jpg");
const AboutUs_Shop = require("../assets/AboutUs_Shop.jpg");
const Media_logo_01 = require("../assets/media_logo_01.png");
const Media_logo_02 = require("../assets/media_logo_02.png");
const Media_logo_03 = require("../assets/media_logo_03.png");
const Media_logo_04 = require("../assets/media_logo_04.png");
const Media_logo_05 = require("../assets/media_logo_05.png");
const Media_logo_06 = require("../assets/media_logo_06.png");

class About extends React.Component {
  render() {
    return (
      <div>
        <div className="about-cover">
          <div className="about-cover-content">
              <div className="about-cover-content-img">
                <img src="../assets/logo.png"/>
              </div>
              <div className="about-cover-content-text">
                Inspiring Little Readers
              </div>
          </div>
        </div>
        <div className="about-desc">
          <div className="about-desc-container">
            <h1>Membuka Pintu Baru Dunia Baca Anak Indonesia</h1>
            <p>Akrab dengan buku sejak usia dini akan mengasah perkembangan daya imajinasi dan empati, serta mendorong kemampuan anak menganalisa, berpikir kritis, dan tentunya memperkaya kosakatanya agar lebih tajam dalam berkomunikasi. Begitu banyak nilai yang dapat dituai dari sebuah cerita petualangan yang seru ataupun kehangatan yang dirasakan dalam sebuah cerita.</p>
            <div className="about-desc-table">
              <div className="about-desc-table-left">
                <p>
                  <b>PiBo bertujuan menjadikan kegiatan membaca sebagai kegiatan yang menyenangkan.</b>
                </p>
                <p>
                  PiBo hadir atas kesadaran pentingnya minat baca pada anak. Di era digital ini, buku tidak lagi terbatas hanya pada lembaran kertas. Saat gadget ada di tangan anak, PiBo menjadi pilihan edukatif selain bermain games atau menonton video.
                </p>
                <p>
                  Bekerjasama dengan mitra-mitra kami, PiBo ingin memperluas akses terhadap buku-buku anak yang berkualitas bagi anak-anak Indonesia dan senantiasa mengenalkan dunia cerita yang sungguh menyenangkan.
                </p>
                <div className="about-desc-table-left-split">
                  <h2>Dikurasi Secara Profesional</h2>
                  <p>Setiap buku yang ada dalam platform PiBo telah dipilih secara seksama dengan muatan, topik, dan cerita yang relevan untuk anak-anak. Mereka dapat menjelajahi sesuai minat dan level baca dalam lingkungan yang menyenangkan.</p>
                </div>
                <div className="about-desc-table-left-split">
                  <h2>100% Aman untuk Anak</h2>
                  <p>Tidak ada iklan, tidak ada pembelian dalam aplikasi, hanya cukup berlangganan bulanan. PiBo adalah taman bermain yang aman untuk anak-anak di mana mereka tidak dapat mengakses konten dewasa.</p>
                </div>
              </div>
              <div className="about-desc-table-right">
                <img src={AboutUsSrc} />
              </div>
            </div>
          </div>
        </div>
        <div className="about-elibrary">
          <div className="about-elibrary-container">
            <h1>PiBo eLibrary</h1>
            <div className="about-elibrary-container-table">
              <div className="about-elibrary-container-table-left">
                <img src={AboutUs_eLibrary}/>
              </div>
              <div className="about-elibrary-container-table-right">
                <p>PiBo memberi anak-anak di bawah 12 tahun sebuah perpustakaan pribadi yang bisa mereka bawa ke mana saja sebagai alternatif cerdas dari games dan video. PiBo dirancang khusus untuk anak-anak dengan tampilannya yang menarik dan navigasi yang sederhana agar mereka dapat menjelajah secara mandiri.</p>
                <h3>"Perpustakaan Anak di Ujung Jarimu.<br/> Cara membaca baru yang menyenangkan."</h3>
              </div>
            </div>
          </div>
        </div>
        <div className="about-elibrary-list">
          <div className="about-elibrary-list-container">
            <div className="about-elibrary-list-container-box">
                <h1 className="about-elibrary-list-container-box-title">
                  Penjelajahan Mandiri
                </h1>
                <p>
                  Studi menunjukkan pembelajaran mandiri membantu menumbuhkan kecintaan membaca dan belajar pada anak. Inilah sebabnya mengapa PiBo dirancang khusus untuk membantu mendorong anak untuk menemukan dan mengeksplorasi minat pribadi mereka.
                </p>
            </div>
            <div className="about-elibrary-list-container-box">
              <h1 className="about-elibrary-list-container-box-title">
                Bermain Sambil Belajar
              </h1>
              <p>
                Permainan dengan menggunakan reward system, akan meningkatkan minat anak-anak dalam kegiatan membaca. Memiliki kegiatan yang menyenangkan akan mendorong anak-anak untuk terus membaca, menjelajahi, dan belajar.
              </p>
            </div>
            <div className="about-elibrary-list-container-box">
              <h1 className="about-elibrary-list-container-box-title">
                Perpustakaan Tanpa Batas
              </h1>
              <p>
                Perpustakaan kami terus kian bertumbuh dengan ditambahkannya buku-buku anak berkualitas setiap bulan demi untuk terus-menerus menginspirasi rasa ingin tahu anak.
              </p>
            </div>
          </div>
        </div>
        <div className="about-shop">
          <div className="about-shop-container">
            <div className="about-shop-container-table">
              <div className="about-shop-container-table-left">
                <h1 className="about-shop-container-table-left-title">PiBo Shop</h1>
                <p>Melalui PiBo Shop, kami ingin memastikan akses yang lebih luas ke buku-buku dan instrumen pendidikan lainnya. Semua barang yang dijual di toko kami dikurasi untuk membantu meningkatkan proses belajar anak-anak.</p>
              </div>
              <div className="about-shop-container-table-right">
                <img src={AboutUs_Shop} />
              </div>
            </div>
            <div className="about-shop-container-signup">
              <a href="/register">Daftar Sekarang</a>
            </div>
          </div>
        </div>
        <div className="about-footer">
          <div className="about-footer-container">
            <div className="about-footer-container-media">
              <a href="#">
                <img src={Media_logo_01} />
              </a>
              <a href="#">
                <img src={Media_logo_02} />
              </a>
              <a href="#">
                <img src={Media_logo_03} />
              </a>
              <a href="#">
                <img src={Media_logo_04} />
              </a>
              <a href="#">
                <img src={Media_logo_05} />
              </a>
              <a href="#">
                <img src={Media_logo_06} />
              </a>
            </div>
          </div>
          <div className="about-footer-address">
            PT PIBO MEDIA ANAK<br/>
            Graha Kapital 1, Lt. 5<br/>
            Jl. Kemang Raya No. 4<br/>
            Jakarta Selatan
          </div>
        </div>
      </div>
    );
  }
}

export default About;
