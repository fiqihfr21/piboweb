import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import qs from 'querystring';
import swal from 'sweetalert';

import '../style/Register.scss';
import '../style/ResetPassword.scss';
import client from '../libs/client';
const color = require('../style/colors.scss');
import Button from '../components/Button';
import FetchComponent from '../components/FetchComponent';

class ResetPassword extends Component<
  { url: string } & RouteComponentProps,
  { password: string; password_confirmation: string; loading: boolean }
> {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      password_confirmation: '',
      loading: false,
    };
  }

  _reset = async () => {
    const { password, password_confirmation } = this.state;

    if (password !== '' && password_confirmation !== '') {
      this.setState({ loading: true });
      try {
        const result = await client.post(`/password/reset/${this.props.url}`, {
          password,
          password_confirmation,
        });
        swal('Perhatian!', result.data.status, 'success');
        this.props.history.push('/login');
      } catch (error) {
        console.log(error);
        swal('Perhatian!', 'Terjadi kesalahan', 'error');
      }
      this.setState({ loading: false });
    } else {
      swal('Perhatian!', 'Silahkan isi semua field yang disediakan', 'info');
    }
  };

  render() {
    return (
      <div className="form">
        <div className="form-box">
          <h2 className="form-box-title" style={{ color: color.blue }}>Masukkan password baru Anda</h2>
          <input
            className="Input"
            placeholder="Password"
            type="password"
            onChange={e => this.setState({ password: e.target.value })}
          />
          <p></p>
          <input
            className="Input"
            id="confirm"
            placeholder="Confirm password"
            type="password"
            onChange={e =>
              this.setState({ password_confirmation: e.target.value })
            }
            onKeyDown={key => {
              if (key.key === 'Enter') {
                const x = document.getElementById('confirm');
                x!.blur();
                this._reset();
              }
            }}
          />
          <div style={{ height: '8px' }} />
          <Button onClick={this.state.loading ? () => {} : this._reset}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <div
                className={'Loading' + (this.state.loading ? ' loading' : '')}
              />
              Reset
            </div>
          </Button>
        </div>
      </div>
    );
  }
}

export default props => (
  <FetchComponent
    doBeforeRender={async () => {
      const { token } = qs.parse(props.location.search.substr(1));
      return { url: token };
    }}
  >
    <ResetPassword {...props} />
  </FetchComponent>
);
