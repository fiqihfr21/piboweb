import React, { Component } from 'react';
import qs from 'querystring';
import { Link } from 'react-router-dom';
import client from '../libs/client';
import { RouteComponentProps } from 'react-router';
import FetchComponent from '../components/FetchComponent';
const color = require('../style/colors.scss');
import '../style/VerifyEmail.scss';

class VerifyEmail extends Component<
  { verify: { status: string } } & RouteComponentProps
> {
  render() {
    return (
      <div className="Container">
        <div className="Content-Container">
          <div className="content">
            <h3>{this.props.verify.status}</h3>
            {!this.props.verify.status.includes('something') &&
              !this.props.verify.status.includes('expired') && (
                <p>
                  Silahkan{' '}
                  <Link
                    to="/login"
                    style={{
                      color: color.yellow,
                    }}
                  >
                    login
                  </Link>{' '}
                  untuk melanjutkan
                </p>
              )}
          </div>
        </div>
      </div>
    );
  }
}

export default props => (
  <FetchComponent
    doBeforeRender={async () => {
      try {
        const { token } = qs.parse(props.location.search.substr(1));
        const res = await client.post(`/email/verify/${token}`);
        return { verify: res.data };
      } catch (error) {
        console.log(error.response.data);
        const errorData = error.response.data;
        return {
          verify: {
            status: errorData.status || 'something went wrong',
          },
        };
      }
    }}
  >
    <VerifyEmail {...props} />
  </FetchComponent>
);
