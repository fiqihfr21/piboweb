import React from 'react';
import client from '../libs/client';
import '../style/Dashboard.scss';
import Button from '../components/Button';
import Table from '../components/common/Table';
import BookHistoryTableContent from '../components/common/components/BookHistoryTableContent';
import { RouteComponentProps } from 'react-router';

interface AvatarType {
  id: number;
  avatar: string;
}

class DashboardAdminSekolah extends React.Component<
  RouteComponentProps,
   {
    user?: {
      name: string;
      type: string;
      age: number;
      avatar: string;
      email: string;
      id: number;
      summary: {
        total_books: number;
        total_readtime: string;
      }
    } | null;
    section: string;
    read_history: [];
  }
> {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      section: 'student',
      read_history: []
    };
  }
  
  async componentDidMount() {
    try {
      await client.get('/me')
        .then((res) => {
          this.setState({ user: res.data });
          return client.get('/read_history');
        })
        .then((res) => {
          this.setState({ read_history: res.data });
        })
      
    } catch (error) {
      console.log(error);
    }
  }

  handlePage = () => {
    const { user, read_history } = this.state;
    // console.log(user);
    switch (this.state.section) {
      case 'student': {
        return (
          //@ts-ignore
          <Table type={user && user!.type}>
            {read_history.length > 0 && read_history.map(data => (
              <BookHistoryTableContent content={data}/>
            ))}
          </Table>
        );
      }
      case 'setting': {
        // @ts-ignore
        return <EditProfile {...this.state.user} />;
      }
    }
  };

  EditAvatar = () => {
    const [avatars, setAvatars] = React.useState<Array<AvatarType> | null>(
      null
    );
    const [selected, setSelected] = React.useState<AvatarType | null>(null);
    React.useEffect(() => {
      client
        .get('/avatar')
        .then(val => {
          setAvatars(val.data);
        })
        .catch(err => console.log(err));
    }, []);
    return (
      <div>
        <h3>Pilih salah satu avatar di bawah ini</h3>
        <div className="Avatars">
          {avatars
            ? avatars.map(val => (
                <div className="Profile-Image-Container" key={val.id}>
                  <img className="Avatar-Image" src={val.avatar} alt="avatar" />
                  <div
                    className={'Edit-Image-Container' + (val.id == (selected && selected.id) ? ' selected' : '')}
                    onClick={() => setSelected(val)}
                  >
                    <img
                      className="Edit-Image"
                      src={require('../assets/check.png')}
                      alt="avatar"
                    />
                  </div>
                </div>
              ))
            : 'Loading...'}
        </div>
        {selected && (
          <Button
            onClick={() =>
              client
                .post('/avatar', { avatar: selected.avatar })
                .then(() => location.reload())
                .catch(err => console.log(err))
            }
          >
            Submit
          </Button>
        )}
      </div>
    );
  };

  render() {
    const { user } = this.state;
    return (
      <div className="Container">
        <img
          id="banner"
          style={{ width: '100%', height: '200px', objectFit: 'cover' }}
          src={require('../assets/backdrop.jpg')}
        />
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
            position: 'relative',
            marginRight: '32px',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              // alignItems: 'center',
              position: 'absolute',
              top: '0',
              width: '100%',
              transform: 'translateY(-50%)',
            }}
          >
            <div style={{ marginRight: 20 }}>
              <h3 style={{ color: 'white' }}>{user && user.name}</h3>
            </div>
            <div className="Profile-Image-Container">
              <img
                className="Avatar-Image"
                src={
                  user && user.avatar
                    ? user.avatar
                    : require('../assets/avatar.png')
                }
                alt="avatar"
              />
            </div>
          </div>
        </div>
        <div className="Dashboard-Content">
          <div className="left">
            <a className={ (this.state.section === 'student' ? 'buttonspilih' : 'buttons')}
                  onClick={() => { this.setState({ section: 'student' });}}>
                Aktifitas Kamu
            </a>

            <br />
            <a className={(this.state.section === 'setting' ? 'buttonspilih' : 'buttons')}
                  onClick={() => {this.setState({ section: 'setting' });}}>
                Pengaturan
            </a>

            <br />

            <div className="contentmenu">
              <a href={`/elibrary`} style={{ color: 'rgb(56, 193, 222)'}}>
              <div className="child" style={{ justifyContent: 'flex-end' }}>
                <div className={ 'child-content' } >
                <div style={{ marginLeft: 40 }}>
                  <img
                    height={40}
                    width={40}
                    src={require('../assets/navigation/eLibrary.png')}
                  />
                  </div>
                    <div style={{ marginLeft: 16 }}>
                      <h2 style={{ marginTop: 16 }}>eLibrary</h2>
                    </div>
                </div>
              </div>
              </a>
            </div>
            <br />
          </div>
          <div className="right">
            {this.state.section !== 'setting' && (
              <React.Fragment>
                <h1 style={{ margin: '0px', color: '#1f3f72' }}>Aktifitas Baca Kamu</h1>
                <div style={{ display: 'flex', color: '#bababa', marginTop: '8px' }}>
                  <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '8px', flexDirection: 'column', borderRight: '1px solid #bababa' }}>
                    <h2 style={{ margin: '0px', display: 'inline-block' }}>{ user ? user.summary.total_books : `0` }</h2>
                    <p style={{ margin: '0px', fontSize: '10px', display: 'inline-block' }}>Buku Terbaca</p>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '8px', flexDirection: 'column', borderRight: '1px solid #bababa' }}>
                    <h2 style={{ margin: '0px', display: 'inline-block' }}>{ user ? user.summary.total_readtime : `00.00` }</h2>
                    <p style={{ margin: '0px', fontSize: '10px', display: 'inline-block' }}>Jam Baca</p>
                  </div>
                </div>
              </React.Fragment>
            )}
            <div className="Dashboard-Tab-Container">
              {this.handlePage()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

type Props = {
  age: number;
  avatar: string;
  email: string;
  email_verified_at: string;
  id: number;
  name: string;
  type: string;
};

class EditProfile extends React.Component<Props> {
  state = {
    user_id: 0,
    name: '',
    editMode: false,
  };
  componentDidMount() {
    this.setState({ ...this.props });
  }
  
  handleTextChange = e => {
    const input = e.target;
    this.setState({ [input.name]: input.value });
  };

  handleSubmit = async () => {
    try {
      if (this.state.editMode) {
        const { name, user_id } = this.state;
        const raw = await await client.put(`family/${user_id}`, {
          name,
        });
        const res = raw.data;
        this.setState({
          editMode: false,
          user_id: res.user_id,
          name: res.name,
        });
      } else {
        this.setState({ editMode: true });
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  render() {
    const { name, editMode } = this.state;
    
    return (
      <div className="Edit-Profile-Container">
        <div className="top">
          <div className="content">
            <div className="Edit-Profile-Container-Form">
              <label>Name</label>
              <input
                className="Input"
                name="name"
                disabled={!editMode}
                value={name}
                type="text"
                placeholder="John"
                onChange={this.handleTextChange}
                onSubmit={() => console.log('submit')}
              />

              <Button onClick={this.handleSubmit}>
                {editMode ? 'Simpan perubahan' : 'Edit profile'}
              </Button>
              </div>
            </div>
        </div>
      </div>
    );
  }
}

export default DashboardAdminSekolah;
