import React from 'react';
// import { books, book, order } from '../libs/types';
import client from '../libs/client';
import '../style/Dashboard.scss';
import Button from '../components/Button';
import Table from '../components/common/Table';
import StudentTableContent from '../components/common/components/StudentTableContent';
import { RouteComponentProps } from 'react-router';
// import LoadMore from '../components/LoadMore';
// import LoadMoreOrder from '../components/LoadMoreOrder';
// import EBookCard from '../components/EBookCard';
// import BookCard from '../components/BookCard';
// import moment from 'moment';
// import { Link } from 'react-router-dom';
import rSwal from '@sweetalert/with-react';

interface AvatarType {
  id: number;
  avatar: string;
}

class DashboardAdminSekolah extends React.Component<
  RouteComponentProps,
   {
    user?: {
      name: string;
      school_name: string;
      type: string;
      age: number;
      avatar: string;
      email: string;
      id: number;
    } | null;
    section: string;
    list_student: any;
  }
> {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      section: 'student',
      list_student: []
    };
  }
  
  async componentDidMount() {
    try {
      await client.get('/me')
        .then((res) => {
          this.setState({ user: res.data });
          return client.get('/class');
        })
        .then((res) => {
          this.setState({ list_student: res.data.Data });
        })
      
    } catch (error) {
      console.log(error);
    }
  }

  handlePage = () => {
    const { user, list_student } = this.state;
    
    switch (this.state.section) {
      case 'student': {
        return (
          //@ts-ignore
          <Table type={user && user!.type} totalStudent={ Object.keys(list_student).length > 0 && list_student.murid.length }>
            {Object.keys(list_student).length > 0 && list_student.murid.map(data => (
              <StudentTableContent content={data}/>
            ))}
          </Table>
        );
      }
      case 'setting': {
        // @ts-ignore
        return <EditProfile {...this.state.user} />;
      }
    }
  };

  EditAvatar = () => {
    const [avatars, setAvatars] = React.useState<Array<AvatarType> | null>(
      null
    );
    const [selected, setSelected] = React.useState<AvatarType | null>(null);
    React.useEffect(() => {
      client
        .get('/avatar')
        .then(val => {
          setAvatars(val.data);
        })
        .catch(err => console.log(err));
    }, []);
    return (
      <div>
        <h3>Pilih salah satu avatar di bawah ini</h3>
        <div className="Avatars">
          {avatars
            ? avatars.map(val => (
                <div className="Profile-Image-Container" key={val.id}>
                  <img className="Avatar-Image" src={val.avatar} alt="avatar" />
                  <div
                    className={'Edit-Image-Container' + (val.id == (selected && selected.id) ? ' selected' : '')}
                    onClick={() => setSelected(val)}
                  >
                    <img
                      className="Edit-Image"
                      src={require('../assets/check.png')}
                      alt="avatar"
                    />
                  </div>
                </div>
              ))
            : 'Loading...'}
        </div>
        {selected && (
          <Button
            onClick={() =>
              client
                .post('/avatar', { avatar: selected.avatar })
                .then(() => location.reload())
                .catch(err => console.log(err))
            }
          >
            Submit
          </Button>
        )}
      </div>
    );
  };

  render() {
    const { user } = this.state;
    return (
      <div className="Container">
        <img
          id="banner"
          style={{ width: '100%', height: '200px', objectFit: 'cover' }}
          src={require('../assets/backdrop.jpg')}
        />
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
            position: 'relative',
            marginRight: '32px',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              // alignItems: 'center',
              position: 'absolute',
              top: '0',
              width: '100%',
              transform: 'translateY(-50%)',
            }}
          >
            <div style={{ marginRight: 20 }}>
              <h2 style={{ color: 'white', marginTop: '0px', marginBottom: '0px' }}>{user && user.name}</h2>
              <h5 style={{ color: '#1f3f72', marginTop: '0px', textAlign: 'right' }}>Guru Kelas</h5>
            </div>
            <div className="Profile-Image-Container">
              <img
                className="Avatar-Image"
                src={
                  user && user.avatar
                    ? user.avatar
                    : require('../assets/avatar.png')
                }
                alt="avatar"
              />
              {this.state.section === 'setting' && (
                <div
                  className="Edit-Image-Container"
                  onClick={() =>
                    rSwal({
                      content: <this.EditAvatar />,
                      button: false,
                      className: 'edit-avatar-modal',
                    })
                  }
                >
                  <img
                    className="Edit-Image"
                    src={require('../assets/edit.png')}
                    alt="avatar"
                  />
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="Dashboard-Content">
          <div className="left">
            <a className={ (this.state.section === 'student' ? 'buttonspilih' : 'buttons')}
                  onClick={() => { this.setState({ section: 'student' });}}>
                List Murid
            </a>

            <br />
            <a className={(this.state.section === 'setting' ? 'buttonspilih' : 'buttons')}
                  onClick={() => {this.setState({ section: 'setting' });}}>
                Pengaturan
            </a>

            <br />

            <div className="contentmenu">
              <a href={`/elibrary`} style={{ color: 'rgb(56, 193, 222)'}}>
              <div className="child" style={{ justifyContent: 'flex-end' }}>
                <div className={ 'child-content' } >
                <div style={{ marginLeft: 40 }}>
                  <img
                    height={40}
                    width={40}
                    src={require('../assets/navigation/eLibrary.png')}
                  />
                  </div>
                    <div style={{ marginLeft: 16 }}>
                      <h2 style={{ marginTop: 16 }}>eLibrary</h2>
                    </div>
                </div>
              </div>
              </a>
              {/* <a href={`/shop`} style={{ color: 'rgb(56, 193, 222)'}}>
                <div className="child" style={{  justifyContent: 'flex-start' }}>
                  <div className={ 'child-content' } >
                    <div style={{ marginLeft: 40 }}>
                      <img
                        height={40}
                        width={40}
                        src={require('../assets/navigation/shop.png')}
                      />
                    </div>
                    <div style={{ marginLeft: 16 }}>
                      <h2 style={{ marginTop: 16 }}>Shop</h2>
                    </div>
                  </div>
                </div>
              </a> */}
            </div>
            <br />
          </div>
          <div className="right">
            {this.state.section !== 'setting' && (
              <React.Fragment>
                <h1 style={{ margin: '0px', color: '#1f3f72' }}>{ user && user.school_name }</h1>
              </React.Fragment>
            )}
            <div className="Dashboard-Tab-Container">{this.handlePage()}</div>
          </div>
        </div>
      </div>
    );
  }
}

type Props = {
  avatar: string;
  email: string;
  email_verified_at: string;
  id: number;
  name: string;
  type: string;
};

class EditProfile extends React.Component<Props> {
  state = {
    fav: [],
    avatar: '',
    email: '',
    id: 0,
    name: '',
    type: '',
    editMode: false,
  };
  componentDidMount() {
    this.setState({ ...this.props });
  }

  handleFav = event => {
    let value = event.target.value;
    let fav = [...this.state.fav];
    // @ts-ignore
    if (fav.includes(value)) {
      fav = fav.filter(e => e !== value);
    } else {
      // @ts-ignore
      fav.push(value);
    }
    this.setState({ fav: [...fav] });
  };

  handleChecked = (value: string) => {
    // @ts-ignore
    return this.state.fav.includes(value);
  };

  handleTextChange = e => {
    const input = e.target;
    this.setState({ [input.name]: input.value });
  };

  handleSubmit = async () => {
    try {
      if (this.state.editMode) {
        const { name } = this.state;
        const raw = await await client.post(`/me`, {
          name,
        });
        const res = raw.data;
        this.setState({
          editMode: false,
          avatar: res.avatar,
          email: res.email,
          id: res.id,
          name: res.name,
          type: res.type,
        });
      } else {
        this.setState({ editMode: true });
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  render() {
    const { name, email, editMode } = this.state;
    return (
      <div className="Edit-Profile-Container">
        <div className="top">
          <div className="content">
            <div className="Edit-Profile-Container-Form">
              <label>Name</label>
              <input
                className="Input"
                name="name"
                disabled={!editMode}
                value={name}
                type="text"
                placeholder="John"
                onChange={this.handleTextChange}
                onSubmit={() => console.log('submit')}
              />

              <label>E-mail</label>
              <input
                className="Input"
                name="email"
                disabled={!editMode}
                value={email}
                type="email"
                placeholder="johncena@mail.com"
                onChange={this.handleTextChange}
                onSubmit={() => console.log('submit')}
              />

              <span className="Edit-Profile-Container-Form-Forgot">
                <a href="/change-password">Ganti kata sandi</a>
              </span>
              <Button onClick={this.handleSubmit}>
                {editMode ? 'Simpan perubahan' : 'Edit profile'}
              </Button>
              </div>
            </div>
        </div>
      </div>
    );
  }
}

export default DashboardAdminSekolah;
