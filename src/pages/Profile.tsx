import React from 'react';
import { books, book, order } from '../libs/types';
import client from '../libs/client';
import '../style/Dashboard.scss';
import Table from '../components/common/Table';
import Button from '../components/Button';
import LoadMore from '../components/LoadMore';
import FamilyTableContent from '../components/common/components/FamilyTableContent';
import LoadMoreOrder from '../components/LoadMoreOrder';
import EBookCard from '../components/EBookCard';
import BookCard from '../components/BookCard';
import moment from 'moment';
import { Link } from 'react-router-dom';
import rSwal from '@sweetalert/with-react';
interface AvatarType {
  id: number;
  avatar: string;
}

class Profile extends React.Component<
  { books: books },
  {
    user?: {
      name: string;
      age: number;
      type: string;
      avatar: string;
      email: string;
      id: number;
    } | null;
    section: string;
    list_child: any;
  }
> {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      section: 'profil_anak',
      list_child: []
    };
  }

  async componentDidMount() {
    try {
      const user = await client.get('/me');
      this.setState({ user: user.data });
      const list_child = await client.get('/family');
      console.log(list_child.data.Data)
      this.setState({ list_child: list_child.data.Data });
    } catch (error) {
      console.log(error);
    }
  }

  handlePage = () => {
    const { user, list_child } = this.state;
    
    switch (this.state.section) {
      case 'profil_anak' : {
        return (
          //@ts-ignore
          <Table type={user && user.type} totalChild={ Object.keys(list_child).length > 0 && list_child.child.length }>
            {Object.keys(list_child).length > 0 && list_child.child.map(data => (
              <FamilyTableContent content={data}/>
            ))}
          </Table>
        )
      }
      case 'collection': {
        return (
          <LoadMore
            url="/booksubscribe"
            container={props => (
              <>
                <div className="Tab-Header">
                  {/*<span>Kamu memiliki {props.total} buku</span>*/}
                  <span>Lanjut baca atau baca ulang lagi.</span>
                </div>
                <div className="Tab-Content-Container">
                  <div className="Tab-Content">{props.children}</div>
                </div>
              </>
            )}
            renderItems={(item: book) => {
              return (
                <EBookCard
                  key={item.id + item.name + Math.random()}
                  item={item}
                />
              );
            }}
          />
        );
      }
      case 'wishlist': {
        return (
          <LoadMore
            url="/listwishlists"
            container={props => (
              <>
                <div className="Tab-Header">
                  {/*<span>Kamu memiliki {props.total} buku</span>*/}
                  <span>Segera miliki produk yang ada diwishlist kamu.</span>
                </div>
                <div className="Tab-Content-Container">
                  <div className="Tab-Content">{props.children}</div>
                </div>
              </>
            )}
            renderItems={(item: book) => {
              return (
                <BookCard
                  bookId={item.id}
                  image={
                    item.cover_image
                      ? item.cover_image
                      : require('../assets/placeholder.png')
                  }
                  key={item.id}
                  price={item.regular_price}
                  title={item.name}
                  type={item.type}
                  price_1={item.regular_price}
                />
              );
            }}
          />
        );
      }
      case 'bacananti': {
        return (
          <LoadMore
            url="/listbacananti"
            container={props => (
              <>
                <div className="Tab-Header">
                  {/*<span>Kamu memiliki {props.total} buku</span>*/}
                  <span>Ayo, segera baca!</span>
                </div>
                <div className="Tab-Content-Container">
                  <div className="Tab-Content">{props.children}</div>
                </div>
              </>
            )}
            renderItems={(item: book) => {
              return (
                <EBookCard
                  key={item.id + item.name + Math.random()}
                  item={item}
                />
              );
            }}
          />
        );
      }
      case 'transaction': {
        return (
          <div className="Transaction-Content">
            <LoadMoreOrder
              url="/orders"
              container={props => (
                <>
                  {console.log("yg muncul",props)}
                  <div className="Tab-Header">
                    <span style={{ fontFamily: "Chewy",fontSize:"22px",fontWeight:"bold"}}>
                        eLibrary
                    </span>
                  </div>
                  <table style={{ width: '100%' }}>
                    <tbody>
                      <tr className="title">
                        <th>{props.total}</th>
                        <th style={{ textDecoration: "underline" }}>
                          <Link to="/subscribe">Ubah Paket</Link></th>
                        <th />
                      </tr>
                    </tbody>
                  </table>
                  <div className="Tab-Header">
                    <span style={{ fontFamily: "Chewy", fontSize: "22px", fontWeight: "bold" }}>
                      eShop
                    </span>
                    {/* <span>Kamu memiliki {props.total} order</span> */}
                  </div>
                  <table style={{ width: '100%' }}>
                    <tbody>
                      <tr className="title">
                        <th>Order</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Total</th>
                        <th />
                      </tr>
                      {props.children}
                    </tbody>
                  </table>
                </>
              )}
              renderItems={(item: order) => {
                return (
                  <tr key={item.id}>
                    <td>#{item.id}</td>
                    <td>{moment(item.created_at).format('MMMM D, YYYY')}</td>
                    <td>{item.status}</td>
                    <td>Rp. {parseFloat(item.total_price).toLocaleString()}</td>
                    <td />
                  </tr>
                );
              }}
            />
          </div>
        );
      }
      case 'setting': {
        // @ts-ignore
        return <EditProfile {...this.state.user} />;
      }
    }
  };

  EditAvatar = () => {
    const [avatars, setAvatars] = React.useState<Array<AvatarType> | null>(
      null
    );
    const [selected, setSelected] = React.useState<AvatarType | null>(null);
    React.useEffect(() => {
      client
        .get('/avatar')
        .then(val => {
          setAvatars(val.data);
        })
        .catch(err => console.log(err));
    }, []);
    return (
      <div>
        <h3>Pilih salah satu avatar di bawah ini</h3>
        <div className="Avatars">
          {avatars
            ? avatars.map(val => (
                <div className="Profile-Image-Container" key={val.id}>
                  <img className="Avatar-Image" src={val.avatar} alt="avatar" />
                  <div
                    className={
                      'Edit-Image-Container' +
                      (val.id == (selected && selected.id) ? ' selected' : '')
                    }
                    onClick={() => setSelected(val)}
                  >
                    <img
                      className="Edit-Image"
                      src={require('../assets/check.png')}
                      alt="avatar"
                    />
                  </div>
                </div>
              ))
            : 'Loading...'}
        </div>
        {selected && (
          <Button
            onClick={() =>
              client
                .post('/avatar', { avatar: selected.avatar })
                .then(() => location.reload())
                .catch(err => console.log(err))
            }
          >
            Submit
          </Button>
        )}
      </div>
    );
  };

  render() {
    const { user } = this.state;
    return (
      <div className="Container">
        <img
          id="banner"
          style={{ width: '100%', height: '200px', objectFit: 'cover' }}
          src={require('../assets/backdrop.jpg')}
        />
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
            position: 'relative',
            marginRight: '32px',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              // alignItems: 'center',
              position: 'absolute',
              top: '0',
              width: '100%',
              transform: 'translateY(-50%)',
            }}
          >
            <div style={{ marginRight: 20 }}>
              <h3 style={{ color: 'white', margin: '0px' }}>{user && user.name}</h3>
              {/* <h4 style={{ color: 'rgb(31, 63, 114)', margin: '0px' }}>{user && user.type}</h4> */}
            </div>
            <div className="Profile-Image-Container">
              <img
                className="Avatar-Image"
                src={
                  user && user.avatar
                    ? user.avatar
                    : require('../assets/avatar.png')
                }
                alt="avatar"
              />
              {this.state.section === 'setting' && (
                <div
                  className="Edit-Image-Container"
                  onClick={() =>
                    rSwal({
                      content: <this.EditAvatar />,
                      button: false,
                      className: 'edit-avatar-modal',
                    })
                  }
                >
                  <img
                    className="Edit-Image"
                    src={require('../assets/edit.png')}
                    alt="avatar"
                  />
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="Dashboard-Content">
          <div className="left">
            <a className={
                    (this.state.section === 'profil_anak' ? 'buttonspilih' : 'buttons')
                  }
                  onClick={() => {
                    this.setState({ section: 'profil_anak' });
                  }}
              >
                Pindah Profil
            </a>

            <br />
            <a className={
                    (this.state.section === 'collection' ? 'buttonspilih' : 'buttons')
                  }
                  onClick={() => {
                    this.setState({ section: 'collection' });
                  }}
              >
                Koleksiku
            </a>

            <br />
            <a className={
                    (this.state.section === 'bacananti' ? 'buttonspilih' : 'buttons')
                  }
                  onClick={() => {
                    this.setState({ section: 'bacananti' });
                  }}
              >
                Baca Nanti
            </a>

            <br />
            <a className={
                    (this.state.section === 'wishlist' ? 'buttonspilih' : 'buttons')
                  }
                  onClick={() => {
                    this.setState({ section: 'wishlist' });
                  }}
              >
                Wishlist
            </a>

            <br />
            <a className={
                    (this.state.section === 'transaction' ? 'buttonspilih' : 'buttons')
                  }
                  onClick={() => {
                    this.setState({ section: 'transaction' });
                  }}
              >
                Transaksi
            </a>

            <br />
            <a className={
                    (this.state.section === 'setting' ? 'buttonspilih' : 'buttons')
                  }
                  onClick={() => {
                    this.setState({ section: 'setting' });
                  }}
              >
                Pengaturan
            </a>

            <br />

            <div className="contentmenu">
              <a href={`/elibrary`} style={{ color: 'rgb(56, 193, 222)'}}>
              <div
              className="child"
              style={{
                justifyContent: 'flex-end',
              }}
              >
                <div
                  className={
                    'child-content'
                  }
                >
                <div style={{ marginLeft: 40 }}>
                  <img
                    height={40}
                    width={40}
                    src={require('../assets/navigation/eLibrary.png')}
                  />
                  </div>
                    <div style={{ marginLeft: 16 }}>
                      <h2 style={{ marginTop: 16 }}>eLibrary</h2>
                    </div>
                </div>
              </div>
              </a>
              <a href={`/shop`} style={{ color: 'rgb(56, 193, 222)'}}>
                <div
                  className="child"
                  style={{
                    justifyContent: 'flex-start',
                  }}
                >
                  <div
                    className={
                      'child-content'
                    }
                  >
                    <div style={{ marginLeft: 40 }}>
                      <img
                        height={40}
                        width={40}
                        src={require('../assets/navigation/shop.png')}
                      />
                    </div>
                    <div style={{ marginLeft: 16 }}>
                      <h2 style={{ marginTop: 16 }}>Shop</h2>
                    </div>
                  </div>
                  </div>
              </a>
            </div>
            <br />
          </div>
          <div className="right">
            <div className="Dashboard-Tab-Container">{this.handlePage()}</div>
          </div>
        </div>
      </div>
    );
  }
}

type Props = {
  age: number;
  avatar: string;
  email: string;
  email_verified_at: string;
  id: number;
  name: string;
  type: string;
};

class EditProfile extends React.Component<Props> {
  state = {
    age: 0,
    fav: [],
    avatar: '',
    email: '',
    id: 0,
    name: '',
    type: '',
    editMode: false,
  };
  componentDidMount() {
    this.setState({ ...this.props });
  }
  handleAge = changeEvent => {
    this.setState({ age: changeEvent.target.value });
  };

  handleFav = event => {
    let value = event.target.value;
    let fav = [...this.state.fav];
    // @ts-ignore
    if (fav.includes(value)) {
      fav = fav.filter(e => e !== value);
    } else {
      // @ts-ignore
      fav.push(value);
    }
    this.setState({ fav: [...fav] });
  };

  handleChecked = (value: string) => {
    // @ts-ignore
    return this.state.fav.includes(value);
  };

  handleTextChange = e => {
    const input = e.target;
    this.setState({ [input.name]: input.value });
  };

  handleSubmit = async () => {
    try {
      if (this.state.editMode) {
        const { age, name } = this.state;
        const raw = await await client.post(`/me`, {
          age,
          name,
        });
        const res = raw.data;
        this.setState({
          editMode: false,
          age: res.age,
          avatar: res.avatar,
          email: res.email,
          id: res.id,
          name: res.name,
          type: res.type,
        });
      } else {
        this.setState({ editMode: true });
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  render() {
    const { name, email, age, editMode } = this.state;
    return (
      <div className="Edit-Profile-Container">
        <div className="top">
          <div className="content">
            <div className="Edit-Profile-Container-Form">
              <label>Name</label>
              <input
                className="Input"
                name="name"
                disabled={!editMode}
                value={name}
                type="text"
                placeholder="John"
                onChange={this.handleTextChange}
                onSubmit={() => console.log('submit')}
              />

              <label>E-mail</label>
              <input
                className="Input"
                name="email"
                disabled={!editMode}
                value={email}
                type="email"
                placeholder="johncena@mail.com"
                onChange={this.handleTextChange}
                onSubmit={() => console.log('submit')}
              />

              <label>Umur</label>
              <input
                className="Input"
                name="age"
                disabled={!editMode}
                // @ts-ignore
                value={age !== 0 && age}
                type="number"
                placeholder="Umur"
                onChange={this.handleTextChange}
                onSubmit={() => console.log('submit')}
              />

              <span className="Edit-Profile-Container-Form-Forgot">
                <a href="/change-password">Ganti kata sandi</a>
              </span>
              <Button onClick={this.handleSubmit}>
                {editMode ? 'Simpan perubahan' : 'Edit profile'}
              </Button>
              </div>
            </div>
        </div>
      </div>
    );
  }
}

export default Profile;
