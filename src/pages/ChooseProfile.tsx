import React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import UserStore from '../stores/UserStore';
import { inject, observer } from 'mobx-react';
import client from '../libs/client';
import '../style/Dashboard.scss';
import { color } from '../libs/metrics';

@inject('userStore')
@observer
class Profile extends React.Component<
  RouteComponentProps & { userStore?: UserStore },
  {
    user?: {
      name: string;
      age: number;
      type: string;
      avatar: string;
      email: string;
      id: number;
    } | null;
    section: string;
    list_child: any;
  }
> {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      section: 'profil_anak',
      list_child: []
    };
  }

  async componentDidMount() {
    try {
      const user = await client.get('/me');
      this.setState({ user: user.data });
      const list_child = await client.get('/family');
      console.log(list_child.data.Data)
      this.setState({ list_child: list_child.data.Data });
    } catch (error) {
      console.log(error);
    }
  }

  async switchToChildAcc(data) {
    try {
      const res = await client.post('/login/family', {user_id: data});
      if(res) {
        const { access_token } = res.data;
        await this.props.userStore!.setToken(access_token);
        this.props.history.replace('/dashboard_anak');
        location.reload();
      }
    } catch (error) {
      console.log(error);
    }        
  }


  render() {
    return (
      <div className="Container">
        <div
          id="banner"
          style={{ 
            width: '100%', 
            height: '600px',
            display: 'flex',
            justifyContent: 'center',
            background: `url(${ require('../assets/backdrop.jpg')})`}}
        >
          <div
            style={{
              margin: '20px',
              width: '600px',
              height: '300px',
              display: 'flex',
              alignItems: 'center',
              flexDirection: 'column',
              borderRadius: '16px',
              background: '#fff',
            }}
          >
            <div style={{ marginTop: '10px', padding: '12px', display: 'inline', borderRadius: '42px', border: `2px solid ${color.blue}` }}>
              <h2 style={{ color: `${color.blue}`, display: 'inline', marginBottom: '16px'}}>E-Library</h2>
            </div>

            <h3 style={{ color: `${color.blue}` }}> {localStorage.getItem('familyInfo') ? "Pilih Profilmu" : "Profil anak masih kosong !" }</h3>

            <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'row' }}>
              {localStorage.getItem('familyInfo') && JSON.parse(localStorage.getItem('familyInfo') as string).childs.map(data => (
                <a style={{ margin: '10px', cursor: 'pointer' }} onClick={() => this.switchToChildAcc(data.user_id)}>
                  <img style={{ marginTop: '5px', marginBottom: '5px', height: 80, width: 80 }} src={ require('../assets/avatar_sm.png') }/>
                  <p style={{ margin: 0, textAlign: 'center' }}><strong>{data.name}</strong></p>
                </a>
              ))}
            </div>
            {localStorage.getItem('familyInfo') && 
             JSON.parse(localStorage.getItem('familyInfo') as string).childs.length < 3 && (
              <Link style={{ marginTop: '28px', }} to={`/family`}>
                <div style={{ backgroundColor: `${color.blue}`, padding: '8px', display: 'inline', borderRadius: '4px' }}>
                  <p style={{ color: '#fff', display: 'inline', marginBottom: '16px'}}>Tambah Akun</p>
                </div>
              </Link>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Profile;
