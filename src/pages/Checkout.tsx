import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import CartStore from '../stores/CartStore';
import '../style/Checkout.scss';
import InputDropdown from '../components/InputDropdown';
import { province, address, paymentType, shipping } from '../libs/types';
import FetchComponent from '../components/FetchComponent';
import client from '../libs/client';
import { RouteComponentProps } from 'react-router';
import Button from '../components/Button';
const color = require('../style/colors.scss');
import swal from 'sweetalert';

const ListItem = inject('cartStore')(
  observer((props: { cartStore?: CartStore }) => {
    return (
      <div className="List-Item-Container">
        {props.cartStore!.items.map(val => (
          <div className="item" key={val.id} style={{ minHeight: 0 }}>
            <div style={{ width: '72px', textAlign: 'end' }}>
              <img src={val.cover_image} />
            </div>
            <div style={{ flex: 1, marginRight: '8px', position: 'relative' }}>
              <p className="title">{val.name}</p>
              <p className="type">{val.type}</p>
              <p className="price">
                Rp{' '}
                {parseFloat(
                  val.sales_price ? val.sales_price : val.regular_price
                ).toLocaleString()}{' '}
                <span style={{ color: 'black' }}>x {val.quantity}</span>
              </p>
            </div>
          </div>
        ))}
      </div>
    );
  })
);

interface Props {
  cartStore?: CartStore;
}

interface State {
  provinces: Array<province>;
  province_name: string;
  districts: Array<province>;
  districts_name: string;
  regency: Array<province>;
  regency_name: string;
  postCodes: Array<province>;
  postCode: string;
  addresses: Array<address>;
  address: string;
  name: string;
  phone: string;
  addressLocation: string;
  paymentType: Array<paymentType>;
  selectedPayment: string;
  loading: boolean;
  shipping: shipping | null;
  shippingMethod: string;
  shippingCost: number;
  addingAddress: boolean;
}

@inject('cartStore')
@observer
class Checkout extends Component<Props & RouteComponentProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      province_name: '',
      provinces: [],
      districts_name: '',
      districts: [],
      regency_name: '',
      regency: [],
      postCodes: [],
      postCode: '',
      addresses: [],
      address: '',
      name: '',
      phone: '',
      addressLocation: '',
      paymentType: [],
      selectedPayment: '',
      loading: false,
      shipping: null,
      shippingMethod: '',
      shippingCost: 0,
      addingAddress: false,
    };
  }

  async componentDidMount() {
    await this.getPayment();
    await this.getProvinces();
    await this.getAddress();
    await this.getShipping();
  }

  getAddress = async () => {
    try {
      const addresses = await getAddresses();
      this.setState({ addresses });
      if (addresses.length === 0) {
        this.setState({ address: 'Tambah alamat baru' });
      } else {
        this.setState({ address: addresses[0].address });
        this.setAddress(addresses[0].address);
      }
    } catch (error) {
      throw error;
    }
  };

  setAddress = async address => {
    console.log('selected', address);
    this.setState({
      shippingMethod: '',
      shippingCost: 0,
    });
    try {
      if (address !== 'Tambah alamat baru') {
        const adrs = this.state.addresses.find(
          (val: address) => val.address === address
        );
        this.setState({
          name: adrs!.recipient_name,
          phone: adrs!.phone_number,
          province_name: adrs!.province,
          regency_name: adrs!.regency,
          districts_name: adrs!.district,
          postCode: `${adrs!.village} / ${adrs!.zip_code}`,
          addressLocation: adrs!.address,
        });
        await this.getRegency();
        await this.getDistricts();
        await this.getPostCode();
        await this.getShipping();
      } else {
        this.setState({
          name: '',
          phone: '',
          province_name: '',
          regency_name: '',
          districts_name: '',
          postCode: '',
          addressLocation: '',
          regency: [],
          districts: [],
          postCodes: [],
        });
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  getProvinces = async () => {
    try {
      const provinces = await getProvince();
      this.setState({ provinces });
    } catch (error) {
      throw error;
    }
  };

  getRegency = async () => {
    try {
      const { province_name, provinces } = this.state;
      const province: province | undefined = await provinces.find(
        (val: province) => val.name === province_name
      );
      const regency = await getDistrict(province!.id);
      this.setState({ regency });
    } catch (error) {
      throw error;
    }
  };

  getDistricts = async () => {
    try {
      const { regency_name, regency } = this.state;
      const reg: province | undefined = await regency.find(
        (val: province) => val.name === regency_name
      );
      const districts = await getSubDistrict(reg!.id);
      this.setState({ districts });
    } catch (error) {
      throw error;
    }
  };

  getPostCode = async () => {
    try {
      const { districts_name, districts } = this.state;
      const district: province | undefined = await districts.find(
        (val: province) => val.name === districts_name
      );
      const postCodes = await getPostCode(district!.id);
      this.setState({ postCodes });
    } catch (error) {
      throw error;
    }
  };

  getPayment = async () => {
    try {
      const type = await getPaymentType();
      this.setState({ paymentType: type });
    } catch (error) {
      throw error;
    }
  };

  getShipping = async () => {
    const { addresses, address } = this.state;
    try {
      let ADDRESS: address | undefined = addresses.find(
        (val: address) => val.address === address
      );
      const list = await getShippingList(ADDRESS!.id);
      this.setState({ shipping: list });
    } catch (error) {
      throw error;
    }
  };

  addAddress = async () => {
    const {
      name,
      phone,
      province_name,
      regency_name,
      districts_name,
      postCode,
      addressLocation,
    } = this.state;
    const code = postCode.split(' / ');
    try {
      this.setState({ addingAddress: true });
      const ADDRESS = await addAddress({
        recipient_name: name,
        phone_number: phone,
        province: province_name,
        regency: regency_name,
        district: districts_name,
        village: code[0],
        zip_code: code[1],
        address: addressLocation,
      });
      await this.getAddress();
      await this.setAddress(ADDRESS.address);
      await this.setState({ addingAddress: true, address: ADDRESS.address });
      await this.getShipping();
    } catch (error) {
      console.log(
        '%cError on add new address: ',
        'color: red; font-weight: bold;',
        error.message
      );
    }
  };

  payment = async coupon => {
    if (this.state.shippingMethod === '') {
      swal(
        'Perhatian!',
        'Silahkan pilih metode pengiriman terlebih dahulu',
        'info'
      );
    } else if (this.state.addressLocation !== '') {
      const {
        name,
        phone,
        province_name,
        regency_name,
        districts_name,
        postCode,
        addressLocation,
        addresses,
        address,
        shippingMethod,
      } = this.state;
      this.setState({ loading: true });
      const code = postCode.split(' / ');
      try {
        let ADDRESS: address | undefined;
        if (this.state.address === 'Tambah alamat baru') {
          try {
            ADDRESS = await addAddress({
              recipient_name: name,
              phone_number: phone,
              province: province_name,
              regency: regency_name,
              district: districts_name,
              village: code[0],
              zip_code: code[1],
              address: addressLocation,
            });
          } catch (error) {
            console.log(error);
            console.log(
              '%cError on add new address: ',
              'color: red; font-weight: bold;',
              error.message
            );
          }
        } else {
          ADDRESS = addresses.find((val: address) => val.address === address);
        }
        try {
          // const type = paymentType.find(val => val.type === selectedPayment);
          const result = await checkOut(1, ADDRESS!.id, shippingMethod, coupon);
          await this.setState({ loading: false });
          // this.props.history.push(result.url);
          // this.props.history.createHref()
          // window.open(result.url, '_blank');
          window.location = result.url;
        } catch (error) {
          console.log(
            '%cError on transaction: ',
            'color: red; font-weight: bold;',
            error.message
          );
        }
      } catch (error) {
        console.log(error.message);
      }
    }
  };

  render() {
    const disabled = this.state.address !== 'Tambah alamat baru';
    const cart = this.props.cartStore!;
    return (
      <div className="Container">
        <div className="Checkout-Content">
          <div className="content-container">
            <div className="horizontal">
              <div className="section left">
                <div className="card">
                  <h1>Alamat Kirim</h1>
                  <h4>Silahkan isi data alamat kamu</h4>
                  <label style={{ color: color.blue }}>Pilih Alamat atau Tambah Alamat Baru</label>
                  <InputDropdown
                    placeholder="Alamat"
                    options={[
                      ...this.state.addresses,
                      { id: 99, address: 'Tambah alamat baru' },
                    ]}
                    value={this.state.address}
                    valueBy="address"
                    handleChange={async val => {
                      await this.setState({
                        address: val,
                      });
                      this.setAddress(val);
                    }}
                  />
                  {this.state.address !== '' && (
                    <React.Fragment>
                      <label style={{ color: color.blue }}>Nama</label>
                      <input
                        className="Input"
                        disabled={disabled}
                        placeholder="Nama"
                        value={this.state.name}
                        onChange={e => this.setState({ name: e.target.value })}
                      />
                      <label style={{ color: color.blue }}>Telepon</label>
                      <input
                        className="Input"
                        disabled={disabled}
                        placeholder="Telepon"
                        value={this.state.phone}
                        onChange={e => this.setState({ phone: e.target.value })}
                      />
                      <div className="horizontal-section">
                        <div className="content">
                          <label style={{ color: color.blue }}>Provinsi</label>
                          <InputDropdown
                            disabled={disabled}
                            placeholder="Provinsi"
                            options={this.state.provinces}
                            value={this.state.province_name}
                            valueBy="name"
                            handleChange={async val => {
                              await this.setState({
                                province_name: val,
                              });
                              await this.getRegency();
                            }}
                          />
                        </div>
                        <div style={{ width: '16px' }} />
                        <div className="content">
                          <label style={{ color: color.blue }}>
                            Kotamadya/Kabupaten
                          </label>
                          <InputDropdown
                            disabled={disabled}
                            placeholder="Kotamadya/Kabupaten"
                            options={this.state.regency}
                            value={this.state.regency_name}
                            valueBy="name"
                            handleChange={async val => {
                              await this.setState({
                                regency_name: val,
                              });
                              await this.getDistricts();
                            }}
                          />
                        </div>
                      </div>
                      <div className="horizontal-section">
                        <div className="content">
                          <label style={{ color: color.blue }}>
                            Kelurahan/Kecamatan
                          </label>
                          <InputDropdown
                            disabled={disabled}
                            placeholder="Kelurahan/Kecamatan"
                            options={this.state.districts}
                            value={this.state.districts_name}
                            valueBy="name"
                            handleChange={async val => {
                              await this.setState({
                                districts_name: val,
                              });
                              await this.getPostCode();
                            }}
                          />
                        </div>
                        <div style={{ width: '16px' }} />
                        <div className="content">
                          <label style={{ color: color.blue }}>Kode Pos</label>
                          <InputDropdown
                            disabled={disabled}
                            placeholder="Kode Pos"
                            options={this.state.postCodes}
                            value={this.state.postCode}
                            valueBy="name"
                            handleChange={async val => {
                              await this.setState({
                                postCode: val,
                              });
                            }}
                          />
                        </div>
                      </div>
                      <label style={{ color: color.blue }}>Alamat</label>
                      <input
                        className="Input"
                        disabled={disabled}
                        placeholder="Alamat"
                        value={this.state.addressLocation}
                        onChange={e =>
                          this.setState({ addressLocation: e.target.value })
                        }
                      />
                      {!disabled && (
                        <div
                          style={{
                            width: '100%',
                            display: 'flex',
                            marginTop: 8,
                            flexDirection: 'column',
                          }}
                        >
                          <Button
                            onClick={this.addAddress}
                            style={{
                              backgroundColor: color.yellow,
                              shadowColor: color.yellow,
                              borderRadius: '8px',
                            }}
                          >
                            <div
                              style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                              }}
                            >
                              <div
                                className={
                                  'Loading' +
                                  (this.state.addingAddress ? ' loading' : '')
                                }
                              />
                              Simpan
                            </div>
                          </Button>
                        </div>
                      )}
                    </React.Fragment>
                  )}
                </div>
              </div>
              <div className="section right hide-on-mobile-and-tablet">
                <div style={{ width: '100%' }}>
                  <div className="card">
                    <>
                      <div className="horizontal-section">
                        <div className="content">
                          <div className="horizontal">
                            <p>Subtotal</p>
                            <p style={{ color: color.blue }}>
                              Rp {cart.totalPrice.toLocaleString()}
                            </p>
                          </div>
                          <div className="horizontal">
                            <p>Pengiriman</p>
                            <p style={{ color: color.blue }}>
                              Rp {this.state.shippingCost.toLocaleString()}
                            </p>
                          </div>

                          <div className="horizontal">
                            <p>Voucher</p>
                            <p style={{ color: color.blue }}>
                              {cart.coupon ? cart.coupon : '-'}
                            </p>
                          </div>
                          <div className="horizontal">
                            <h3 style={{ color: color.yellow }}>Total</h3>
                            <h3 style={{ color: color.yellow }}>
                              Rp{' '}
                              {(
                                cart.totalPrice + this.state.shippingCost
                              ).toLocaleString()}
                            </h3>
                          </div>
                        </div>
                      </div>
                      <Button
                        onClick={() => {
                          console.log(cart.coupon);
                          this.payment(cart.coupon);
                        }}
                        style={{
                          backgroundColor: color.yellow,
                          shadowColor: color.yellow,
                          // borderRadius: '8px',
                        }}
                      >
                        <div
                          style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                        >
                          <div
                            className={
                              'Loading' + (this.state.loading ? ' loading' : '')
                            }
                          />
                          Bayar
                        </div>
                      </Button>
                    </>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className="card">
                <h1>Rincian Belanja</h1>
                <h4>Silahkan cek kembali belanjaan Anda</h4>
                {this.props.cartStore!.items.length === 0 ? (
                  <div
                    style={{
                      flex: 1,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <h2>Maaf anda tidak punya barang di keranjang</h2>
                  </div>
                ) : (
                  <ListItem />
                )}
              </div>
            </div>
            <div>
              <div className="card">
                <h1>Pengiriman</h1>
                <h4>Silahkan pilih salah satu metode pengiriman di bawah</h4>
                <div style={{ flex: 1, padding: '16px' }}>
                  {this.state.shipping && (
                    <React.Fragment>
                      <p>JNE</p>
                      <InputDropdown
                        placeholder="Shipping"
                        options={this.state.shipping.options[0].costs}
                        value={this.state.shippingMethod}
                        valueBy="service"
                        handleChange={async val => {
                          let price = await this.state.shipping!.options[0].costs.find(
                            searchVal => searchVal.service === val
                          );
                          await this.setState({
                            shippingMethod: val,
                            shippingCost: price!.cost[0].value,
                          });
                        }}
                      />
                    </React.Fragment>
                  )}
                </div>
              </div>
            </div>
            <div className="hide-on-tablet-and-desktop">
              <div style={{ width: '100%' }}>
                <div className="card">
                  <>
                    <div className="horizontal-section">
                      <div className="content">
                        <div className="horizontal">
                          <p>Subtotal</p>
                          <p style={{ color: color.blue }}>
                            Rp {cart.totalPrice.toLocaleString()}
                          </p>
                        </div>
                        <div className="horizontal">
                          <p>Pengiriman</p>
                          <p style={{ color: color.blue }}>
                            Rp {this.state.shippingCost.toLocaleString()}
                          </p>
                        </div>

                        <div className="horizontal">
                          <p>Voucher</p>
                          <p style={{ color: color.blue }}>
                            {cart.coupon ? cart.coupon : '-'}
                          </p>
                        </div>
                        <div className="horizontal">
                          <h3 style={{ color: color.yellow }}>Total</h3>
                          <h3 style={{ color: color.yellow }}>
                            Rp{' '}
                            {(
                              cart.totalPrice + this.state.shippingCost
                            ).toLocaleString()}
                          </h3>
                        </div>
                      </div>
                    </div>
                    <Button
                      onClick={() => {
                        console.log(cart.coupon);
                        this.payment(cart.coupon);
                      }}
                      style={{
                        backgroundColor: color.yellow,
                        shadowColor: color.yellow,
                        // borderRadius: '8px',
                      }}
                    >
                      <div
                        style={{
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <div
                          className={
                            'Loading' + (this.state.loading ? ' loading' : '')
                          }
                        />
                        Bayar
                      </div>
                    </Button>
                  </>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

async function getAddresses() {
  try {
    const res = await client.get<Array<address>>('/addresses');
    const addresses = res.data;
    return addresses;
  } catch (error) {
    throw error;
  }
}

const getProvince = async () => {
  try {
    const result = await client.get<Array<province>>('/region/provinsi');
    return result.data;
  } catch (error) {
    throw error;
  }
};

const getDistrict = async (province_id: number) => {
  try {
    const result = await client.get<Array<province>>(
      `/region/kabupaten/${province_id}`
    );
    return result.data;
  } catch (error) {
    throw error;
  }
};

const getSubDistrict = async (district_id: number) => {
  try {
    const result = await client.get<Array<province>>(
      `/region/kecamatan/${district_id}`
    );
    return result.data;
  } catch (error) {
    throw error;
  }
};

const getPostCode = async (subDistrict_id: number) => {
  try {
    const result = await client.get<
      Array<{ id: number; name: string; zip_code: number }>
    >(`/region/kelurahan/${subDistrict_id}`);
    const data: Array<province> = result.data.map(val => {
      return {
        id: val.id,
        name: `${val.name} / ${val.zip_code}`,
      };
    });
    return data;
  } catch (error) {
    throw error;
  }
};

const getPaymentType = async () => {
  try {
    const result = await client.get<Array<paymentType>>('/payment/types');
    return result.data;
  } catch (error) {
    throw error;
  }
};

const checkOut = async (
  payment_type: number,
  address: number,
  courier_option: string,
  coupon_code?: string
) => {
  try {
    const reqData: {
      payment_type: number;
      address: number;
      courier_option: string;
      courier: string;
      coupon_code?: string;
      platform: string;
    } = {
      payment_type: payment_type,
      address: address,
      courier: 'jne',
      courier_option: courier_option,
      platform: 'web',
    };
    if (coupon_code) reqData.coupon_code = coupon_code;
    const result = await client.post('/ordervt', reqData);
    return result.data;
  } catch (error) {
    throw error;
  }
};

const getShippingList = async (address: number) => {
  try {
    const result = await client.get(
      `/shipping/list?address=${address}&courier=jne`
    );
    return result.data;
  } catch (error) {
    throw error;
  }
};

const addAddress = async ({
  recipient_name,
  address,
  province,
  regency,
  district,
  village,
  zip_code,
  phone_number,
}: {
  recipient_name: string;
  address: string;
  province: string;
  regency: string;
  district: string;
  village: string;
  zip_code: string;
  phone_number: string;
}) => {
  try {
    const result = await client.post<address>('/address', {
      recipient_name,
      address,
      province,
      regency,
      district,
      village,
      zip_code,
      phone_number,
    });
    return result.data;
  } catch (error) {
    throw error;
  }
};

export default props => (
  <FetchComponent
    doBeforeRender={async () => {
      try {
        return {};
      } catch (error) {
        throw error;
      }
    }}
  >
    <Checkout {...props} />
  </FetchComponent>
);
