import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { RouteComponentProps } from 'react-router';
import { validateStudentLoginInput,
         validateNormalLoginInput } from '../libs/validate'
import UserStore from '../stores/UserStore';
import '../style/Login.scss';
import swal from 'sweetalert';
import Button from '../components/Button';
import client from '../libs/client';

interface State {
  email: string;
  password: string;
  token_murid: string;
  error: string;
  loadingSchool: boolean;
  loadingStudent: boolean;
  loading: boolean;
  fbLoading: boolean;
  requestingPassword: boolean;
  page: string;
}

@inject('userStore')
@observer
export default class Login extends Component<
  { userStore?: UserStore } & RouteComponentProps,
  State
> {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      token_murid: '',
      error: '',
      loadingSchool: false,
      loadingStudent: false,
      loading: false,
      fbLoading: false,
      requestingPassword: false,
      page: 'keluarga'
    };
  }

  loginParent = async () => {
    const { email, password } = this.state;
    let error = validateNormalLoginInput(email, password);

    this.setState(prevState => {
      return { loading: !prevState.loading };
    });

    if(error.length === 0) {
      try {
        const res = await client.post('/login', {
          email,
          password,
        });
        const { access_token } = res.data;
        await this.props.userStore!.setToken(access_token);
        this.props.history.replace('/pilih_akun');
        
      } catch (error) {
        console.log(error.response.data);
        // untuk sekarang, error handling seperti ini karena return result dari backend seperti dibawah
        if(error.response.data.status === 'Access Denied'){
          swal('Perhatian!', 'Anda tidak bisa menggunakan akun ini untuk login di fitur ini', 'error');
        }

        if(error.response.data.status === 'Unauthorized'){
          swal('Perhatian!', 'Login gagal. Pastikan anda telah verifikasi terlebih dahulu', 'error');
        }

        if(error.response.data.status === 'NotFound'){
          swal('Perhatian!', 'Email atau password salah! Silahkan coba lagi', 'error');
        }
      }
    } else {
      this.setState({ error });
    }

    setTimeout(() => {
      this.setState(prevState => {
        return { loading: !prevState.loading };
      });

    }, 3000);
  };

  loginSchool = async () => {
    const { email, password } = this.state;
    let error = validateNormalLoginInput(email, password);

    this.setState(prevState => {
      return { loadingSchool: !prevState.loadingSchool };
    });
    
    if(error.length === 0) {
      try {
        const res = await client.post('/login/school', {
          email,
          password,
        });

        const { type, access_token } = res.data.Data;
        await this.props.userStore!.setToken(access_token);

        if (type === 'admin_sekolah') {
          this.props.history.replace('/dashboard_admin_sekolah');
        } else if (type === 'guru') {
          this.props.history.replace('/dashboard_guru');
        }
        
      } catch (error) {
        console.log(error.response.data);
        // untuk sekarang, error handling seperti ini karena return result dari backend seperti dibawah
        if(error.response.data.message === 'Email atau Password anda salah'){
          swal('Perhatian!', 'Email atau password salah! Silahkan coba lagi', 'error');
        }

        if(error.response.data.message === 'Subscription Needed to Access'){
          swal('Subscription Dibutuhkan!', 'Akun sekolah anda belum terdaftar dalam subscription. Silahkan subscribe terlebih dahulu', 'error');
        }

        if(error.response.data.message === 'Subcription Expired'){
          swal('Subscription Expired!', 'Subscription sekolah anda telah expired. silahkan hubungi pihak kami untuk lebih lanjutnya', 'error');
        }
      }
    } else {
      this.setState({ error });
    }

    setTimeout(() => {
      this.setState(prevState => {
        return { loadingSchool: !prevState.loadingSchool };
      });
    }, 3000);
  }

  loginStudent = async () => {
    const { token_murid } = this.state
    let error = validateStudentLoginInput(token_murid);

    this.setState(prevState => {
      return { loadingStudent: !prevState.loadingStudent };
    });

    if(error.length === 0) {
      try {
        const res = await client.post('/login/murid', { token: token_murid });
        const { access_token } = res.data.Data;
        console.log(res.data.Data);
        await this.props.userStore!.setToken(access_token);
        this.props.history.replace('/dashboard_murid');

      } catch (error) {
        console.log(error.response);
        // untuk sekarang, error handling seperti ini karena return result dari backend seperti dibawah
        if(error.response.data.message === 'Your School Subcription Expired'){
          swal('Token Tidak Bisa Diakses!', 'Saat ini tokenmu tidak bisa digunakan. Hubungi guru kelas atau admin sekolahmu untuk keterangan lebih lanjut', 'error');
        }

        if(error.response.data.message === 'Wrong Credentials'){
          swal('Tokenmu Salah!', 'Sepertinya kamu menggunakan token yang salah. Silahkan tanya ke guru kamu mengenai tokenmu', 'error');
        }
      }
    } else {
      this.setState({ error });
    }
    
    setTimeout(() => {
      this.setState(prevState => {
        return { loadingStudent: !prevState.loadingStudent };
      });
    }, 3000);
  }

  validate = (loginType) => {
    const { email, password } = this.state;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if (!email && !password) {
      this.setState({ error: 'both' });
    } else if (!password) {
      this.setState({ error: 'password' });
    } else if (!email || !re.test(String(email).toLowerCase())) {
      this.setState({ error: 'email' });
    } else {
      switch (loginType) {
        case "parent":
          this.loginParent();
          break;
        case "school":
          this.loginSchool();
          break;
        case "student":
          this.loginStudent();
          break;
        default:
          break;
      }
      
      this.setState({ error: '' });
    }
  };

  // loginFB = async () => {
  //   this.setState(prevState => {
  //     return { fbLoading: !prevState.fbLoading };
  //   });
  //   try {
  //     // const url = await getFBLoginURL();
  //     const url = '';
  //     this.props.history.push(url);
  //   } catch (error) {
  //     console.log(error.response.data);
  //   }
  // };

  requestChangePassword = async () => {
    if (this.state.email !== '') {
      this.setState({ requestingPassword: true });
      try {
        const res = await client.post('/password/forgot', {
          email: this.state.email,
        });
        const data = res.data;
        swal('Success', data.status, 'success');
      } catch (error) {
        console.log(error.response.data);
      }
      this.setState({ requestingPassword: false });
    } else {
      swal('Perhatian!', 'Silahkan isi field email terlebih dahulu', 'info');
    }
  };

  changePage = () => {
    const { page } = this.state;

    if(page === "keluarga") {
      this.setState({ page: "sekolah", email: '', password: '' });
    } else {
      this.setState({ page: "keluarga", email: '', password: '' });
    }
  }

  render() {
    const { email, password, error, page, token_murid} = this.state;
    return (
      
      <div className="login">
        <div className="login-con">
          {page === 'keluarga' ? (
            <div className="login-con-table">
              <div className="login-con-table-left">
                <h2 className="login-con-table-left-title">Akun Keluarga</h2>
                <input
                  value={email}
                  className={
                    'Input' +
                    (error.includes('email') || error.includes('both')
                      ? ' error'
                      : '')
                  }
                  type="email"
                  placeholder="Email"
                  onChange={e => this.setState({ email: e.target.value })}
                  onSubmit={() => console.log('submit')}
                  onKeyDown={key => {
                    if (key.key === 'Enter') {
                      const x = document.getElementById('pass');
                      x!.focus();
                    }
                  }}
                />
                <p
                  className={
                    'Error' + (error === 'email' || error === 'both' ? ' show' : '')
                  }
                >
                  {error === 'email' || error === 'both'
                    ? 'Email yang dimasukkan tidak tepat'
                    : ''}
                </p>
                <input
                  id="pass"
                  className={
                    'Input' +
                    (error.includes('password') || error.includes('both')
                      ? ' error'
                      : '')
                  }
                  type="password"
                  value={password}
                  placeholder="Kata Sandi"
                  onChange={e => this.setState({ password: e.target.value })}
                  onSubmit={() => console.log('submit')}
                  onKeyDown={key => {
                    if (key.key === 'Enter') {
                      const x = document.getElementById('pass');
                      x!.blur();
                      this.loginParent();
                    }
                  }}
                />
                <p
                  className={
                    'Error' +
                    (error === 'password' || error === 'both' ? ' show' : '')
                  }
                >
                  {error === 'password' || error === 'both'
                    ? 'Password yang dimasukkan tidak tepat'
                    : ''}
                </p>
                <Button
                  onClick={this.loginParent}
                >
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <div
                      className={'Loading' + (this.state.loading ? ' loading' : '')}
                    />
                    Masuk
                  </div>
                </Button>
                <p className="text">
                  Belum punya akun? &nbsp;
                  <a href="/register" style={{ textDecoration: 'underline' }}>
                    Daftar Sekarang
                  </a>
                </p>
                <p className="text">
                  <a
                    onClick={
                      !this.state.requestingPassword
                        ? this.requestChangePassword
                        : () => {}
                    }
                  >
                    <div
                      className={
                        'Loading' + (this.state.requestingPassword ? ' loading' : '')
                      }
                    />
                    Lupa Password
                  </a>
                </p>
              </div>

              <div className="login-con-table-left">
                <h2 className="login-con-table-left-title">
                  Akun Sekolah
                </h2>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                  <img style={{ width: '300px', height: 'auto' }} src={`${require("../assets/register1.png")}`}/>
                </div>
                <Button onClick={this.changePage}>
                  Masuk
                </Button>
              </div>
            </div>
          ) : (
            <div className="login-con-table">
              <div className="login-con-table-left">
                <h2 className="login-con-table-left-title">Akun Sekolah</h2>
                <h2 className="login-con-table-left-subtitle">Guru/Admin</h2>
                
                <input
                  value={email}
                  className={
                    'Input' +
                    (error.includes('email') || error.includes('both')
                      ? ' error'
                      : '')
                  }
                  type="email"
                  placeholder="Email"
                  onChange={e => this.setState({ email: e.target.value })}
                  onSubmit={() => console.log('submit')}
                  onKeyDown={key => {
                    if (key.key === 'Enter') {
                      const x = document.getElementById('pass');
                      x!.focus();
                    }
                  }}
                />
                <p className={ 'Error' + (error === 'email' || error === 'both' ? ' show' : '')}>
                  {error === 'email' || error === 'both'
                    ? 'Email yang dimasukkan tidak tepat'
                    : ''}
                </p>

                <input
                  id="pass"
                  className={
                    'Input' +
                    (error.includes('password') || error.includes('both')
                      ? ' error'
                      : '')
                  }
                  type="password"
                  value={password}
                  placeholder="Kata Sandi"
                  onChange={e => this.setState({ password: e.target.value })}
                  onSubmit={() => console.log('submit')}
                  onKeyDown={key => {
                    if (key.key === 'Enter') {
                      const x = document.getElementById('pass');
                      x!.blur();
                      this.loginSchool();
                    }
                  }}
                />
                <p className={ 'Error' + (error === 'password' || error === 'both' ? ' show' : '') } >
                  {error === 'password' || error === 'both'
                    ? 'Password yang dimasukkan tidak tepat'
                    : ''}
                </p>

                <Button onClick={this.loginSchool}>
                  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', }} >
                    <div className={'Loading' + (this.state.loadingSchool ? ' loading' : '')} />
                    Masuk
                  </div>
                </Button>
                <p className="text">
                  Ingin mendaftarkan sekolah anda ? &nbsp;
                  <a href="/register" style={{ textDecoration: 'underline' }}>
                    Kontak Kami
                  </a>
                </p>
                <p className="text">
                  <a onClick={ !this.state.requestingPassword ? this.requestChangePassword : () => {} }>
                    <div className={ 'Loading' + (this.state.requestingPassword ? ' loading' : '') } />
                    Lupa Password
                  </a>
                </p>

                <p className="text">
                  <a onClick={ this.changePage }>
                    Kembali ke login Keluarga
                  </a>
                </p>
              </div>

              <div className="login-con-table-left">
                <h2 className="login-con-table-left-title">Akun Sekolah</h2>
                <h2 className="login-con-table-left-subtitle">Murid</h2>
                
                <input
                  value={token_murid}
                  className={ 'Input' + (error.includes('token_murid') ? ' error': '') }
                  type="text"
                  placeholder="Token Murid"
                  onChange={e => this.setState({ token_murid: e.target.value })}
                  onSubmit={() => console.log('submit')}
                  onKeyDown={key => {
                    if (key.key === 'Enter') {
                      this.loginStudent;
                    }
                  }}
                />

                <p className={  'Error' + (error === 'token_murid'  ? ' show' : '')}>
                  {error === 'token_murid'
                    ? 'Token kelas yang dimasukkan tidak tepat'
                    : ''}
                </p>

                <Button onClick={this.loginStudent} >
                  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', }} >
                    <div className={'Loading' + (this.state.loadingStudent ? ' loading' : '')} />
                    Masuk
                  </div>
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
