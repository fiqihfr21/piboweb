import React, { useState } from 'react';
import swal from '@sweetalert/with-react';
import { cartItem } from '../libs/types';
import { inject, observer } from 'mobx-react';
import CartStore from '../stores/CartStore';
import Button from '../components/Button';
import { RouteComponentProps } from 'react-router';
const color = require('../style/colors.scss');
import '../style/Cart.scss';
import client from '../libs/client';

interface ListItemT {
  items: Array<cartItem>;
  deleteItem(book_id: number): void;
  cartStore?: CartStore;
}

const ListItem = inject('cartStore')(
  observer(({ items, deleteItem, cartStore }: ListItemT) => (
    <div className="List-Item-Container">
      {items.map((val, idx) => (
        <div className="item" key={idx}>
          <div style={{ width: '150px' }}>
            <img src={val.cover_image} />
          </div>

          <div style={{ flex: 1, marginLeft: '8px', position: 'relative' }}>
            <a
              style={{
                fontSize: '56px',
                color: color.red,
                cursor: 'pointer',
                position: 'absolute',
                right: 0,
              }}
              onClick={async () => {
                await deleteItem(val.id);
              }}
            >
              &times;
            </a>
            <p className="title">{val.name}</p>
            <p className="type">{val.type}</p>
            <p className="price">
              Rp{' '}
              {parseFloat(
                val.sales_price ? val.sales_price : val.regular_price
              ).toLocaleString()}
            </p>
            <div
              style={{
                fontSize: '56px',
                color: color.red,
                cursor: 'pointer',
                position: 'absolute',
                right: 0,
                bottom: 16,
              }}
            >
              <Unit
                add={async () => {
                  await cartStore!.updateItem(val.id, val.quantity + 1);
                }}
                min={async () => {
                  await cartStore!.updateItem(val.id, val.quantity - 1);
                }}
                value={val.quantity}
              />
            </div>
          </div>
        </div>
      ))}
    </div>
  ))
);

const Unit = ({
  add,
  min,
  value,
}: {
  add(e): void;
  min(e): void;
  value: number;
}) => (
  <div className="Unit">
    <div className="input-group input-number-group">
      <div className="input-group-button" onClick={value > 1 ? min : () => {}}>
        <span className="input-number-decrement">-</span>
      </div>
      <input
        className="input-number"
        type="number"
        value={value}
        min="0"
        max="1000"
        onChange={() => {}}
      />
      <div className="input-group-button" onClick={add}>
        <span className="input-number-increment">+</span>
      </div>
    </div>
  </div>
);

const Voucher = ({ setCoupon, setTotalPrice }) => {
  const [code, setCode] = useState('');
  const [checking, setChecking] = useState(false);
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <label>Kode voucher</label>
      <input
        className="Input"
        placeholder="Tulis kode voucher disini"
        onChange={e => setCode(e.target.value)}
      />
      <div style={{ height: 8 }} />
      <Button
        onClick={async () => {
          if (code !== '') {
            setChecking(true);
            try {
              let res = await client.post('/coupon/check', {
                coupon: code,
                shoptype: 'eShop',
              });
              console.log(res.data);
              setCoupon(code);
              setTotalPrice(res.data.new_total);
              swal('Sukses', 'Voucher berhasil dipakai', 'success');
            } catch (error) {
              const { status } = error.response.data;
              swal('Perhatian!', status, 'error');
            }
            setChecking(false);
          }
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <div className={checking ? 'loading' : ''} />
          Check
        </div>
      </Button>
    </div>
  );
};

@inject('cartStore')
@observer
class Cart extends React.Component<
  { cartStore?: CartStore } & RouteComponentProps,
  { checking: boolean,
    coupon: string,
    totalPrice: number }
> {
  state = {
    checking: false,
    coupon: '',
    totalPrice: 0
  };

  async componentDidMount() {
    // const cart = await getCart();
    // this.setState({ cart: cart });
  }

  deleteItem = async (book_id: number) => {
    try {
      await swal({
        title: 'Perhatian',
        text: 'Apakah Anda yakin ingin menghapus buku ke keranjang?',
        buttons: {
          cancel: 'Batal',

          confirm: {
            text: 'Lanjutkan',
            value: 'ok',
          },
        },
      }).then(async value => {
        switch (value) {
          case 'ok':
            await this.props.cartStore!.deleteItem(book_id);
            break;
        }
      });
    } catch (error) {
      throw error;
    }
  };

  _checkVoucher = async () => {
    swal({
      buttons: false,
      content: (
        <Voucher
          setCoupon={coupon => this.setState({ coupon })}
          setTotalPrice={totalPrice => this.setState({ totalPrice })}
        />
      ),
    });
  };

  render() {
    const { totalPrice, coupon } = this.state;
    return (
      <div className="Container">
        <div className="Cart-Page-Content">
          <div className="content-container">
            <div className="Header">
              <span>Book</span>
            </div>
            <div className="horizontal">
              <div className="section left">
                <div className="card">
                  {this.props.cartStore!.items.length === 0 ? (
                    <div
                      style={{
                        flex: 1,
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <h2 style={{ textAlign: "center", color: "rgb(75, 191, 209)", fontFamily: "Montserrat" }}>Maaf, tidak ada produk di keranjang.<br /><br />Silahkan kembali belanja.</h2>
                    </div>
                  ) : (
                    <ListItem
                      items={this.props.cartStore!.items}
                      deleteItem={this.deleteItem}
                    />
                  )}
                </div>
              </div>
              <div className="section right">
                <div style={{ width: '100%' }}>
                  <div className="card">
                    <div className="horizontal-section">
                      <div className="content">
                        <p>Coupon</p>
                      </div>
                      <div
                        className="content"
                        style={{ alignItems: 'flex-end' }}
                      >
                        <p style={{ color: color.blue }}>
                          {coupon ? (`${coupon}`) : (`-`)}
                        </p>
                      </div>
                    </div>
                    <div className="horizontal-section">
                      <div className="content">
                        <h3 style={{ color: color.yellow }}>Total</h3>
                      </div>
                      <div
                        className="content"
                        style={{ alignItems: 'flex-end' }}
                      >
                        <h3 style={{ color: color.yellow }}>
                          {totalPrice !== 0 ? (`Rp ${totalPrice.toLocaleString()}`) : (`Rp ${this.props.cartStore!.totalPrice.toLocaleString()}`)}
                        </h3>
                      </div>
                    </div>
                    <Button onClick={this._checkVoucher}>
                      <div
                        style={{ display: 'flex', justifyContent: 'center' }}
                      >
                        Apply Voucher
                      </div>
                    </Button>
                    <div style={{ height: '8px' }} />
                    <Button
                      onClick={() => {
                        this.props.cartStore!.items.length !== 0
                          ? this.props.history.push('/checkout')
                          : swal(
                              'Perhatian!',
                              'Silahkan tambah buku ke keranjang terlebih dahulu',
                              'info'
                            );
                      }}
                    >
                      <div
                        style={{ display: 'flex', justifyContent: 'center' }}
                      >
                        Check Out
                      </div>
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Cart;
