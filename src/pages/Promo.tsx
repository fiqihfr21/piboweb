import React, { Component } from 'react';
import client from '../libs/client';
import '../style/Container.scss';
import '../style/Grid.scss';
import LoadMore from '../components/LoadMore';
import Button from '../components/Button';
import { book } from '../libs/types';
import BookCard from '../components/BookCard';
import EBookCard from '../components/EBookCard';
import FetchComponent from '../components/FetchComponent';
const color = require('../style/colors.scss');
import qs from 'querystring';

class Promo extends Component<{ type: string; slug: string; from: string }> {
  async componentDidMount() {
    try {
      const { slug, type, from = 'promo' } = this.props;
      console.log(slug, type, from);
      const result = await client.get(`/${from}/${slug}/${type}/0`);
      console.log(`/${from}/${slug}/${type}/0`);
      console.log(result.data);
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { slug, type, from = 'promo' } = this.props;
    return (
      <div className="Container">
        <style>{`
          body, .App-Content { background-color: rgb(56, 193, 222);}
          body {
            background: ${
              type.toLowerCase() === 'eshop' ? color.yellow : '#4bbfd1'
            };
          }
          .Header {
            color: ${color.blue};
            border-bottom: 1px solid ${color.red};
            padding: 8px;
            margin: 0 80px;
            margin-top: 16px;
          }
          @media (max-width: 767px) {
            .Header {
              margin: 0;
              margin-top: 85px;
            }
          }
        `}</style>
        <LoadMore
          url={`/${from}/${slug}/${type}`}
          container={props => (
            <>
              <div className="Header">
                <span>
                  {type} > {slug}
                </span>
              </div>
              <div className="Grid">
                <div className="grid-row">{props.children}</div>
              </div>
            </>
          )}
          LoaderButton={
            type.toLowerCase() === 'eshop' &&
            function(props) {
              return (
                <Button
                  {...props}
                  style={{
                    backgroundColor: color.lightBlue,
                    shadowColor: color.darkBlue,
                  }}
                />
              );
            }
          }
          renderItems={(item: book) => {
            return (
              <div
                className="grid-item"
                key={item.id + item.name + Math.random()}
              >
                {type.toLowerCase() === 'eshop' ? (
                  <BookCard
                    key={item.id}
                    bookId={item.id}
                    image={
                      item.cover_image
                        ? item.cover_image
                        : require('../assets/placeholder.png')
                    }
                    price_1={item.regular_price}
                    price={item.sales_price}
                    title={item.name}
                    type={item.type}
                  />
                ) : (
                  <EBookCard item={item} />
                )}
              </div>
            );
          }}
        />
      </div>
    );
  }
}

export default props => (
  <FetchComponent
    doBeforeRender={async () => {
      try {
        const { slug, type, from } = qs.parse(props.location.search.substr(1));
        if (!slug && !type) {
          props.history.replace('/');
        }
        return { slug, type, from };
      } catch (error) {
        throw error;
      }
    }}
  >
    <Promo {...props} />
  </FetchComponent>
);
