import React, { Component } from 'react';
import '../style/Subscribe.scss';
import { Subscription } from '../libs/types';
import client from '../libs/client';
import { RouteComponentProps } from 'react-router';
import FetchComponent from '../components/FetchComponent';
const color = require('../style/colors.scss');

class Subscribe extends Component<
  {
    data: Array<Subscription>;
  } & RouteComponentProps
> {
  render() {
    return (
      <div className="Subscribe-Container">
        <div className="blue-background"/>
        <div className="btn-top">
          <div
            className="child"
            style={{
              paddingRight: 16,
            }}
          >
            <div
              className="child-content"
              onClick={() => this.props.history.push('/elibrary')}
            >
              <img
                height={40}
                width={40}
                src={require('../assets/navigation/eLibrary.png')}
              />
              <h2>eLibrary</h2>
            </div>
          </div>
        </div>
        <div className="sub-top">
          <h3>Akses instan ke beragam buku anak, baca sepuasnya!</h3>
          <p>
            Pilih paket yang sesuai dengan kebutuhan. Paket dapat diganti kapan
            pun.
          </p>
        </div>
        <div className="Grid">
          <div className="grid-row">
            {this.props.data.map((item, index) => {
              return (
                <div className="grid-item" key={index}>
                  <div className={'subscribe-item' + (index === 1 ? "-active" : "")}>
                    <div className="subscribe-item-content">
                      <h4 className="name">{item.name}</h4>
                      <h4 className="price1">
                        IDR {parseFloat(item.price).toLocaleString()}
                      </h4>
                      <h4 className="price">
                        IDR {parseFloat(item.regular_price).toLocaleString()}
                      </h4>
                      <p className="desc" style={{ color: color.blue, margin: '4px' }}>
                        {item.promo}
                      </p>
                      <p className="desc" style={{ color: color.red, margin: '4px' }}>
                      {item.hemat}
                      </p>

                      <ul className="desc">
                        {item.desc.map(text => (
                          <li>{text}</li>
                        ))}
                      </ul>
                      <div className="btn-container">
                        <a
                          onClick={() => {
                            this.props.history.push({
                              pathname: '/checkout-subscription',
                              state: {
                                ...item,
                              },
                              // `/checkout-subscription?id=${item.id}&name=${
                              //   item.name
                              // }&price=${item.regular_price}`,
                            });
                          }}
                        >
                          Langganan
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="sub-bottom">
          <h3>PiBo eLibrary memiliki tawaran khusus untuk Sekolah/Instansi</h3>
          <a href={"mailto:halo@bacapibo.com"}>Kontak Kami</a>
        </div>
      </div>
    );
  }
}

export default props => (
  <FetchComponent
    doBeforeRender={async () => {
      try {
        const res = await client.get('/paketsubscribe');
        return { data: res.data };
      } catch (error) {
        throw error;
      }
    }}
  >
    <Subscribe {...props} />
  </FetchComponent>
);
