import React from 'react';
import { RouteComponentProps } from 'react-router';
import FetchComponent from '../components/FetchComponent';
import qs from 'querystring';
import { inject, observer } from 'mobx-react';
import FooterStore, { FooterData } from '../stores/FooterStore';
import { toJS } from 'mobx';
import '../style/Page.scss';

const Page = (props: { data?: FooterData } & RouteComponentProps) => {
  return <div className="page-content" dangerouslySetInnerHTML={{ __html: props.data!.deskripsi }} />;
};

export default inject('footerStore')(
  observer((props: { footerStore?: FooterStore } & RouteComponentProps) => (
    <FetchComponent
      watch={props.location.search.substr(1)}
      doBeforeRender={async () => {
        try {
          const { q } = qs.parse(props.location.search.substr(1));
          if (!q) {
            props.history.replace('/');
            return {};
          }
          await props.footerStore!.getFooterData();
          const raw = toJS(props.footerStore!.data);
          const data = raw.filter(v => v.slug === q);
          return { data: data[0] };
        } catch (error) {
          throw error;
        }
      }}
    >
      <Page {...props} />
    </FetchComponent>
  ))
);
