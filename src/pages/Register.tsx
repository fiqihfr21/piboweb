import '../style/Register.scss';
import React from 'react';
import Button from '../components/Button';
import { RouteComponentProps } from 'react-router';
import client from '../libs/client';
const color = require('../style/colors.scss');

interface State {
  name: string;
  email: string;
  password: string;
  confirmPassword: string;
  error: object;
  loading: boolean;
}

export default class Register extends React.Component<
  RouteComponentProps,
  State
> {
  state = {
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
    error: {
      name: null,
      email: null,
      password: null,
    },
    loading: false,
  };

  register = async () => {
    this.setState(prevState => {
      return {
        loading: !prevState.loading,
        error: {
          name: null,
          email: null,
          password: null,
        },
      };
    });

    try {
      const { email, name, password, confirmPassword } = this.state;
      const registrationData = {
        email: email,
        password: password,
        password_confirmation: confirmPassword,
        name: name,
      };
      const registerData = await client.post('/register', registrationData);
      console.log(registerData);
      this.setState(prevState => {
        return { loading: !prevState.loading };
      });
      console.log(registerData);
      this.setState(prevState => {
        return { loading: !prevState.loading };
      });
      this.props.history.replace('/check_email');
      // Router.replace('/check_email');
    } catch (error) {
      this.setState(prevState => {
        return {
          loading: !prevState.loading,
          error: error.response.data.errors,
        };
      });
      console.log(error.response.data);
    }
  };

  render() {
    const { error, name, email, password, confirmPassword } = this.state;
    return (
        <div className="form">
          <div className="form-box">
              <h2 className="form-box-title" style={{ color: color.blue }}>
                Buat Akun Baru
              </h2>
              {/*NAME*/}
              <input
                value={name}
                className={'Input' + (error.name ? ' error' : '')}
                type="text"
                placeholder="Masukkan nama"
                onChange={e => this.setState({ name: e.target.value })}
                onKeyDown={key => {
                  if (key.key === 'Enter') {
                    document.getElementById('email')!.focus();
                  }
                }}
              />
              <p className={'Error' + (error.name ? ' show' : '')}>
                {error.name && error.name![0]}
              </p>
              {/*EMAIL*/}
              <input
                value={email}
                className={'Input' + (error.email ? ' error' : '')}
                id="email"
                type="email"
                placeholder="Masukkan email"
                onChange={e => this.setState({ email: e.target.value })}
                onKeyDown={key => {
                  if (key.key === 'Enter') {
                    document.getElementById('pass')!.focus();
                  }
                }}
              />
              <p className={'Error' + (error.email ? ' show' : '')}>
                {error.email && error.email![0]}
              </p>
              {/*PASSWORD*/}
              <input
                value={password}
                placeholder="Buat kata sandi"
                className={
                  'Input' +
                  // @ts-ignore
                  (error.password && !error.password[0].includes('confirmation')
                    ? ' error'
                    : '')
                }
                id="pass"
                type="password"
                onChange={e => this.setState({ password: e.target.value })}
                onKeyDown={key => {
                  if (key.key === 'Enter') {
                    document.getElementById('confirmPass')!.focus();
                  }
                }}
              />
              <p
                className={
                  'Error' +
                  // @ts-ignore
                  (error.password && !error.password[0].includes('confirmation')
                    ? ' show'
                    : '')
                }
              >
                {error.password &&
                  // @ts-ignore
                  !error.password[0].includes('confirmation') &&
                  error.password}
              </p>
              {/*CONFIRM PASSWORD*/}
              <input
                value={confirmPassword}
                className={
                  'Input' +
                  (error.password &&
                  // @ts-ignore
                  (error.password[0].includes('confirmation') ||
                    // @ts-ignore
                    error.password[0].includes('password field is required'))
                    ? ' error'
                    : '')
                }
                id="confirmPass"
                type="password"
                placeholder="Masukkan kembali kata sandi"
                onChange={e => this.setState({ confirmPassword: e.target.value })}
                onKeyDown={key => {
                  if (key.key === 'Enter') {
                    document.getElementById('pass')!.blur();
                    this.register();
                  }
                }}
              />
              <p
                className={
                  'Error' +
                  (error.password &&
                  // @ts-ignore
                  (error.password[0].includes('confirmation') ||
                    // @ts-ignore
                    error.password[0].includes('password field is required'))
                    ? ' show'
                    : '')
                }
              >
                {error.password &&
                  // @ts-ignore
                  (error.password[0].includes('confirmation') ||
                    // @ts-ignore
                    error.password[0].includes('password field is required')) &&
                  error.password![0]}
              </p>
              <br />
              <Button onClick={this.register}>
                <div>
                  <div
                    className={'Loading' + (this.state.loading ? ' loading' : '')}
                  />
                  Daftar
                </div>
              </Button>
              <p className="text">Sudah punya akun? <a href="/login">Masuk</a></p>
              <p className="text">Dengan mendaftar. Anda telah setuju dengan <a href="/page?q=syarat-dan-ketentuan">Syarat & Ketentuan</a></p>
            </div>
          </div>
    );
  }
}
