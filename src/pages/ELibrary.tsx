import React, { Component } from 'react';
import FetchComponent from '../components/FetchComponent';
import client from '../libs/client';
import { books, ELibraryDataType, book } from '../libs/types';
import '../style/ELibrary.scss';
import Carousel from '../components/Carousel';
import { inject, observer } from 'mobx-react';
import UserStore from '../stores/UserStore';
import Slider from '../components/Slider';
import LoadMoreLib from '../components/LoadMoreLib';
import EBookCard from '../components/EBookCard';

interface Props {
  books: books;
  data: ELibraryDataType;
  userStore?: UserStore;
}
interface State {
  isFilterActive: boolean;
  age: Array<number>;
  format: Array<number>;
  theme: Array<number>;
  isReading: boolean;
  current: string;
  filtered: boolean;
}

@inject('userStore')
@observer
class ELibrary extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      isFilterActive: false,
      age: [],
      format: [],
      theme: [],
      isReading: false,
      current: '',
      filtered: false,
    };
  }

  applyFilter = async () => {
    try {
      const { age, format, theme } = this.state;
      if (age.length !== 0 || format.length !== 0 || theme.length !== 0) {
        if (this.state.filtered) {
          await this.setState({ filtered: false });
        }
        this.setState({ filtered: true });
      } else {
        this.setState({ filtered: false });
      }
    } catch (error) {
      console.log(error);
    }
  };

  handleFilter = (state: 'format' | 'age' | 'theme', id: number) => {
    const include = this.state[state].includes(id);
    if (include) {
      // @ts-ignore
      this.setState(prev => {
        let removed = [...prev[state]];
        removed.splice(prev[state].indexOf(id), 1);
        return { [state]: [...removed] };
      });
      return;
    }
    // @ts-ignore
    this.setState(prev => ({
      [state]: [...prev[state], id],
    }));
  };

  getQuery = () => {
    const { format, age, theme } = this.state;
    let props: { age?: string; format?: string; themes?: string } = {};
    if (age.length !== 0) props.age = age.toString();
    if (format.length !== 0) props.format = format.toString();
    if (theme.length !== 0) props.themes = theme.toString();

    let query = '';
    if (props.format) query += `format=${props.format}`;
    if (props.age) {
      if (query !== '') query += '&';
      query += `age=${props.age}`;
    }
    if (props.themes) {
      if (query !== '') query += '&';
      query += `themes=${props.themes}`;
    }
    return query;
  };

  render() {
    return (
      <div className="Container">
        <Carousel banners={this.props.data.banner} />
        {!this.props.userStore!.token ? (
          <div className="No-Login">
            <div className="No-Login-Content">
              <img src={require('../assets/1.png')} />
              <h3>Pilih Sesuai Minat</h3>
              <p>Menambah rasa ingin tahu si Kecil.</p>
            </div>
            <div className="No-Login-Content">
              <img src={require('../assets/2.png')} />
              <h3>Akses Mudah</h3>
              <p>
                Dapat dibaca di semua perangkat.
              </p>
            </div>
            <div className="No-Login-Content">
              <img src={require('../assets/3.png')} />
              <h3>Baca Sepuasnya</h3>
              <p>
                Mulai dari Rp 80.000/bulan
              </p>
            </div>
          </div>
        ) : (
          <div style={{ background: 'rgb(244,244,244)' }}>
            {this.props.data.subscribe == 0 && (
              <div className="No-Login">
              <div className="btn-subscribe">
                <a href="/subscribe">Mulai Berlangganan</a>
              </div>
              </div>
            )}
            <div
              className="Top-Dropdown"
              onClick={() =>
                this.setState(prev => ({
                  isFilterActive: !prev.isFilterActive,
                }))
              }
            >
              <img
                src={require('../assets/navigation/gear.png')}
                width={40}
                height={40}
              />
              <h1>Mau baca buku apa hari ini?</h1>
              <div className="bottom-triangle" />
            </div>
            <div
              className={
                'Collapsable' + (this.state.isFilterActive ? ' active' : '')
              }
            >
              <div className="age-container">
                Pilih Umur /  Level Baca
                <div className="age-list">
                  {this.props.data.age.map(age => {
                    const include = this.state.age.includes(age.id);
                    return (
                      <div
                        style={{ whiteSpace: 'pre' }}
                        className={'age-item' + (include ? ' active' : '')}
                        key={age.id}
                        onClick={() => {
                          this.handleFilter('age', age.id);
                        }}
                      >
                        {age.data}
                      </div>
                    );
                  })}
                </div>
                <div className="age-list" style={{marginTop:10}}>
                  {this.props.data.age1.map(age => {
                    const include = this.state.age.includes(age.id);
                    return (
                      <div
                        style={{ whiteSpace: 'pre' }}
                        className={'age-item' + (include ? ' active' : '')}
                        key={age.id}
                        onClick={() => {
                          this.handleFilter('age', age.id);
                        }}
                      >
                        {age.data}
                      </div>
                    );
                  })}
                </div>
              </div>
              <div className="format-container">
                Pilih Format
                <div className="format-list">
                  {this.props.data.format.map(format => {
                    const include = this.state.format.includes(format.id);
                    return (
                      <div
                        className={'format-item' + (include ? ' active' : '')}
                        key={format.id}
                        onClick={() => {
                          this.handleFilter('format', format.id);
                        }}
                      >
                        {format.name}
                      </div>
                    );
                  })}
                </div>
              </div>
              <div className="theme-container">
                Pilih Tema
                <Slider
                  data={this.props.data.themes}
                  arrowColor={'#4bbfd1'}
                  renderItem={theme => {
                    const include = this.state.theme.includes(theme.id);
                    return (
                      <div
                        key={theme.id}
                        className={'theme-item' + (include ? ' active' : '')}
                        onClick={() => {
                          this.handleFilter('theme', theme.id);
                        }}
                      >
                        <img className="theme-logo" src={theme.image} />
                        <p className="theme-name">{theme.name}</p>
                      </div>
                    );
                  }}
                />
              </div>
              <div className="button-container">
                <div className="button" onClick={this.applyFilter}>
                  Lihat
                </div>
              </div>
            </div>
          </div>
        )}
        {this.state.filtered && (
          <LoadMoreLib
            url="/bookslug"
            query={this.getQuery()}
            container={props => (
              <div style={{ zIndex: 10, background: '#4bbfd1' }}>
                <div className="Grid">
                  <div className="grid-row">{props.children}</div>
                </div>
              </div>
            )}
            renderItems={(item: book) => {
              return (
                <div
                  className="grid-item"
                  key={item.id + item.name + Math.random()}
                >
                  <EBookCard item={item} />
                </div>
              );
            }}
          />
        )}
        {(this.state.filtered
          ? [...this.props.data.promo.slice(2, 5)]
          : [...this.props.data.promo]
        ).map((promo, index) => (
          <div
            className="Section"
            key={promo.data.id + promo.data.slug + index}
            style={
              index % 2 == 0
                ? { background: '#4bbfd1' }
                : {
                    backgroundImage: `url(${require('../assets/pattern_yellow.jpg')})`,
                  }
            }
          >
            <div className="section-child">
              <div className="title-container">
                <h2 className="title" style={{ textShadow: '2px 2px #00798e' }}>
                  {promo.data.name}
                </h2>
                <a
                  href={`/promolib?slug=${promo.data.slug}&type=${
                    promo.data.shop_id
                  }`}
                >
                  Lihat Semua
                </a>
              </div>
              <Slider
                data={promo.listbuku}
                arrowColor={'white'}
                renderItem={val => <EBookCard key={val.id} item={val} />}
              />
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default props => {
  return (
    <FetchComponent
      doBeforeRender={async () => {
        try {
          const data = client.get('/elibrary');
          const books = client.get(`/books`);
          const res = await Promise.all([data, books]);
          return {
            data: res[0].data,
            books: res[1].data.data,
          };
        } catch (error) {
          throw error;
        }
      }}
    >
      <ELibrary {...props} />
    </FetchComponent>
  );
};
