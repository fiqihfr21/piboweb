import React, { Component } from 'react';
import '../style/index.scss';
import { RouteComponentProps } from 'react-router';
import FetchComponent from '../components/FetchComponent';
import client from '../libs/client';
import { Link } from 'react-router-dom';
import Slider from '../components/Slider';
import { book } from '../libs/types';
const color = require('../style/colors.scss');
import { inject, observer } from 'mobx-react';
import ReaderStore from '../stores/ReaderStore';
import UserStore from '../stores/UserStore';
import BookCard from '../components/BookCard';
import EBookCard from '../components/EBookCard';

interface ELibraryDataType {
  data: { id: number; name: string; slug: string; shop_id: string };
  listbuku: book[];
}

@inject('readerStore', 'userStore')
@observer
class Index extends Component<
  {
    books: book[];
    data: book[];
    dataELibrary: ELibraryDataType[];
    readerStore?: ReaderStore;
    userStore?: UserStore;
  } & RouteComponentProps,
  { tab: string }
> {
  constructor(props) {
    super(props);
    this.state = {
      tab: 'elibrary',
    };
  }

  render() {
    return (
      <div className="Container">
        <div className="Top">
          <div
            style={{
              display: 'flex',
              padding: '0 10%',
              backgroundColor: color.blue,
            }}
          >
            <div
              style={{
                flex: 2,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                textAlign: 'left',
                zIndex: 10,
              }}
            >
              <h1 style={{ color: color.yellow, fontSize: 56 }}>
                Baca. Tumbuh. Kembang.
              </h1>
              <p style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>
                A Fun New Way of Reading
              </p>
              <p style={{ color: 'white', fontSize: 16 }}>
                PiBo hadir sebagai alternatif cerdas demi menyuguhkan konten digital yang positif untuk si Kecil. Seluruh buku-buku pada situs ini sudah melalui proses kurasi sehingga aman untuk si Kecil.
              </p>
              {/* <div>
                {!user.token && (
                  <SubmitButton
                    onClick={() => Router.push('/register')}
                    style={{
                      background: color.yellow,
                      padding: '4px 16px',
                      borderRadius: 8,
                    }}
                    value="Daftar"
                  />
                )}
              </div> */}
            </div>
            <div style={{ flex: 1.5 }}>
              <img
                className="content"
                src={require('../assets/asset-landing.png')}
              />
            </div>
          </div>
          <img src={require('../assets/cloud.png')} className={'cloud'} />
        </div>
        <div className="Middle">
          <h1 style={{ textAlign: 'center', marginTop: 16 }}>Pilih Layanan</h1>
          <div className="content">
            <div
              className="child"
              style={{
                justifyContent: 'flex-end',
                paddingRight: 16,
              }}
            >
              <div
                className={
                  'child-content'
                  // + (this.state.tab == 'elibrary' ? ' active' : '')
                }
                onClick={() => this.props.history.push('/elibrary')}
              >
                <img
                  height={80}
                  width={80}
                  src={require('../assets/navigation/eLibrary.png')}
                />
                <div style={{ marginLeft: 16 }}>
                  <h2 style={{ marginTop: 16 }}>eLibrary</h2>
                  <p>Perpustakaan digital si kecil.</p>
                </div>
              </div>
            </div>
            <div
              className="child"
              style={{
                justifyContent: 'flex-start',
                paddingLeft: 16,
              }}
            >
              <div
                className={
                  'child-content' + (this.state.tab == 'shop' ? ' active' : '')
                }
                onClick={() => this.props.history.push('/shop')}
              >
                <div>
                  <img
                    height={80}
                    width={80}
                    src={require('../assets/navigation/shop.png')}
                  />
                </div>
                <div style={{ marginLeft: 16 }}>
                  <h2 style={{ marginTop: 16 }}>Shop</h2>
                  <p>Kebutuhan belajar dan bermain si kecil.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.tab === 'elibrary' && (
          <>
            <div className="Shop">
              <div style={{ backgroundColor: '#4bbfd1' }}>
                <div className="content">
                  <Link to="/elibrary">
                    <div className="title" style={{ cursor: 'pointer' }}>
                      <img
                        height={80}
                        width={80}
                        src={require('../assets/navigation/eLibrary.png')}
                      />
                      <h1 style={{ marginLeft: 16 }}>eLibrary</h1>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
            {this.props.dataELibrary.map((data, index) => (
              <div
                className="Section"
                style={
                  index % 2 === 0
                    ? { background: '#4bbfd1' }
                    : {
                        backgroundImage: `url(${require('../assets/pattern_yellow.jpg')})`,
                      }
                }
                key={data.data.id + data.data.slug}
              >
                <div className="section-child" style={{ margin: '0 88px' }}>
                  <div className="title-container">
                    <h2
                      className="title"
                      style={{ textShadow: '2px 2px #00798e' }}
                    >
                      {data.data.name}
                    </h2>
                    <Link
                      to={`/promolib?slug=${data.data.slug}&type=${
                        data.data.shop_id
                      }`}
                    >
                      Lihat Semua
                    </Link>
                  </div>
                  <Slider
                    data={data.listbuku}
                    arrowColor={'white'}
                    renderItem={val => <EBookCard key={val.id} item={val} />}
                  />
                </div>
              </div>
            ))}
            <div className="Shop">
              <div
                style={{
                  backgroundImage: `url(${require('../assets/eLibraryExtra.png')})`,
                  position: 'relative',
                  backgroundSize: 'cover',
                }}
              >
                <div className="content">
                  <div className="title">
                    <img
                      height={80}
                      width={80}
                      src={require('../assets/navigation/shop.png')}
                    />
                    <h1 style={{ marginLeft: 16 }}>Shop</h1>
                  </div>
                  <div style={{ display: 'flex' }}>
                    <div style={{ flex: 1 }}>
                      <h1 style={{ color: 'white', marginTop: 24 }}>
                        Buka Kemampuan Baru Si Kecil
                      </h1>
                      <p style={{ fontSize: 20, color: color.blue }}>
                        Berbagai pilihan produk untuk kebutuhan belajar dan
                        bermain si kecil.
                      </p>
                      <Link to="/shop">
                        <input
                          className="Submit-Button"
                          type="submit"
                          value="Belanja Sekarang"
                        />
                      </Link>
                    </div>
                  </div>
                  <img className="shop_images"
                    src={require('../assets/eLibraryExtraImg.png')}
                  />
                </div>
              </div>
            </div>
          </>
        )}
        {this.state.tab === 'shop' && (
          <>
            <div className="Shop">
              <div style={{ backgroundColor: 'white' }}>
                <div className="content grid">
                  <Link to="/shop">
                    <div className="title grid" style={{ cursor: 'pointer' }}>
                      <img
                        height={80}
                        width={80}
                        src={require('../assets/navigation/shop.png')}
                      />
                      <h1 style={{ marginLeft: 16 }}>Shop</h1>
                    </div>
                  </Link>

                  <div className="Grid">
                    <div className="grid-row">
                      {this.props.books.map((book, index) => {
                        return (
                          <div className="grid-item" key={book.id}>
                            <BookCard
                              key={index}
                              bookId={book.id}
                              image={
                                book.cover_image
                                  ? book.cover_image
                                  : require('../assets/placeholder.png')
                              }
                              price_1={book.regular_price}
                              price={book.sales_price}
                              title={book.name}
                              type={book.type}
                            />
                          </div>
                        );
                      })}
                    </div>
                  </div>
                  <div className="extra-content">
                    <Link to="/shop">
                      <button className="extra-button">Lihat Semua</button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="Shop">
              <div
                style={{
                  backgroundColor: color.lightBlue,
                }}
              >
                <div className="content">
                  <div className="title">
                    <img
                      height={80}
                      width={80}
                      src={require('../assets/navigation/eLibrary.png')}
                    />
                    <h1 style={{ marginLeft: 16 }}>eLibrary</h1>
                  </div>
                  <div style={{ display: 'flex' }}>
                    <div style={{ flex: 1 }}>
                      <h1 style={{ color: 'white', marginTop: 24 }}>
                        Teman tumbuh dan kembang si kecil
                      </h1>
                      <p style={{ fontSize: 20, color: color.blue }}>
                        Konten baik untuk si kecil, alternatif pintar selain gim
                        dan video
                      </p>
                      <ul
                        style={{
                          color: 'white',
                          fontWeight: 'bold',
                          paddingLeft: 16,
                          margin: 0,
                        }}
                      >
                        <li>Bayar 1x setiap bulan, akses sepuasnya.</li>
                        <li>Tersedia lebih dari 200 judul.</li>
                        <li>Pilih judul berdasarkan minat dan umur.</li>
                        <li>Konten terkurasi sehingga aman untuk si kecil.</li>
                      </ul>
                      {!this.props.userStore!.token && (
                        <Link to="/register">
                          <input
                            className="Submit-Button"
                            type="submit"
                            value="Belanja Sekarang"
                          />
                        </Link>
                      )}
                    </div>
                    <div
                      className="image-container"
                      style={{ flex: 1, position: 'relative' }}
                    >
                      <img
                        style={{
                          position: 'absolute',
                          height: '110%',
                          objectFit: 'contain',
                          bottom: 0,
                          transform: 'translateY(40px)',
                          right: 0,
                        }}
                        src={require('../assets/underconstruction/Asset 1.png')}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    );
  }
}

export default props => {
  return (
    <FetchComponent
      doBeforeRender={async () => {
        try {
          const eLibrary = client.get('/home/elibrary');
          const eShop = client.get('/home/eshop');
          const books = client.get(`/books`);
          const res = await Promise.all([eLibrary, eShop, books]);
          return {
            dataELibrary: res[0].data,
            data: res[1].data,
            books: res[2].data.data,
          };
        } catch (error) {
          throw error;
        }
      }}
    >
      <Index {...props} />
    </FetchComponent>
  );
};
