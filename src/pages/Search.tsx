import React, { Component } from 'react';
import '../style/Search.scss';
import { book } from '../libs/types';
import client from '../libs/client';
import EBookCard from '../components/EBookCard';
import qs from 'querystring';
import { RouteComponentProps } from 'react-router';

export default class Search extends Component<
  RouteComponentProps,
  { books: book[] }
> {
  constructor(props) {
    super(props);
    this.state = {
      books: [],
    };
  }
  async componentDidUpdate(prevProps){
    console.log(prevProps.location.search);
    var { q } = qs.parse(this.props.location.search.substr(1));
    // q = q.toString().replace(' ', '%20');
    q = q.toString().split(' ').join('%20');
    var q1 = prevProps.location.search.slice(3);
    q1 = q1.toString().split(' ').join('%20');
    console.log('cari12->',q);
    console.log('cari13->',q1);
    if(q != q1){
      console.log('update');
      try {
        var { q } = qs.parse(this.props.location.search.substr(1));
        q = q.toString().split(' ').join('%20');
        console.log('cari21->',q);
        const res = await client.get(`/bookelibrary?q=${q}`);
        this.setState({ books: res.data.data });
        // window.location.reload();
      } catch (error) {
        console.log(error.response.data);
      }
      window.location.reload();
    }
    console.log('cari2->',q);
    console.log('cari3->',q1);
    // if(JSON.stringify(prevState) !== JSON.stringify(this.state)) {

    // }
  }

  async componentDidMount() {
    try {
      const { q } = qs.parse(this.props.location.search.substr(1));
      console.log('cari->',q);
      const res = await client.get(`/bookelibrary?q=${q}`);
      this.setState({ books: res.data.data });
    } catch (error) {
      console.log(error.response.data);
    }
  }

  render() {
    return (
      <div className="Search-Container">
        <style>{`body, .App-Content { background-color: rgb(56, 193, 222);}`}</style>
        <div className="Search-Header">
          <span>{this.state.books.length} buku ditemukan</span>
        </div>
        <div className="Search-Content">
          <div className="content">
            {this.state.books.map(item => {
              return <EBookCard item={item} key={item.id + item.name} />;
            })}
          </div>
        </div>
      </div>
    );
  }
}
