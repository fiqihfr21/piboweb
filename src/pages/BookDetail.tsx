import React, { Component } from 'react';
import qs from 'querystring';
import Carousel from 'nuka-carousel';
import { bookdet, books } from '../libs/types';
import FetchComponent from '../components/FetchComponent';
import client from '../libs/client';
import { RouteComponentProps } from 'react-router';
import swal from 'sweetalert';
import { inject, observer } from 'mobx-react';
import CartStore from '../stores/CartStore';
import '../style/BookDetail.scss';
import Button from '../components/Button';
import Slider from '../components/Slider';
import BookCard from '../components/BookCard';
const color = require('../style/colors.scss');

@inject('cartStore')
@observer
class BookDetail extends Component<
  { book: bookdet; books: books; cartStore?: CartStore } & RouteComponentProps,
  { loading: boolean; wishLoading: boolean }
> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      wishLoading: false,
    };
  }

  addItem = async (book_id: number) => {
    try {
      await this.setState({ loading: true });
      // await addToCart(book_id);
      await this.props.cartStore!.addToCart(book_id);
      await this.setState({ loading: false });
    } catch (error) {
      const data = error.response;
      if (data.status === 401) {
        swal(
          'Perhatian!',
          'Silahkan login atau register terlebih dahulu!',
          'info'
        ).then(() => {
          this.props.history.push('/login');
        });
      }
    }
  };

  addWishlist = async (book_id: number) => {
    try {
      await this.setState({ wishLoading: true });
      const res = await client.get<{ exists: boolean }>(
        `/wishlists/${book_id}`
      );
      const { exists } = res.data;
      if (exists) {
        swal('Perhatian!', 'Buku sudah ada di wishlist', 'info');
      } else {
        await client.post('/wishlists', {
          book_id,
        });
        swal('Sukses', 'Buku berhasil ditambah ke wishlist', 'success');
      }
      this.setState({ wishLoading: false });
    } catch (error) {
      const data = error.response;
      if (data.status === 401) {
        swal(
          'Perhatian!',
          'Silahkan login atau register terlebih dahulu!',
          'info'
        ).then(() => {
          this.props.history.push('/login');
        });
      }
    }
  };

  render() {
    const book = this.props.book;
    return (
      <div className="Container">
        <div className="Header">
          <span>
            <a
              className="Link"
              onClick={() => this.props.history.push('/shop')}
            >
              <u>
                <b>Beranda</b>
              </u>
            </a>
            <span className="nav"> > </span>
            List Buku
            <span className="nav"> > </span>
            {book.name}
          </span>
        </div>
        <div className="Detail-Content">
          <div className="left-content">
            <Carousel width="300px"
              renderCenterLeftControls={({previousSlide, currentSlide}) => (
              currentSlide !== 0 ?
              <i onClick={previousSlide} className={'arrow left'}/>
              : ''
              )}
              renderCenterRightControls={({nextSlide, slideCount, currentSlide}) => (
              currentSlide !== slideCount - 1 ?
              <i onClick={nextSlide} className={'arrow right'}/>
              : ''
              )}
            >
              <img
                src={
                  book.cover_image
                    ? book.cover_image
                    : require('../assets/placeholder.png')
                }
              />
              {book.images.map(image => (
                <img src={image.image} />
              ))}
            </Carousel>
          </div>
          <div className="center">
            <h2>{book.name}</h2>
            {/* <h3 style={{ color: color.red }}>{book.type}</h3> */}
            <div className="section">
              <h4>Detail</h4>
              <div style={{ display: 'flex' }}>
                <div style={{ flex: 1 }}>
                  {/* <p>Halaman</p> */}
                  <p>Penulis</p>
                  <p>Ilustrator</p>
                  <p>Penerbit</p>
                </div>
                <div style={{ flex: 2 }}>
                  {/* <p>{book.page}</p> */}
                  <p>{book.writer}</p>
                  <p>{book.illustrator}</p>
                  <p>{book.publisher}</p>
                </div>
              </div>
            </div>
            <div className="section">
              <h4>Keterangan</h4>
              {/* <p>{book.long_description}</p> */}
              <div
                dangerouslySetInnerHTML={{ __html: book.long_description }}
              />
            </div>
          </div>
          <div className="right">
            <div className="content">
              <p>
                {/* Buku {book.type} {book.page} Halaman */}
                Harga
              </p>
              <h1>
                Rp.{' '}
                {parseFloat(
                  book.sales_price ? book.sales_price : book.regular_price
                ).toLocaleString()}
              </h1>

              <Button
                onClick={async () => {
                  // @ts-ignore
                  await swal({
                    title: 'Perhatian',
                    text:
                      'Apakah Anda yakin ingin menambahkan buku ke keranjang?',
                    buttons: {
                      cancel: 'Batal',

                      confirm: {
                        text: 'Lanjutkan',
                        value: 'ok',
                      },
                    },
                  }).then(async value => {
                    switch (value) {
                      case 'ok':
                        await this.addItem(book.id);
                        await this.props.cartStore!.getCount();
                        break;
                    }
                  });
                }}
              >
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: 20,
                  }}
                >
                  <div
                    className={
                      'Loading' + (this.state.loading ? ' loading' : '')
                    }
                  />
                  Tambah Ke Keranjang
                </div>
              </Button>
              <a
                style={{
                  border: `3px solid ${color.yellow}`,
                  borderRadius: '5px',
                  marginTop: '16px',
                  padding: '8px',
                  color: color.yellow,
                  cursor: 'pointer',
                  display: 'flex',
                  justifyContent: 'center',
                }}
                onClick={() => this.addWishlist(book.id)}
              >
                <div
                  className={
                    'Loading' + (this.state.wishLoading ? ' loading' : '')
                  }
                />
                Wishlist
              </a>
            </div>
          </div>
        </div>
        <div
          className="Section"
          style={{
            // backgroundImage: `url(${require('../assets/pattern_red.jpg')})`,
            backgroundColor: 'rgb(255, 239, 147)',
          }}
        >
          <div className="section-child">
            <div className="title-container">
              <h2 className="title">Produk Lainnya</h2>
            </div>
            <Slider
              data={this.props.books.data}
              renderItem={val => (
                <BookCard
                  key={val.id}
                  bookId={val.id}
                  image={
                    val.cover_image
                      ? val.cover_image
                      : '/static/placeholder.png'
                  }
                  //price={val.sales_price ? val.sales_price : val.regular_price}
                  //price_1 = ''
                  price = ''
                  price_1={val.sales_price ? val.sales_price : val.regular_price}
                  title={val.name}
                  type={val.type}
                />
              )}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default props => (
  <FetchComponent
    doBeforeRender={async () => {
      try {
        const { id } = qs.parse(props.location.search.substr(1));
        const data1 = client.get(`/book/${id}`);
        const data2 = client.get(`/books?page=1`);
        const res = await Promise.all([data1, data2]);
        return { book: res[0].data, books: res[1].data };
      } catch (error) {
        throw error;
      }
    }}
  >
    <BookDetail {...props} />
  </FetchComponent>
);
