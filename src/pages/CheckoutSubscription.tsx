import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import client from '../libs/client';
import '../style/CheckoutSubscription.scss';
import Button from '../components/Button';
const color = require('../style/colors.scss');
import swal from '@sweetalert/with-react';
import Voucher from '../components/Voucher';

export default class CheckoutSubscription extends Component<
  RouteComponentProps,
  {
    loading: boolean;
    coupon: string;
    totalPrice: number;
    checking: boolean;
  }
> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      coupon: '',
      totalPrice: 0,
      checking: false,
    };
    if (!props.location.state) {
      props.history.replace('/subscribe');
    }
  }

  subscribe = async () => {
    try {
      await this.setState({ loading: true });
      const { id } = this.props.location.state;
      const res = await client.post('/paketsubscribe/order', {
        subscribe_id: id,
        coupon_code: this.state.coupon,
        platform: 'web',
      });
      await this.setState({ loading: false });
      console.log(res.data);
      window.location = res.data.url;
    } catch (error) {
      console.log(error);
      if (error.response.status === 401 || error.response.status === 500) {
        swal({
          title: "Perhatian!",
          text: "Sesi anda telah berakhir, silahkan login lagi",
          icon: "warning",
          dangerMode: true,
        })
        .then(willDelete => {
          if (willDelete) {
            this.props.history.push('/login');
          }
        });
      }
      await this.setState({ loading: false });
    }
  };

  render() {
    const { name, regular_price } = this.props.location.state;
    const { totalPrice } = this.state;
    
    return (
      <div className="Container">
        <div className="Checkout-Content" style={{ minHeight: '55vh' }}>
          <div className="content-container">
            <div className="horizontal">
              <div className="section left">
                <div>
                  <div className="card">
                    <h1>Rincian Belanja</h1>
                    <h4>Silahkan cek kembali belanjaan Anda</h4>
                    <div className="Subscription-Item">
                      <p className="title">{name}</p>
                      <p className="type">Paket</p>
                      <p className="price">
                        Rp {parseFloat(regular_price).toLocaleString()}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="section right">
                <div style={{ width: '100%' }}>
                  <div className="card">
                    <div className="horizontal-section">
                      <div className="content">
                        <div className="horizontal">
                          <p>Subtotal</p>
                          <p style={{ color: color.blue }}>
                            Rp {parseFloat(regular_price).toLocaleString()}
                          </p>
                        </div>

                        <div className="horizontal">
                          <p>Voucher</p>
                          <p style={{ color: color.blue }}>
                            {this.state.coupon ? this.state.coupon : '-'}
                          </p>
                        </div>
                        <div className="horizontal">
                          <h3 style={{ color: color.yellow }}>Total</h3>
                          <h3 style={{ color: color.yellow }}>
                            {totalPrice === 0 ? `Rp ${parseFloat(regular_price).toLocaleString()}` : `Rp ${parseFloat(totalPrice.toString()).toLocaleString()}`}
                          </h3>
                        </div>
                      </div>
                    </div>
                    {this.state.coupon === '' && (
                      <>
                        <Button
                          onClick={() => {
                            swal({
                              buttons: false,
                              content: (
                                <Voucher
                                  cost={regular_price}
                                  setCoupon={(coupon, totalPrice) =>
                                    this.setState({ coupon, totalPrice })
                                  }
                                />
                              ),
                            });
                          }}
                        >
                          <div
                            style={{
                              display: 'flex',
                              justifyContent: 'center',
                            }}
                          >
                            Apply Voucher
                          </div>
                        </Button>
                        <div style={{ height: 16 }} />
                      </>
                    )}

                    <Button onClick={this.subscribe}>
                      <div
                        style={{
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <div
                          className={
                            'Loading' + (this.state.loading ? ' loading' : '')
                          }
                        />
                        Bayar
                      </div>
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
