import { book } from '../libs/types';

export default class Book implements book {
  id: number;
  name: string;
  long_description: string;
  type: string;
  regular_price: string;
  sales_price: string;
  sku: string;
  stock: number;
  sold: number;
  weight: number;
  length: number;
  width: number;
  height: number;
  writer: string;
  illustrator: string;
  publisher: string;
  isbn: string;
  page: number;
  read_time: number;
  orientation: string;
  trial_persentage: number;
  enable_review: number;
  author_id: number;
  wishlist: number;
  created_at: Date;
  updated_at: Date;
  tags: { id: number; name: string }[];
  category: import('../libs/types').category;
  cover_image: string;
  pdf_file: string;
}
