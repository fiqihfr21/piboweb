import React from 'react';
import './style/Nav.scss';
const color = require('./style/colors.scss');

function UnderMaintenance() {
    return (
        <React.Fragment>
            <div className="Nav-Container">
                <div className="Nav-Content">
                    <div className="logo-container" style={{ margin: '5px' }}>
                        <img src="./assets/logo.png" className="logo" />
                    </div>
                </div>
            </div>

            <div
                style={{
                    height: '100vh',
                    width: '100%',
                    display: 'flex',
                    position: 'fixed',
                    flexDirection: 'column',
                    backgroundColor: color.blue,
                    justifyContent: 'center',
                    alignItems: 'center',
                    top: 0,
                }}
            >
                <div style={{ borderBottom: '3px dashed #FFF', height: '200px', width: 'auto' }}>
                    <img
                        alt="logo"
                        src={require('./assets/register1.png')}
                        style={{ width: '100%', objectFit: 'contain' }}
                    />
                </div>
                <div style={{ fontFamily: 'Montserrat, sans-serif' }}>
                    <h2 style={{ textAlign: 'center', color: '#FFF', fontSize: '30px' }}>Kami Akan Segera Kembali</h2>
                    <h3 style={{ textAlign: 'center', color: '#FFF', fontSize: '25px' }}>dengan Fitur Baru</h3>
                </div>
            </div>
        </React.Fragment>
    )
}

export default UnderMaintenance

