import React from 'react';
import $ from 'jquery';
import { findDOMNode } from 'react-dom';
const color = require('../style/colors.scss');
import '../style/Slider.scss';

interface Props<T> {
  data: Array<T>;
  renderItem(item: T): React.ReactNode;
  arrowColor: string;
}

class Slider<T> extends React.Component<Props<T>> {
  static defaultProps = {
    arrowColor: color.yellow,
  };

  handleScroll = (event, handle) => {
    const el = findDOMNode(this.refs.toggle);
    event.preventDefault();
    let slide = 0;
    if (window.screen.availWidth >= 768) slide = 210;
    else slide = 160;
    $(el).animate(
      {
        scrollLeft: handle === 1 ? `-=${slide}px` : `+=${slide}px`,
      },
      'slow'
    );
  };
  render() {
    return (
      <div className="Slider-Container">
        <div
          className="Arrow-Button arrow-left"
          style={{ borderLeftColor: this.props.arrowColor }}
          onClick={e => this.handleScroll(e, 0)}
        />
        <div
          className="Arrow-Button arrow-right"
          style={{ borderRightColor: this.props.arrowColor }}
          onClick={e => this.handleScroll(e, 1)}
        />
        <div className="Slider-Content" ref="toggle">
          {this.props.data.map(val => this.props.renderItem(val))}
        </div>
      </div>
    );
  }
}

export default Slider;
