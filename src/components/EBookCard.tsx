import '../style/EBookCard.scss';
import React from 'react';
import { inject, observer } from 'mobx-react';
import ReaderStore from '../stores/ReaderStore';
import { book } from '../libs/types';
import showModal from '../libs/modal';
import swal from 'sweetalert';
import client from '../libs/client';
import $ from 'jquery';


const EBookCard = inject('readerStore')(
  observer((props: { item: book; readerStore?: ReaderStore }) => {
    const { item: val } = props;
    return (
      <div
        key={val.id + val.name + Math.random()}
        className="EBookCard"
        onClick={async () => {
          showModal({
            book: val,
            onClick: item => {
              props.readerStore!.read(item.pdf_file);
            },
            onWishlist: async (item) => {
              console.log(item.id);
              var book_id = item.id;
              try {
                const res = await client.get<{ exists: boolean }>(
                  `/wishlists/${item.id}`
                );
                const { exists } = res.data;
                if (exists) {
                  // swal('Perhatian!', 'Buku sudah ada di wishlist', 'info');
                } else {
                  await client.post('/wishlists', {
                    book_id,
                  });
                  $('#book1').addClass("button-wishlist-active")
                  // swal(
                  //   'Sukses',
                  //   'Buku berhasil ditambah ke wishlist',
                  //   'success'
                  // ).then(() => {
                  //   window.location.reload();
                  // });
                }
                // this.setState({ wishLoading: false });
              } catch (error) {
                const data = error;
                console.log('error',data);
                if(typeof(data) != undefined){
                  if (data.status === 401) {
                    swal(
                      'Perhatian!',
                      'Silahkan login atau register terlebih dahulu!',
                      'info'
                    ).then(() => {
                      this.props.history.push('/login');
                    });
                  }

                }
              }
              
            },
          });
        }}
      >
        <img
          src={
            val.cover_image
              ? val.cover_image
              : require('../assets/placeholder.png')
          }
        />
        <div className="detail">
          <p>{val.name.length >= 20 ? val.name.substring(0,20)+"..." : val.name}</p>
          {/* <p>{val.name}</p> */}
        </div>
      </div>
    );
  })
);

export default EBookCard;