import React from 'react';
import Select from 'react-select';

interface Props {
  placeholder?: string | '';
  value: any;
  options: Array<any>;
  handleChange(val: string): void;
  valueBy: string;
  disabled?: boolean;
}

export default class InputDropdown extends React.Component<Props> {
  render() {
    const options = this.props.options.map(val => {
      return { value: val[this.props.valueBy], label: val[this.props.valueBy] };
    });
    const selected = options.find(
      searchVal => searchVal.value === this.props.value
    );
    return (
      <Select
        isDisabled={
          this.props.disabled
            ? this.props.disabled
            : this.props.options.length === 0
        }
        placeholder={this.props.placeholder}
        value={selected == undefined ? this.props.placeholder : selected}
        onChange={selected => this.props.handleChange(selected.value)}
        options={options}
      />
    );
  }
}
