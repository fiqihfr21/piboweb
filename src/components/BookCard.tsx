import React from 'react';
import '../style/BookCard.scss';
import { withRouter, RouteComponentProps } from 'react-router';
import { inject, observer } from 'mobx-react';
import UserStore from '../stores/UserStore';
import CartStore from '../stores/CartStore';
import swal from 'sweetalert';

interface BookCardType {
  bookId: number;
  image: string;
  title: string;
  type: string;
  price: string;
  price_1: string;
  userStore?: UserStore;
  cartStore?: CartStore;
}

function BookCard(props: BookCardType & RouteComponentProps) {
  return (
    <div className="Book-Card">
      <div
        className="card-content"
        onClick={() => {
          props.history.push(`/book_detail?id=${props.bookId}`);
          window.location.reload();
        }}
      >
        <img src={props.image} />
        <div className="detail">
          <p className="book-title">
            {/* {props.title.length > 15
          ? props.title.substring(0, window.innerWidth > 425 ? 18 : 13) + '...'
          : props.title} */}
            {props.title}
          </p>
          {props.price ?
            <p className="price_1">
              IDR {parseFloat(props.price_1).toLocaleString()}
            </p>
            :
            ''
          }
          {props.price_1 ?
            <p className="price">
              IDR {parseFloat(props.price ? props.price : props.price_1).toLocaleString()}
            </p>
            :
            ''
          }
        </div>
      </div>
      <div className="button-container">
        <button
          onClick={async () => {
            if (!props.userStore!.token) {
              props.history.push('/login');
            } else {
              // @ts-ignore
              await swal({
                title: 'Perhatian',
                text: 'Apakah Anda yakin ingin menambahkan buku ke keranjang?',
                buttons: {
                  cancel: 'Batal',

                  confirm: {
                    text: 'Lanjutkan',
                    value: 'ok',
                  },
                },
              }).then(async value => {
                switch (value) {
                  case 'ok':
                    console.log('add to cart');
                    // await addToCart(props.bookId);
                    // await cart.getCount();
                    await props.cartStore!.addToCart(props.bookId);
                    break;
                }
              });
            }
          }}
        >
          Tambah Ke Keranjang
        </button>
      </div>
    </div>
  );
}

export default withRouter(inject('userStore', 'cartStore')(observer(BookCard)));
