import React, { useEffect, useState } from 'react';
import NukaCarousel from 'nuka-carousel';
import { Banner } from '../libs/types';
import '../style/Carousel.scss';
import { Link } from 'react-router-dom';
import { inject, Observer } from 'mobx-react';
import ReaderStore from '../stores/ReaderStore';
// import showModal from '../libs/modal';
// import Book from '../models/Book';

const useWindowSize = (): { width: number; height: number } => {
  const [windowSize, setWindowSize] = useState(() => ({
    width: window.innerWidth,
    height: window.innerHeight,
  }));

  useEffect(() => {
    const onResize = () =>
      setWindowSize({ width: window.innerWidth, height: window.innerHeight });
    window.addEventListener('resize', onResize);
    return () => window.removeEventListener('resize', onResize);
  });

  return windowSize;
};

const Carousel = inject('readerStore')(
  (props: { banners: Banner[]; readerStore?: ReaderStore }) => {
    const size = useWindowSize();
    const isDesktop = size.width > 800;
    const nukaProps =
      size.width > 820
        ? { animation: 'zoom', cellSpacing: -20, slideOffset: 50 }
        : { slideOffset: 50 };
    return (
      <div className="Carousel">
        <NukaCarousel
          autoplay={true}
          autoplayInterval={2000}
          wrapAround={true}
          initialSlideHeight={isDesktop ? 380 : 180}
          cellAlign="center"
          {...nukaProps}
        >
          {props.banners.map(val => {
            const Wrapper = ({ children }) =>
              val.urllink ? (
                val.is_pdf === 0 ? (
                  <a href={val.urllink}>{children}</a>
                ) : (
                  <Observer>
                    {() => (
                      <a
                        onClick={() => {
                          props.readerStore!.read(val.urllink!);
                          // const book = new Book();
                          // book.id = val.id;
                          // book.name = val.name;
                          // book.cover_image = val.image;
                          // book.pdf_file = val.urllink!;
                          // showModal({
                          //   book: book,
                          //   onClick: item => {
                          //     props.readerStore!.read(item.pdf_file);
                          //   },
                          // });
                        }}
                      >
                        {children}
                      </a>
                    )}
                  </Observer>
                )
              ) : (
                <Link
                  to={`/promo?slug=${val.slug}&type=${val.shop_id}&from=banner`}
                >
                  {children}
                </Link>
              );
            return (
              <div className="image-slide" key={val.id}>
                <Wrapper>
                  <div
                    className="image-slide-content"
                    style={{
                      backgroundImage: `url(${
                        isDesktop ? val.image : val.image_mobile
                      })`,
                    }}
                  />
                </Wrapper>
              </div>
            );
          })}
        </NukaCarousel>
      </div>
    );
  }
);

export default Carousel;
