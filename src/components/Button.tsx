import React from 'react';
const color = require('../style/colors.scss');
import '../style/Button.scss';

interface ButtonProps {
  style?: {
    backgroundColor?: string;
    shadowColor?: string;
    borderRadius?: string;
  };
  onClick(e): void;
  disabled?: boolean;
}

const Button: React.FunctionComponent<ButtonProps> = ({ style, ...props }) => {
  let styles = {};
  if (!style) {
    styles = {
      borderRadius: '8px',
      backgroundColor: color.yellow,
      boxShadow: `0 5px ${color.darkYellow}`,
    };
  } else {
    styles = {
      borderRadius: style!.borderRadius ? style!.borderRadius : '8px',
      backgroundColor: style!.backgroundColor
        ? style!.backgroundColor
        : color.yellow,
      boxShadow: `0 5px ${
        style!.shadowColor ? style!.shadowColor : color.darkYellow
      }`,
    };
  }
  
  return (
    <button className="Button" {...props} style={{ ...styles }}>
      {props.children}
    </button>
  );
};

export default Button;
