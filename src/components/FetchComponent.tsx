import React, { Component } from 'react';
import { observable, runInAction } from 'mobx';
import { observer } from 'mobx-react';
import Loading from './Loading';

@observer
export default class FetchComponent<S> extends Component<
  { doBeforeRender: () => Promise<S>; watch?: any },
  S
> {
  @observable loading = true;
  @observable error;

  componentWillReceiveProps(nextProps) {
    const { watch } = nextProps;
    if (watch !== this.props.watch) {
      runInAction(() => {
        this.loading = true;
      });
      this.fetchData(nextProps);
    }
  }

  componentDidMount() {
    this.fetchData(this.props);
  }

  async fetchData(props) {
    try {
      const state = await props.doBeforeRender();
      await this.setState({ ...state });
    } catch (error) {
      runInAction(() => {
        this.error = error;
      });
    } finally {
      runInAction(() => {
        this.loading = false;
      });
    }
  }

  render() {
    const { children } = this.props;
    const childrenWithProps = React.Children.map(children, child =>
      React.cloneElement(child as React.ReactElement, { ...this.state })
    );
    return this.loading || this.error ? (
      <Loading error={this.error} />
    ) : (
      childrenWithProps
    );
  }
}
