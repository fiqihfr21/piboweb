import * as React from 'react';
// import client from '../libs/client';
import { inject, Observer } from 'mobx-react';
import FooterStore from '../stores/FooterStore';
import { withRouter, RouteComponentProps } from 'react-router';

// const link = [
//   { name: 'Blog' },
//   { name: 'Tentang Kami' },
//   { name: 'Fitur' },
//   { name: 'Download' },
//   { name: 'Butuh Bantuan?' },
//   { name: 'Syarat dan Ketentuan' },
//   { name: 'Cara Pembayaran' },
// ];

const Footer = inject('footerStore')(
  (props: { footerStore?: FooterStore } & RouteComponentProps) => {
    React.useEffect(() => {
      props.footerStore!.getFooterData();
    }, []);

    return (
      <div className="App-Footer">
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Observer>
            {() =>
              props.footerStore!.data ? (
                <ul className="List">
                  <li className="List-Item">
                    <a
                      className="Link"
                      href='/about'
                    >
                      Tentang Kami
                    </a>
                  </li>
                  {props.footerStore!.data.map((val, key) => (
                    <li className="List-Item" key={val.id + ' ' + key}>
                        {val.slug === 'butuh-bantuan' && (
                          <a
                            className="Link"
                            href={"mailto:support@bacapibo.com"}
                          >
                            {val.name}
                          </a>
                        )}
                        {val.slug != 'butuh-bantuan' && val.slug != 'tentang-kami' && (
                          <a
                            className="Link"
                            onClick={() => {
                              props.history.push(`/page?q=${val.slug}`);
                            }}
                          >
                            {val.name}
                          </a>
                        )}
                    </li>
                  ))}
                </ul>
              ) : (
                <div />
              )
            }
          </Observer>
          <div
            style={{
              marginBottom: '10px',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            {/* <a className="Link">Ikuti Kami</a> */}
            <div>
              <a href={`https://www.facebook.com/bacapibo`}>
              <img
                className="Small-Image"
                src={require('../assets/facebook.png')}
              />
              </a>
              <a href={`https://www.instagram.com/bacapibo/`}>
              <img
                className="Small-Image"
                src={require('../assets/instagram.png')}
              />
              </a>
              {/* <img
                className="Small-Image"
                src={require('../assets/youtube.png')}
              /> */}
            </div>
          </div>
          <div
            style={{
              borderTop: '2px solid white',
              width: '100%',
              textAlign: 'center',
            }}
          >
            <div className="linkBawah">
              <span className="Link">Copyright © 2019 PT PIBO MEDIA ANAK, supported by </span><a className="Link" href={`https://www.digify.us`}>Digify</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
);

export default withRouter(Footer);
