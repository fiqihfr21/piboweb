import React, { Component, ReactNode } from 'react';
import client from '../libs/client';
import '../style/LoadMore.scss';
import Button from './Button';

interface State<T> {
  data: Array<T>;
  next: number;
  end: boolean;
  error: boolean;
  total: number;
  subscribe: string;
  isLoading: boolean;
}

interface Props<T> {
  url: string;
  container?: any;
  renderItems: (item: T) => ReactNode;
  LoaderButton?: any;
  query?: string;
  style?: React.CSSProperties;
}

const initialState = {
  data: [],
  next: 0,
  end: false,
  error: false,
  total: 0,
  subscribe: "",
  isLoading: true,
};

export default class LoadMoreOrder<T> extends Component<Props<T>, State<T>> {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
    };
  }

  componentDidMount() {
    try {
      this.fetchData();
    } catch (error) {
      console.log(error);
    }
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.url !== nextProps.url) {
      await this.setState({ ...initialState });
      this.fetchData();
    }
  }

  fetchData = async () => {
    try {
      await this.setState({ isLoading: true, error: false });
      let url = `${this.props.url}/${this.state.next}`;
      if (this.props.query) {
        url = `${this.props.url}/${this.state.next}?${this.props.query}`;
      }
      const res = await client.get(url);
      console.log(this.props.url, this.state);
      const { next, end, data, total, subscribe } = res.data;
      console.log('data', res.data);
      this.setState(prev => ({
        end,
        next,
        total,
        subscribe,
        data: [...prev.data, ...data],
        isLoading: false,
      }));
    } catch (error) {
      console.log(error.response);
      if (error.response.status === 404) {
        this.setState({ end: true });
        return;
      }
      this.setState({ error: true });
    }
  };

  render() {
    const DefaultContainer = props => <div>{props.children}</div>;
    const {
      container: Container = DefaultContainer,
      LoaderButton,
      style,
    } = this.props;
    return (
      <div className="Load-More" style={style}>
        {this.state.error ? (
          <div className="error">Oops, something wrong</div>
        ) : (
          <>
            <Container total={this.state.subscribe}>
              {this.state.data.map(item => this.props.renderItems(item))}
            </Container>
            <div className="loader-container">
              {!this.state.end &&
                (this.state.isLoading ? (
                  <span>Loading</span>
                ) : LoaderButton ? (
                  <LoaderButton onClick={this.fetchData}>
                    Load More
                  </LoaderButton>
                ) : (
                  <Button onClick={this.fetchData}>Load More</Button>
                ))}
            </div>
          </>
        )}
      </div>
    );
  }
}
