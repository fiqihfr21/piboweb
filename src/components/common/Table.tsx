import React, { Component } from 'react';
import './style/Table.scss';
import client from '../../libs/client';
// import swal from '@sweetalert/with-react';

interface Props {
    type: string | null;
    totalStudent?: number;
    totalChild?: number;
}

interface State {
    addContent : boolean;
    values: {
        name: string;
        email: string;
        password: string;
        class_name: string;
    }
}

export default class Table extends Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = {
            addContent: false,
            values : {
                name: '',
                email: '',
                password: '',
                class_name: ''
            }
        };
    }

    handleAddContent = () => {
        const { addContent } = this.state;
        this.setState({ addContent: !addContent});
    }

    render() {
        const { type, totalStudent, totalChild } = this.props;
        const { addContent } = this.state;
        
        return (
            <React.Fragment>
                <div className="title">
                    {type ? (
                        type === 'guru' ? (
                            totalStudent !== undefined && totalStudent < 30 ? (
                                <p onClick={this.handleAddContent}><strong>+ Tambah Murid</strong></p>
                            ) :(
                                <p><strong>Kelas Anda Telah Mencapai batas maksimal.</strong> Hubungi admin sekolah untuk menambah kuota</p>
                            )
                            
                        ) :
                        type === 'user' ? (
                            totalChild !== undefined && totalChild < 3 ? (
                                <p onClick={this.handleAddContent}><strong>+ Tambah Profil Anak</strong></p>
                            ) : (
                                <p><strong>Jumlah Profil Anak Telah Mencapai batas maksimal.</strong></p>
                            )
                        ) :
                        type === 'admin_sekolah' && (<p onClick={this.handleAddContent}><strong>+ Tambah Guru</strong></p>)
                    ) : (
                        <p><strong>Mohon Tunggu</strong></p>
                    )}
                    
                    
                    {addContent && (
                        type === 'guru' && totalStudent !== undefined && totalStudent < 30 ? ( <StudentForm /> ) :
                        type === 'user' && totalChild !== undefined && totalChild < 3 ? ( <FamilyForm /> ) :
                        type === 'admin_sekolah' && ( <ClassForm /> )
                    )}

                </div>
                <div className="Table">
                    <table style={{ width: '100%' }}>
                        <tbody>
                            { this.props.children }
                        </tbody>
                    </table>
                </div>
            </React.Fragment>
        )
    }
}

interface ClassState { values: { name: string; email: string; password: string; class_name: string; } }

class ClassForm extends Component<{}, ClassState> {
    constructor(props) {
        super(props);
        this.state = {
            values : {
                name: '',
                email: '',
                password: '',
                class_name: ''
            }
        };
    }

    handleOnChangeInput = (e) => {
        const { values } = { ...this.state };
        const currentState = values;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({ values: currentState });
    }

    handleSubmit = async () => {
        const { values } = this.state;
        console.log(values);
        try{
            const res = await client.post('/regist_guru', values);
            if(res) {
                location.reload();
            }
        } catch(err) {
            if(err) {
                console.log(err.response);
            }
        }
    }

    render() {
        const { values } = this.state;

        return (
            <div className="add-teacher-form">
                <input 
                    type="text"
                    name="name"
                    value={values.name}
                    onChange={e => this.handleOnChangeInput(e)}
                    placeholder="Nama Guru" 
                    required />

                <input
                    type="email"
                    name="email"
                    value={values.email}
                    onChange={e => this.handleOnChangeInput(e)}
                    placeholder="Email Guru"
                    required />
                <input
                    type="text"
                    name="password"
                    value={values.password}
                    onChange={e => this.handleOnChangeInput(e)}
                    placeholder="Password Guru"
                    required />

                <input
                    type="text"
                    name="class_name"
                    value={values.class_name}
                    onChange={e => this.handleOnChangeInput(e)}
                    placeholder="Nama Kelas Guru"
                    required />
                
                <div className="submit-button" onClick={this.handleSubmit}>Buat Kelas</div>
            </div>
                        
        )
    }
}

interface StudentState { values: { name: string; no_absen: string; } }

class StudentForm extends Component<{}, StudentState> {
    constructor(props) {
        super(props);
        this.state = {
            values : {
                name: '',
                no_absen: ''
            }
        };
    }

    handleOnChangeInput = (e) => {
        const { values } = { ...this.state };
        const currentState = values;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({ values: currentState });
    }

    handleSubmit = async () => {
        const { values } = this.state;
        console.log(values);
        try{
            const res = await client.post('/class/murid', values);
            if(res) {
                location.reload();
            }
        } catch(err) {
            if(err) {
                console.log(err.response);
            }
        }
    }

    render() {
        const { values } = this.state;

        return (
            <div className="add-teacher-form">
                <input 
                    type="text"
                    name="name"
                    value={values.name}
                    onChange={e => this.handleOnChangeInput(e)}
                    placeholder="Nama Anak" 
                    required />

                <input
                    type="text"
                    name="no_absen"
                    value={values.no_absen}
                    onChange={e => this.handleOnChangeInput(e)}
                    placeholder="No. Absen Murid"
                    required />
                
                <div className="submit-button" onClick={this.handleSubmit}>Buat Murid</div>
            </div>        
        )
    }
}

interface FamilyState { values: { name: string; usia: string; } }

class FamilyForm extends Component<{}, FamilyState> {
    constructor(props) {
        super(props);
        this.state = {
            values : {
                name: '',
                usia: ''
            }
        };
    }

    handleOnChangeInput = (e) => {
        const { values } = { ...this.state };
        const currentState = values;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({ values: currentState });
    }

    handleSubmit = async () => {
        const { values } = this.state;
        console.log(values);
        try{
            const res = await client.post('/regist_child', values);
            if(res) {
                location.reload();
            }
        } catch(err) {
            if(err) {
                console.log(err.response);
            }
        }
    }

    render() {
        const { values } = this.state;

        return (
            <div className="add-teacher-form">
                <input 
                    type="text"
                    name="name"
                    value={values.name}
                    onChange={e => this.handleOnChangeInput(e)}
                    placeholder="Nama Anak" 
                    required />

                <select name="usia" value={values.usia} onChange={e => this.handleOnChangeInput(e)}>
                    <option value="">Pilih Umur</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12/12+</option>
                </select>
                
                <div className="submit-button" onClick={this.handleSubmit}>Buat Akun Anak</div>
            </div>        
        )
    }
}
