import React, { Component } from 'react';
import swal from '@sweetalert/with-react';
import client from '../../../libs/client';

interface State {
  ref: any;
  showMenu: boolean;
}

interface Props {
  content: {
    user_id: number;
    name: string;
    usia: string;
    summary: {
      book_name: string;
      max_readpage: number;
      presentage: number
    }
  };
}

export default class Table extends Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            ref: React.createRef(),
            showMenu: false,
        };

        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    async componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }
  
    componentWillUnmount() {
      document.removeEventListener('mousedown', this.handleClickOutside);
    }
  
    handleClickOutside(event) {
      const { ref } = this.state
      if (ref && ref.current.contains(event.target)) {
        return;
      }
      this.setState({ showMenu: false });
    }

    showDeleteModal = () => {
      swal({
        icon: "warning",
        content: <DeleteModal content={this.props.content} />,
        buttons: true,
        dangerMode: true
      }).then(async (willDelete) => {
        if(willDelete) {
          const { user_id } = this.props.content;
          try {
            const res = await client.delete(`/family/${user_id}`);
            if(res) {
              swal("Murid Sudah Dihapus", {
                icon: "success",
                button: true
              }).then((reload) => {
                if(reload) {
                  location.reload();
                }
              })
            }
          } catch (error) {
            if(error) {
              swal("Terjadi kesalahan, Silahkan coba lagi", {
                icon: "error",
                button: true
              });
            }
          }
        }
      })
    }

    showEditModal = () => {
      swal({
        icon: "info",
        content: <EditModal content={this.props.content} />,
        button: false,
      })
    }

    render() {
        const { showMenu, ref } = this.state;
        const { user_id, name } = this.props.content;

        return (
            <React.Fragment>
                <tr key={user_id}>
                  <td>
                    <div style={{ display: 'flex', alignItems: 'center'}}>
                      <img style={{ width: '80px', height: 'auto' }} src={require('../../../assets/avatar_sm.png')} />
                      <div ref={ref} style={{ position: 'relative' }}>
                        <h2 className="content-item" style={{ display: 'block', color: '#1f3f72' }}>{ name }</h2>
                        <a style={{ color: '#38c1de', fontSize: '14px', cursor: 'pointer' }}
                           onClick={() => this.setState({ showMenu: true })}>opsi profil</a>
                        <div className={'show-more' + (showMenu ? ' show' : '')}>
                          <ul>
                            <li  onClick={this.showEditModal}>Ubah Profil</li>
                            <li onClick={this.showDeleteModal}>Hapus Profil</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">5</h2>
                        <p className="content-title">Buku Terbaca</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">1</h2>
                        <p className="content-title">Jam Baca</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">19/12/2019</h2>
                        <p className="content-title">Terakhir Aktif</p>
                      </div>
                  </td>
                </tr>
            </React.Fragment>
        )
    }
}

class DeleteModal extends Component<Props> {
  render() {
    const { name, usia } = this.props.content;

    return (
      <div className="modal-form">
        <h1>Perhatian</h1>
        <p>Apakah anda yakin ingin menghapus profile ini ?</p>

        <div>
          <p>Nama: {name}</p>
          <p>Usia: {usia}</p>
        </div>
      </div>
    )
  }
}

interface editState {
  name: string;
  usia: string;
}

class EditModal extends Component<Props, editState> {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.content.name,
      usia:  this.props.content.usia
    }
  }

  handleUpdateClass = async () => {
    const { user_id } = this.props.content;
    console.log(user_id);
    try {
      const res = await client.put(`/family/${user_id}`, this.state);
      if(res) {
        swal("Murid Sudah Diupdate", {
          icon: "success",
          button: true
        }).then((reload) => {
          if(reload) {
            location.reload();
          }
        })
      }
    } catch (error) {
      console.log(error.response)
      if(error) {
        swal("Terjadi kesalahan, Silahkan coba lagi", {
          icon: "error",
          button: true
        });
      }
    }
  }

  render() {
    const { name, usia } = this.state;

    return (
      <div className="modal-form">
        <h1>Info Anak</h1>
        <div>
          <p>Nama Anak</p>
          <input 
            type="text"
            name="class_name"
            value={name}
            onChange={e => this.setState({ name: e.target.value })}
            placeholder="Nama Anaj" 
            required />
          <p>Umur Anak</p>
          <input 
            type="text"
            name="class_name"
            value={usia}
            onChange={e => this.setState({ usia: e.target.value })}
            placeholder="Usia Anak" 
            required />

          <div style={{ display: 'flex', justifyContent: 'center', marginTop: '8px' }}>
            <div onClick={this.handleUpdateClass} className="submit-button">Update Anak</div>
          </div>
        </div>
      </div>
    )
  }
}

