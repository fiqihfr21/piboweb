import React, { Component } from 'react';
// import swal from '@sweetalert/with-react';
// import client from '../../../libs/client';

interface State {
  ref: any;
  showMenu: boolean;
}

interface Props {
  content: {
    id: number;
    book_name: string;
    book_page: number;
    max_readpage: number;
    max_readtime: number;
    percentage: string;
  };
}

export default class Table extends Component<Props, State> {
    render() {
        const { id, book_name, book_page, max_readpage, max_readtime, percentage } = this.props.content;
        console.log(book_name, book_page, max_readpage, max_readtime, percentage)
        return (
            <React.Fragment>
                <tr key={id}>
                  <td>
                    <div style={{ display: 'flex', alignItems: 'center'}}>
                      {/* <img style={{ width: '80px', height: 'auto' }} src={require('../../../assets/avatar.png')} /> */}
                      <div style={{ position: 'relative' }}>
                        <h4 className="content-item" style={{ display: 'block', color: '#1f3f72' }}>{ book_name }</h4>
                      </div>
                    </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ percentage }%</h2>
                        <p className="content-title">Buku Selesai</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ max_readtime } Detik</h2>
                        <p className="content-title">Waktu Baca</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ max_readpage }</h2>
                        <p className="content-title">Halaman Terbuka</p>
                      </div>
                  </td>
                </tr>
            </React.Fragment>
        )
    }
}

