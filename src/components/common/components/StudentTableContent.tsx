import React, { Component } from 'react';
import swal from '@sweetalert/with-react';
import client from '../../../libs/client';

interface State {
  ref: any;
  showMenu: boolean;
}

interface Props {
  content: {
    id: number;
    user_id: number;
    name: string;
    no_absen: string;
    token_murid: string;
    summary: any;
  };
}

export default class Table extends Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            ref: React.createRef(),
            showMenu: false,
        };

        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    async componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }
  
    componentWillUnmount() {
      document.removeEventListener('mousedown', this.handleClickOutside);
    }
  
    handleClickOutside(event) {
      const { ref } = this.state
      if (ref && ref.current.contains(event.target)) {
        return;
      }
      this.setState({ showMenu: false });
    }

    showDeleteModal = () => {
      swal({
        icon: "warning",
        content: <DeleteModal content={this.props.content} />,
        buttons: true,
        dangerMode: true
      }).then(async (willDelete) => {
        if(willDelete) {
          const { id } = this.props.content;
          try {
            const res = await client.delete(`/class/murid/${id}`);
            if(res) {
              swal("Murid Sudah Dihapus", {
                icon: "success",
                button: true
              }).then((reload) => {
                if(reload) {
                  location.reload();
                }
              })
            }
          } catch (error) {
            if(error) {
              swal("Terjadi kesalahan, Silahkan coba lagi", {
                icon: "error",
                button: true
              });
            }
          }
        }
      })
    }

    showEditModal = () => {
      swal({
        icon: "info",
        content: <EditModal content={this.props.content} />,
        button: false,
      })
    }

    render() {
        const { showMenu, ref } = this.state;
        const { name, no_absen, token_murid, summary } = this.props.content;

        return (
            <React.Fragment>
                <tr>
                  <td>
                    <div style={{ display: 'flex', alignItems: 'center'}}>
                      <img style={{ width: '80px', height: 'auto' }} src={require('../../../assets/avatar.png')} />
                      <div ref={ref} style={{ position: 'relative' }}>
                        <h2 className="content-item" style={{ display: 'block', color: '#1f3f72' }}>{ name }</h2>
                        <a style={{ color: '#38c1de', fontSize: '14px', cursor: 'pointer' }}
                           onClick={() => this.setState({ showMenu: true })}>opsi profil</a>
                        <div className={'show-more' + (showMenu ? ' show' : '')}>
                          <ul>
                            <li  onClick={this.showEditModal}>Edit Murid</li>
                            <li onClick={this.showDeleteModal}>Hapus Murid</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ token_murid }</h2>
                        <p className="content-title">Token Murid</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ no_absen }</h2>
                        <p className="content-title">Nomor Absen</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ summary.total_books }</h2>
                        <p className="content-title">Buku terbaca</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ summary.total_readtime }</h2>
                        <p className="content-title">Jam Membaca</p>
                      </div>
                  </td>
                </tr>
            </React.Fragment>
        )
    }
}

class DeleteModal extends Component<Props> {
  render() {
    const { name, no_absen, } = this.props.content;

    return (
      <div className="modal-form">
        <h1>Perhatian</h1>
        <p>Apakah anda yakin ingin menghapus profile ini ?</p>

        <div>
          <p>Nama: {name}</p>
          <p>Absen: {no_absen}</p>
        </div>
      </div>
    )
  }
}

interface editState {
  name: string;
  no_absen: string;
}

class EditModal extends Component<Props, editState> {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.content.name,
      no_absen:  this.props.content.no_absen
    }
  }

  handleUpdateClass = async () => {
    const { user_id } = this.props.content;
    console.log(user_id);
    try {
      const res = await client.put(`/class/murid/${user_id}`, this.state);
      if(res) {
        swal("Murid Sudah Diupdate", {
          icon: "success",
          button: true
        }).then((reload) => {
          if(reload) {
            location.reload();
          }
        })
      }
    } catch (error) {
      console.log(error.response)
      if(error) {
        swal("Terjadi kesalahan, Silahkan coba lagi", {
          icon: "error",
          button: true
        });
      }
    }
  }

  render() {
    const { name, no_absen } = this.state;

    return (
      <div className="modal-form">
        <h1>Info Kelas</h1>
        <div>
          <h3>{name}</h3>
          <p>Ubah Nama Murid</p>
          <input 
            type="text"
            name="class_name"
            value={name}
            onChange={e => this.setState({ name: e.target.value })}
            placeholder="Nama Guru" 
            required />
          <p>Ubah Nomor Absen</p>
          <input 
            type="text"
            name="class_name"
            value={no_absen}
            onChange={e => this.setState({ no_absen: e.target.value })}
            placeholder="Nama Guru" 
            required />

          <div style={{ display: 'flex', justifyContent: 'center', marginTop: '8px' }}>
            <div onClick={this.handleUpdateClass} className="submit-button">Update Kelas</div>
          </div>
        </div>
      </div>
    )
  }
}

