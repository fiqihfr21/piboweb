import React, { Component } from 'react';
import swal from '@sweetalert/with-react';
import client from '../../../libs/client';
import moment from 'moment';

interface State {
  ref: any;
  showMenu: boolean;
}

interface Props {
  content: {
    class_id: number;
    teacher_name: string;
    class_name: string;
    summary: any;
  };
}

export default class Table extends Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            ref: React.createRef(),
            showMenu: false,
        };

        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    async componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }
  
    componentWillUnmount() {
      document.removeEventListener('mousedown', this.handleClickOutside);
    }
  
    handleClickOutside(event) {
      const { ref } = this.state
      if (ref && ref.current.contains(event.target)) {
        return;
      }
      this.setState({ showMenu: false });
    }

    showDeleteModal = () => {
      swal({
        icon: "warning",
        content: <DeleteModal content={this.props.content} />,
        buttons: true,
        dangerMode: true
      }).then(async (willDelete) => {
        if(willDelete) {
          const { class_id } = this.props.content;
          try {
            const res = await client.delete(`/class/${class_id}`);
            if(res) {
              swal("Kelas Sudah Dihapus", {
                icon: "success",
                button: true
              }).then((reload) => {
                if(reload) {
                  location.reload();
                }
              })
            }
          } catch (error) {
            if(error) {
              swal("Terjadi kesalahan, Silahkan coba lagi", {
                icon: "error",
                button: true
              });
            }
          }
        }
      })
    }

    showEditModal = () => {
      swal({
        icon: "info",
        content: <EditModal content={this.props.content} />,
        button: false,
      })
    }

    render() {
        const { showMenu, ref } = this.state;
        const { teacher_name, class_name, summary } = this.props.content;

        return (
            <React.Fragment>
                <tr>
                  <td>
                    <div style={{ display: 'flex', alignItems: 'center'}}>
                      <img style={{ width: '80px', height: 'auto' }} src={require('../../../assets/avatar.png')} />
                      <div ref={ref} style={{ position: 'relative' }}>
                        <h2 className="content-item" style={{ display: 'block', color: '#1f3f72' }}>{ teacher_name }</h2>
                        <a style={{ color: '#38c1de', fontSize: '14px', cursor: 'pointer' }}
                           onClick={() => this.setState({ showMenu: true })}>opsi profil</a>
                        <div className={'show-more' + (showMenu ? ' show' : '')}>
                          <ul>
                            <li  onClick={this.showEditModal}>Ubah Kelas</li>
                            <li onClick={this.showDeleteModal}>Hapus Kelas</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ class_name }</h2>
                        <p className="content-title">Kelas</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ summary.total_books }</h2>
                        <p className="content-title">Buku terbaca</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{ summary.total_read_time }</h2>
                        <p className="content-title">Jam Membaca</p>
                      </div>
                  </td>
                  <td>
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <h2 className="content-item">{moment(summary.last_active).format('L')}</h2>
                        <p className="content-title">Terakhir aktif</p>
                      </div>
                  </td>
                </tr>
            </React.Fragment>
        )
    }
}

class DeleteModal extends Component<Props> {
  render() {
    const { teacher_name, class_name } = this.props.content;

    return (
      <div className="modal-form">
        <h1>Perhatian</h1>
        <p>Apakah anda yakin ingin menghapus profile ini ?</p>

        <div>
          <p>{teacher_name}</p>
          <p>{class_name}</p>
        </div>
      </div>
    )
  }
}

interface editState {
  teacher_name: string;
  class_name: string;
}

class EditModal extends Component<Props, editState> {
  constructor(props) {
    super(props);
    this.state = {
      teacher_name: this.props.content.teacher_name,
      class_name:  this.props.content.class_name
    }
  }

  handleUpdateClass = async () => {
    const { class_id } = this.props.content;
    const { class_name } = this.state;

    try {
      const res = await client.put(`/class/${class_id}`, { class_name });
      if(res) {
        swal("Kelas Sudah Diupdate", {
          icon: "success",
          button: true
        }).then((reload) => {
          if(reload) {
            location.reload();
          }
        })
      }
    } catch (error) {
      if(error) {
        swal("Terjadi kesalahan, Silahkan coba lagi", {
          icon: "error",
          button: true
        });
      }
    }
  }

  render() {
    const { teacher_name, class_name } = this.state;

    return (
      <div className="modal-form">
        <h1>Info Kelas</h1>
        <div>
          <h3>{teacher_name}</h3>
          <p>Ubah nama kelas</p>
          <input 
            type="text"
            name="class_name"
            value={class_name}
            onChange={e => this.setState({ class_name: e.target.value })}
            placeholder="Nama Guru" 
            required />

          <div style={{ display: 'flex', justifyContent: 'center', marginTop: '8px' }}>
            <div onClick={this.handleUpdateClass} className="submit-button">Update Kelas</div>
          </div>
        </div>
      </div>
    )
  }
}

