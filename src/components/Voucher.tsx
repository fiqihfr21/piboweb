import React, { useState } from 'react'
import Button from './Button';
import swal from '@sweetalert/with-react';
import client from '../libs/client';

const Voucher = ({ setCoupon, cost }) => {
  const [code, setCode] = useState('');
  const [checking, setChecking] = useState(false);
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <label>Kode voucher</label>
      <input
        className="Input"
        placeholder="Tulis kode voucher disini"
        onChange={e => setCode(e.target.value)}
      />
      <div style={{ height: 8 }} />
      <Button
        onClick={async () => {
          if (code !== '') {
            setChecking(true);
            try {
              const res = await client.post('/coupon/check', {
                coupon: code,
                shoptype: 'eLibrary',
                cost
              });

              console.log(res.data);

              setCoupon(code, res.data.new_total);
              swal('Sukses', 'Voucher berhasil dipakai', 'success');
            } catch (error) {
              const { status } = error.response.data;
              swal('Perhatian!', status, 'error');
            }
            setChecking(false);
          }
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <div className={checking ? 'loading' : ''} />
          Check
        </div>
      </Button>
    </div>
  );
};

export default Voucher;
