import React from 'react';
const color = require('../style/colors.scss');

export default function Loading({ error }: { error?: any }) {
  if (error)
    return (
      <div
        style={{
          height: '100%',
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          fontSize: '32px',
        }}
      >
        Something went wrong!
      </div>
    );
  return (
    <div
      style={{
        zIndex: 1000000,
        height: '100vh',
        width: '100%',
        display: 'flex',
        position: 'fixed',
        backgroundColor: color.blue,
        justifyContent: 'center',
        alignItems: 'center',
        top: 0,
      }}
    >
      <img
        alt="logo"
        src={require('../assets/logo.png')}
        style={{ width: '20%', objectFit: 'contain' }}
      />
    </div>
  );
}
