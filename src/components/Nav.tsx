import React, { useRef, useState, useEffect, createRef } from 'react';
import '../style/Nav.scss';
import { inject, observer, Observer } from 'mobx-react';
import { withRouter, RouteComponentProps, Link } from 'react-router-dom';
import UserStore from '../stores/UserStore';
import client from '../libs/client';
import CartStore from '../stores/CartStore';
import Button from './Button';
import ReactGA from 'react-ga';
import swal from '@sweetalert/with-react';

const color = require('../style/colors.scss');

const navRef = createRef<HTMLDivElement>();

const shopRoutes = ['/shop', '/books', '/book_detail', '/cart', '/checkout', '/promo'];
const elibraryRoutes = ['/elibrary', '/promolib'];
const elibraryPageRoutes = ['/elibrary'];
const elibraryProfileRoutes = ['/profile, /dashboard_murid'];
const searchRoutes = ['/search'];

const withSubHeader = [...shopRoutes, ...elibraryRoutes, ...searchRoutes];

const valueSearch = 'Cari Judul, Penulis, Kata Kunci...';

@inject('userStore')
@observer
class Nav extends React.Component<
  RouteComponentProps & { userStore?: UserStore },
  { shadow: boolean; search: string; extraMenu: boolean; showMenu: boolean, user : { type: string; } | null }
> {
  constructor(props) {
    super(props);
    this.state = {
      shadow: false,
      search: '',
      extraMenu: false,
      showMenu: false,
      user: null,
    };
  }

  async componentDidMount() {
    // window.onscroll = () => this.setState({ shadow: window.scrollY > 5 });
    window.onscroll = () => {
      if (window.scrollY > 5 && !this.state.shadow)
        this.setState({ shadow: true });
      else if (this.state.shadow && window.scrollY <= 5)
        this.setState({ shadow: false });
    };

    const user = await client.get('/me');
    this.setState({ user: user.data.type });
  }


  render() {
    // ReactGA.initialize('UA-45699569-1');
    ReactGA.initialize('UA-145597734-1');
    ReactGA.set({ page: window.location.pathname });
    ReactGA.pageview(window.location.pathname);
    const route = this.props.location.pathname;
    const { shadow } = this.state;
    var height = 0;
    var heights = "88px";
    if(navRef.current!){
      console.log('lewat');
      // console.log(navRef.current!.clientHeight);
      height = navRef.current!.clientHeight;
    }
    var ua = navigator.userAgent.toLowerCase(); 
    if (ua.indexOf('safari') != -1) { 
      if(withSubHeader.includes(route)){
        heights = '';
      }
    }
    return (
      <div
        className={'Nav-Container' + (shadow ? ' active' : '')}
        style={{
          height:
            withSubHeader.includes(route) && this.state.shadow
              ? `${height}px`
              : heights,
        }}
      >
        <div
          className="Nav-Content"
          style={{
            display:
              withSubHeader.includes(route) && this.state.shadow
                ? 'none'
                : 'flex',
          }}
        >
          <div className="logo-container">
            <a
              onClick={() => {
                this.props.history.push(`/`);
              }}
            >
              <img src="../assets/logo.png" className="logo" />
            </a>
            {/* <div className="Hover-Menu">
              <a href="/">Beranda</a>
              <a href="/elibrary">eLibrary</a>
              <a href="/shop">Shop</a>
            </div> */}
          </div>
          <div className="title-container">
            {/* {route == '/' && <p className="title">Inspiring Little Readers</p>} */}
            <span className={(route == '/about' ? 'active' : '')}
              onClick={() => {
                this.props.history.push(`/about`);
              }}
            >
              Tentang Kami
            </span>
            <span className={ (this.state.extraMenu ? 'active' : '') }
              onClick={() =>
                this.setState(prev => ({ extraMenu: !prev.extraMenu }))
              }
            >
              Layanan Kami
            </span>
            <span className={(route == '/subscribe' ? 'active' : '')}
              onClick={() => {
                this.props.history.push(`/subscribe`);
              }}
            >
              Langganan
            </span>
          </div>
          <div className="Animated-Menu">
            <div
              className={'Bars' + (this.state.showMenu ? ' open' : '')}
              onClick={() =>
                this.setState(prev => ({ showMenu: !prev.showMenu }))
              }
            >
              <span />
              <span />
              <span />
            </div>
          </div>
          <div className={'Menus' + (this.state.showMenu ? ' open' : '')}>
            <p
              onClick={() => {
                this.setState({ showMenu: false });
                this.props.history.push(`/about`);
              }}
            >
              Tentang Kami
            </p>
            <p
              onClick={() =>
                this.setState(prev => ({
                  extraMenu: !prev.extraMenu,
                  showMenu: false,
                }))
              }
            >
              Layanan Kami
            </p>
            <p
              onClick={() => {
                this.setState({ showMenu: false });
                this.props.history.push(`/subscribe`);
              }}
            >
              Langganan
            </p> 
          </div>
          <div className="content" style={{ justifyContent: 'flex-end' }}>
            {this.props.userStore!.token ? (
              <>
                {/* {containCartIcon.includes(route) && <Cart />} */}
                <User />
              </>
            ) : (
              <>
                {/* {route == '/' && (
                  <Link to="/" style={{ marginRight: 16 }}>
                    Tentang Kami
                  </Link>
                )} */}
                <a href="/login">
                  <div className="Anchor-Button">Login</div>
                </a>
              </>
            )}
          </div>
          <div
            className={
              'extra-hidden-menu' + (this.state.extraMenu ? ' active' : '')
            }
          >
            <div
              className="child"
              style={{
                justifyContent: 'flex-end',
                paddingRight: 16,
              }}
            >
              <div
                className="child-content"
                // onClick={() => this.props.history.push('/elibrary')}

                onClick={() => {
                  this.setState(prev => ({ extraMenu: !prev.extraMenu }));
                  this.props.history.push('/elibrary');}}
              >
                <img
                  height={50}
                  width={50}
                  src={require('../assets/navigation/eLibrary.png')}
                />
                <h2>eLibrary</h2>
              </div>
            </div>
            <div
              className="child"
              style={{
                justifyContent: 'flex-start',
                paddingLeft: 16,
              }}
            >
              <div
                className="child-content"
                // onClick={() => this.props.history.push('/shop')}
                onClick={() => { this.setState(prev => ({ extraMenu: !prev.extraMenu }));
                this.props.history.push('/shop');}}
              >
                <div>
                  <img
                    height={50}
                    width={50}
                    src={require('../assets/navigation/shop.png')}
                  />
                </div>
                <h2>Shop</h2>
              </div>
            </div>
            <div
              className="child"
              style={{
                paddingLeft: 16,
              }}
            >
              <div
                className="child-content-cancel"
                onClick={() =>
                  this.setState(prev => ({ extraMenu: !prev.extraMenu }))
                }
              >
                <div>
                  <img
                    height={32}
                    width={32}
                    src={require('../assets/cancel.png')}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        {/** SUB-HEADER */}
        {withSubHeader.includes(route) && (
          <div
            className="Sub-Header"
            style={{
              backgroundColor: shopRoutes.includes(route)
                ? color.yellow
                : color.lightBlue,
            }}
            ref={navRef}
          >
            <div className="title-container" style={{ marginLeft: 0 }}>
              {shopRoutes.includes(route) && (
                <>
                  <img
                    height={56}
                    src={require('../assets/navigation/shop.png')}
                    onClick={() => {
                      this.props.history.push('/shop');
                    }}
                  />
                  <div onClick={() => {
                    this.props.history.push('/shop');
                  }}>
                  <p className="title chewy" style={{ marginLeft: 16 }}>
                    Shop
                  </p>
                  </div>
                </>
              )}
              {elibraryRoutes.includes(route) && (
                <>
                  <img
                    height={56}
                    src={require('../assets/navigation/eLibrary.png')}
                    onClick={() => {
                      this.props.history.push('/elibrary');}}
                  />
                  {window.screen.width >= 700 && (
                    <div onClick={() => {
                      this.props.history.push('/elibrary');
                    }}>
                  <p className="title chewy" style={{ marginLeft: 16 }}>
                    eLibrary
                  </p></div>
                  )}
                </>
              )}
              {searchRoutes.includes(route) && (
                <>
                  <img
                    height={56}
                    src={require('../assets/navigation/eLibrary.png')}
                    onClick={() => {
                      this.props.history.push('/elibrary');}}
                  />
                  {window.screen.width >= 700 && (
                    <div onClick={() => {
                      this.props.history.push('/elibrary');
                    }}>
                  <p className="title chewy" style={{ marginLeft: 16 }}>
                    eLibrary
                  </p></div>
                  )}
                </>
              )}
            </div>
            {shopRoutes.includes(route) && (
              <div
                style={{
                  flex: 1,
                  display: 'flex',
                  justifyContent: 'flex-end',
                }}
              >
                {this.props.userStore!.token && <Cart />}
              </div>
            )}
            {elibraryRoutes.includes(route) && (
              <div
                style={{
                  flex: 1,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}
              >
                {window.screen.width >= 700 && (
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      background: 'white',
                      padding: '8px',
                      borderRadius: '4px',
                      marginRight: '20px',
                      height: '36px',
                    }}
                  >
                    <img
                      src={require('../assets/navigation/search.png')}
                      width={23}
                      height={20}
                    />

                    <input
                      onChange={e => this.setState({ search: e.target.value })}
                      onKeyPress={e => {
                        if (e.key === 'Enter') {
                          this.props.history.push(
                            `/search?q=${this.state.search}`
                          );
                          window.location.reload();
                        }
                      }}
                      value={this.state.search}
                      placeholder="Cari Judul, Penulis, Kata Kunci..."
                      style={{
                        border: 'none',
                        minWidth: '208px',
                        marginLeft: 8,
                      }}
                    />
                  </div>
                )}
                {window.screen.width < 700 && (
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      background: 'white',
                      padding: '8px',
                      borderRadius: '4px',
                      marginRight: '8px',
                      marginLeft: '10px',
                      height: '36px',
                    }}
                  >
                    <img
                      src={require('../assets/navigation/search.png')}
                      width={10}
                      height={10}
                    />

                    <input
                      onChange={e => this.setState({ search: e.target.value })}
                      onKeyPress={e => {
                        if (e.key === 'Enter') {
                          this.props.history.replace(`/search?q=${this.state.search}`
                          );
                          window.location.reload();
                        }
                      }}
                      value={this.state.search}
                      placeholder="Cari Judul, Penulis, Kata Kunci..."
                      style={{
                        border: 'none',
                        minWidth: '80px',
                        width : '100%',
                        marginLeft: 8,
                      }}
                    />
                  </div>
                )}
                {elibraryPageRoutes.includes(route) ? (
                  <Link to={`/elibrary`}>
                    <div className="Right-Icon" style={{ flexDirection: 'row', backgroundColor: '#1f3f72', padding: '20px 5px' }}>
                      <img
                        src={require('../assets/navigation/rocket.png')}
                        width={40}
                        height={30}
                        style={{ cursor: 'pointer', marginBottom: 8 }}
                      />
                      {window.screen.width >= 700 && (
                        <p className="item-title">Jelajah</p>
                      )}
                    </div>
                  </Link>
                ) : (
                  <Link to={`/elibrary`}>
                    <div className="Right-Icon" style={{ flexDirection: 'row', padding: '20px 5px' }}>
                      <img
                        src={require('../assets/navigation/rocket.png')}
                        width={40}
                        height={30}
                        style={{ cursor: 'pointer', marginBottom: 8 }}
                      />
                      {window.screen.width >= 700 && (
                        <p className="item-title">Jelajah</p>
                      )}
                    </div>
                  </Link>
                )}
                {/* {elibraryProfileRoutes.includes(route) ? (
                  <Link to="/profile">
                    <div className="Right-Icon" style={{ flexDirection: 'row', backgroundColor: '#1f3f72', padding: '20px 5px' }}>
                      <img
                        src={require('../assets/navigation/star.png')}
                        width={30}
                        height={30}
                        style={{ cursor: 'pointer', marginBottom: 8 }}
                      />
                      {window.screen.width >= 700 && (
                        <p className="item-title">Koleksiku</p>
                      )}
                    </div>
                  </Link>
                ):(
                  <Link to = "/profile">
                    <div className = "Right-Icon" style = {{ flexDirection: 'row', padding: '20px 5px' }}>
                          <img
                        src={require('../assets/navigation/star.png')}
                        width={30}
                        height={30}
                        style={{ cursor: 'pointer', marginBottom: 8 }}
                      />
                      {window.screen.width >= 700 && (
                        <p className="item-title">Koleksiku</p>
                      )}
                    </div>
                  </Link>
                )} */}
              </div>
            )}
            {searchRoutes.includes(route) && (
              <div
                style={{
                  flex: 1,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}
              >
                {window.screen.width >= 700 && (
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      background: 'white',
                      padding: '8px',
                      borderRadius: '4px',
                      marginRight: '20px',
                      height: '36px',
                    }}
                  >
                    <img
                      src={require('../assets/navigation/search.png')}
                      width={23}
                      height={20}
                    />

                    <input
                      onChange={e => this.setState({ search: e.target.value })}
                      onKeyPress={e => {
                        if (e.key === 'Enter') {
                          this.props.history.push(
                            `/search?q=${this.state.search}`
                          );
                        }
                      }}
                      value={this.state.search}
                      placeholder= {valueSearch}
                      style={{
                        border: 'none',
                        minWidth: '208px',
                        marginLeft: 8,
                      }}
                    />
                  </div>
                )}
                {window.screen.width < 700 && (
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      background: 'white',
                      padding: '8px',
                      borderRadius: '4px',
                      marginRight: '8px',
                      marginLeft: '10px',
                      height: '36px',
                    }}
                  >
                    <img
                      src={require('../assets/navigation/search.png')}
                      width={10}
                      height={10}
                    />

                    <input
                      onChange={e => this.setState({ search: e.target.value })}
                      onKeyPress={e => {
                        if (e.key === 'Enter') {
                          this.props.history.push(
                            `/search?q=${this.state.search}`
                          );
                        }
                      }}
                      value={this.state.search}
                      placeholder="Cari Judul, Penulis, Kata Kunci..."
                      style={{
                        border: 'none',
                        minWidth: '100px',
                        width : '100%',
                        marginLeft: 8,
                      }}
                    />
                  </div>
                )}
                {elibraryPageRoutes.includes(route) ? (
                  <Link to={`/elibrary`}>
                    <div className="Right-Icon" style={{ flexDirection: 'row', backgroundColor: '#1f3f72', padding: '20px 5px' }}>
                      <img
                        src={require('../assets/navigation/rocket.png')}
                        width={40}
                        height={30}
                        style={{ cursor: 'pointer', marginBottom: 8 }}
                      />
                      {window.screen.width >= 700 && (
                        <p className="item-title">Jelajah</p>
                      )}
                    </div>
                  </Link>
                ) : (
                    <Link to={`/elibrary`}>
                      <div className="Right-Icon" style={{ flexDirection: 'row', padding: '20px 5px' }}>
                        <img
                          src={require('../assets/navigation/rocket.png')}
                          width={40}
                          height={30}
                          style={{ cursor: 'pointer', marginBottom: 8 }}
                        />
                        {window.screen.width >= 700 && (
                          <p className="item-title">Jelajah</p>
                        )}
                      </div>
                    </Link>
                  )}
                {elibraryProfileRoutes.includes(route) ? (
                  <Link to="/profile">
                    <div className="Right-Icon" style={{ flexDirection: 'row', backgroundColor: '#1f3f72', padding: '20px 5px' }}>
                      <img
                        src={require('../assets/navigation/star.png')}
                        width={30}
                        height={30}
                        style={{ cursor: 'pointer', marginBottom: 8 }}
                      />
                      {window.screen.width >= 700 && (
                        <p className="item-title">Koleksiku</p>
                      )}
                    </div>
                  </Link>
                ):(
                  <Link to = "/profile">
                    <div className = "Right-Icon" style = {{ flexDirection: 'row', padding: '20px 5px' }}>
                          <img
                        src={require('../assets/navigation/star.png')}
                        width={30}
                        height={30}
                        style={{ cursor: 'pointer', marginBottom: 8 }}
                      />
                      {window.screen.width >= 700 && (
                        <p className="item-title">Koleksiku</p>
                      )}
                    </div>
                  </Link>
                )}
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

function useOnClickOutside(ref, handler) {
  React.useEffect(() => {
    const listener = event => {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }

      handler(event);
    };

    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, handler]);
}

const User = withRouter(
  inject('userStore')(
    (props: RouteComponentProps & { userStore?: UserStore }) => {
      const ref = useRef(null);
      const [user, setUser] = useState<{
        name: string;
        age: number;
        type: string;
        avatar: string;
        email: string;
        id: number;
      } | null>(null);

      // const [familyInfo, setFamilyInfo] = useState<{ 
      //   name: string, 
      //   email: string,
      //   child: Array<{name: string; user_id: number;}>
      // } | null >(null);

      const [showMenu, setModalOpen] = useState(false);
      useOnClickOutside(ref, () => setModalOpen(false));
      
      async function getUser() {
        try {
          const user = await client.get('/me');
          console.log(user.data);
          setUser(user.data);
          if(user.data.type === 'user') {
            localStorage.setItem('familyInfo', JSON.stringify({
              name: user.data.name, 
              email: user.data.email, 
              childs: user.data.family_info.child
            }));
          }
        } catch (error) {
          console.log(error);
        }
      }

      useEffect(() => {
        getUser();
      }, []);

      const switchToChildAcc = async (data) => {
        try {
          const res = await client.post('/login/family', {user_id: data});
          if(res) {
            const { access_token } = res.data;
            await props.userStore!.setToken(access_token);
            props.history.replace('/dashboard_anak');
            location.reload();
          }
        } catch (error) {
          console.log(error);
        }        
      }

      const switchToParrentAcc = async () => {
        const { name, email } = JSON.parse(localStorage.getItem('familyInfo') as string);

        swal({
          icon: "info",
          content: <FamilySignInModal {...props} content={{name, email}} />,
          button: false,
        })
      }

      return (
        <Observer>
          {() => (
            <div 
              style={{ background: `url(${
                user && user.avatar
                    ? user.avatar
                    : require('../assets/avatar_sm.png')
              }) center center / 100% 100% no-repeat` }}
              className="Right-Icon avatar-icon" onClick={() => setModalOpen(!showMenu)}>
              {/* <img
                className="Avatar-Image"
                src={
                  user && user.avatar
                    ? user.avatar
                    : require('../assets/avatar.png')
                }
                alt="avatar"
              /> */}
              <a className="item-title">{name}</a>
              <div
                ref={ref}
                className={'User-Menu-Container' + (showMenu ? ' show' : '')}
              >
                {user && console.log(user.type)}
                {user && (user.type === 'user' && (
                  <div>
                    {localStorage.getItem('familyInfo') && JSON.parse(localStorage.getItem('familyInfo') as string).childs.map(data => (
                      <div key={data.user_id} style={{ display: 'flex', alignItems: 'center' }} onClick={() => switchToChildAcc(data.user_id)}>
                        <img style={{ marginRight: '5px', height: 30, width: 30 }} src={ require('../assets/avatar_sm.png') }/>
                        <p>{data.name}</p>
                      </div>
                    ))}
                    
                    <Link
                      //@ts-ignore
                      to='/family'
                      style={{ display: 'flex', alignItems: 'center' }}
                      onClick={() => setModalOpen(false)}
                    >
                      
                      Dashboar Ortu
                    </Link>
                  </div>
                ))}

                {user && (user.type === 'anak' && (
                  <div>
                    {localStorage.getItem('familyInfo') && JSON.parse(localStorage.getItem('familyInfo') as string).childs.map(data => (
                      <div key={data.user_id} style={{ display: 'flex', alignItems: 'center' }} onClick={() => switchToChildAcc(data.user_id)}>
                        <img style={{ marginRight: '5px', height: 30, width: 30 }} src={require('../assets/avatar.png')}/>
                        <p>{data.name}</p>
                      </div>
                    ))}
                    
                    <a style={{ display: 'flex', alignItems: 'center' }} onClick={switchToParrentAcc}>Dashboar Ortu</a>
                  </div>
                ))}

                {user && user.type === 'admin_sekolah' && (
                  <Link
                    //@ts-ignore
                    to='/dashboard_admin_sekolah'
                    style={{ display: 'flex', alignItems: 'center' }}
                    onClick={() => setModalOpen(false)}
                  >
                    <img style={{ marginRight: '5px', height: 30, width: 30 }} src={require('../assets/profile.png')}/>
                    Masuk Ke Dashboard
                  </Link>
                )}

                {user && user.type === 'guru' && (
                  <Link
                    //@ts-ignore
                    to='/dashboard_guru'
                    style={{ display: 'flex', alignItems: 'center' }}
                    onClick={() => setModalOpen(false)}
                  >
                    <img style={{ marginRight: '5px', height: 30, width: 30 }} src={require('../assets/profile.png')}/>
                    Masuk Ke Dashboard
                  </Link>
                )}

                {user && user.type === 'murid' && (
                  <Link
                    //@ts-ignore
                    to='/dashboard_murid'
                    style={{ display: 'flex', alignItems: 'center' }}
                    onClick={() => setModalOpen(false)}
                  >
                    <img style={{ marginRight: '5px', height: 30, width: 30 }} src={require('../assets/profile.png')}/>
                    Masuk Ke Dashboard
                  </Link>
                )}

                <a
                  style={{ display: 'flex', alignItems: 'center' }}
                  onClick={async () => {
                    console.log('test');
                    setModalOpen(false);
                    await props.userStore!.clearToken();
                    props.history.replace('/');
                  }}
                >
                  <img
                    style={{ marginRight: '5px', height: 30, width: 30 }}
                    src={require('../assets/logout.png')}
                  />
                  Keluar
                </a>
              </div>
            </div>
          )}
        </Observer>
      );
    }
  )
);

const Cart = withRouter(
  inject('cartStore')(
    (props: RouteComponentProps & { cartStore?: CartStore }) => {
      const ref = useRef(null);
      const [showMenu, setModalOpen] = useState(false);
      useOnClickOutside(ref, () => setModalOpen(false));

      return (
        <div className="Right-Icon" onClick={() => setModalOpen(!showMenu)}>
          <img
            src={require('../assets/navigation/cart.png')}
            width={40}
            height={40}
            style={{ cursor: 'pointer' }}
          />
          <a className="item-title">Keranjang</a>
          {props.cartStore!.count !== 0 && (
            <p
              style={{
                position: 'absolute',
                top: 24,
                margin: 0,
                right: 24,
                transform: 'translate(50%, -50%)',
                borderRadius: '50%',
                background: color.red,
                height: 24,
                width: 24,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                fontSize: 12,
                color: 'white',
                fontWeight: 'bold',
              }}
            >
              {props.cartStore!.count}
            </p>
          )}
          <div
            ref={ref}
            className={'Cart-Item-Container' + (showMenu ? ' show' : ' hidden')}
          >
            {props.cartStore!.count === 0 ? (
              <div className="item" style={{ flex: 1 }}>
                <p style={{textAlign: "center",color: "rgb(75, 191, 209)",fontFamily:"Montserrat"}}>
                    Maaf, tidak ada produk di keranjang.<br /><br />Silahkan kembali belanja.
                </p>
              </div>
            ) : (
              <>
                <div style={{ flex: 1 }}>
                  {props.cartStore!.items.map((val, idx) => (
                    <div className="item" key={idx}>
                      <img
                        src={
                          val.cover_image
                            ? val.cover_image
                            : require('../assets/placeholder.png')
                        }
                      />
                      <p className="title">{val.name}</p>
                      <p className="price">
                        Rp.{' '}
                        {parseFloat(
                          val.sales_price ? val.sales_price : val.regular_price
                        ).toLocaleString()}{' '}
                        <span style={{ color: '#000' }}>x</span> {val.quantity}
                      </p>
                    </div>
                  ))}
                </div>
                <div className="item">
                  <p className="title" style={{ fontSize: 18 }}>
                    Total harga
                  </p>
                  <p className="price" style={{ fontSize: 18 }}>
                    Rp. {props.cartStore!.totalPrice.toLocaleString()}
                  </p>
                </div>
                <Button
                  onClick={() => {
                    props.history.push('/cart');
                  }}
                >
                  <p>Lihat keranjang</p>
                </Button>
              </>
            )}
          </div>
        </div>
      );
    }
  )
);

interface editState {
  name: string;
  email: string;
  password: string;
}

@inject('userStore')
class FamilySignInModal extends React.Component<
  RouteComponentProps & { userStore?: UserStore } & { 
    content: {name: string; email: string;}
  }, editState> {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.content.name,
      email:  this.props.content.email,
      password: ''
    }
  }

  handleUpdateClass = async () => {
    const { email, password } = this.state;

    try {
      const res = await client.post(`/login/`, {
        email,
        password
      });
      
      if(res) {
        const { access_token } = res.data;
        await this.props.userStore!.setToken(access_token);
        this.props.history.replace('/family');
        location.reload();
      }
    } catch (error) {
      console.log(error.response)
      if(error) {
        swal("Terjadi kesalahan, Silahkan coba lagi", {
          icon: "error",
          button: true
        });
      }
    }
  }

  render() {
    const { name, password } = this.state;

    return (
      <div className="modal-form">
        <h2>Masukkan Kata Sandi</h2>
        <div style={{ margin: "0px 10px" }}>
          <h3>{name}</h3>
          <input 
            type="password"
            name="password"
            value={password}
            onChange={e => this.setState({ password: e.target.value })}
            placeholder="Password Anda" 
            required />

          <div style={{ display: 'flex', justifyContent: 'center', marginTop: '8px' }}>
            <div onClick={this.handleUpdateClass} className="submit-button">Login</div>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(Nav);
