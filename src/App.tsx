import './style/App.scss';
import React, { Suspense, lazy, Component } from 'react';
import { hot } from 'react-hot-loader/root';
import {
  BrowserRouter,
  Route,
  Switch,
  RouteProps,
  Redirect,
  // Link,
} from 'react-router-dom';
import { runInAction, observable } from 'mobx';
import { Provider, inject, observer } from 'mobx-react';

import Nav from './components/Nav';
import Footer from './components/Footer';
import stores from './stores';
import UserStore from './stores/UserStore';
import Loading from './components/Loading';
import ReaderStore from './stores/ReaderStore';
import CartStore from './stores/CartStore';
import FetchComponent from './components/FetchComponent';

@inject('userStore')
@observer
class PrivateRoute extends React.Component<
  RouteProps & { userStore?: UserStore; component: React.ReactType }
> {
  @observable loading = true;

  async componentDidMount() {
    console.log('loading');
    await this.props.userStore!.readTokenFromLocal();
    runInAction(() => {
      this.loading = false;
    });
  }

  render() {
    const { component: Component, ...rest } = this.props;
    const { token } = this.props.userStore!;
    return this.loading ? (
      <div>Please wait...</div>
    ) : (
      <Route
        {...rest}
        render={props =>
          token ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: '/login',
                state: { from: props.location },
              }}
            />
          )
        }
      />
    );
  }
}

const ReaderModal = inject('readerStore')(
  observer((props: { readerStore?: ReaderStore }) => {
    return (
      <div
        className="Reader-Modal"
        style={{ display: props.readerStore!.active ? 'block' : 'none' }}
      >
        <span className="close" onClick={() => props.readerStore!.close()}>
          &times;
        </span>
        <iframe
          src={props.readerStore!.bookUrl}
          style={{ width: '100%', height: '100%' }}
          allowFullScreen
        />
      </div>
    );
  })
);

const Index = lazy(() => import('./pages/index'));
const Login = lazy(() => import('./pages/Login'));
const Register = lazy(() => import('./pages/Register'));
const Shop = lazy(() => import('./pages/Shop'));
const Cart = lazy(() => import('./pages/Cart'));
const Checkout = lazy(() => import('./pages/Checkout'));
const ELibrary = lazy(() => import('./pages/ELibrary'));
const Search = lazy(() => import('./pages/Search'));
const Subscribe = lazy(() => import('./pages/Subscribe'));
const CheckoutSubscription = lazy(() => import('./pages/CheckoutSubscription'));
const Profile = lazy(() => import('./pages/Profile'));
const ChooseProfile = lazy(() => import('./pages/ChooseProfile'));
const DashboardMurid = lazy(() => import('./pages/DashboardMurid'));
const DashboardAnak = lazy(() => import('./pages/DashboardAnak'));
const DashboardGuru = lazy(() => import('./pages/DashboardGuru'));
const DashboardAdminSekolah = lazy(() => import('./pages/DashboardAdminSekolah'));
const Promo = lazy(() => import('./pages/Promo'));
const PromoLib = lazy(() => import('./pages/Promo'));
const VerifyEmail = lazy(() => import('./pages/VerifyEmail'));
const ResetPassword = lazy(() => import('./pages/ResetPassword'));
const ChangePassword = lazy(() => import('./pages/ChangePassword'));
const BookDetail = lazy(() => import('./pages/BookDetail'));
const About = lazy(() => import('./pages/About'));
const Page = lazy(() => import('./pages/page'));
// const About = lazy(() => import('./pages/About'));
//import About from './pages/About';

// @inject('userStore', 'cartStore')
// @observer
class App extends Component {
  // async componentDidMount() {
  //   await this.props.userStore!.readTokenFromLocal();
  //   await this.props.cartStore!.getCount();
  // }

  render() {
    return (
      <>
        <Suspense fallback={<Loading />}>
          <BrowserRouter>
            <div className="App-Container">
              <Nav />
              <div className="App-Content">
                <Switch>
                  <Route
                    exact
                    path="/"
                    render={props => <Index {...props} />}
                  />
                  <Route
                    exact
                    path="/login"
                    render={props => <Login {...props} />}
                  />
                  <Route
                    exact
                    path="/register"
                    render={props => <Register {...props} />}
                  />
                  <Route
                    exact
                    path="/check_email"
                    render={() => (
                      <div className="Special-Content-Container">
                        <div className="Special-Content">
                          <h1>Terima kasih sudah mendaftar</h1><br /><br />
                          <h3><b>Email verifikasi akan dikirimkan ke email Anda.</b></h3>
                          <h3><b>Klik link verifikasi pada email tersebut agar dapat segera login.</b></h3>
                          {/* <Link to="/login">Klik disini untuk login</Link> */}
                        </div>
                      </div>
                    )}
                  />
                  <Route
                    exact
                    path="/finish"
                    render={() => (
                      <div className="Special-Content-Container-finish">
                        <div className="Special-Content-finish">
                          <h2>Terimakasih</h2>
                          <p>Transaksi Anda sedang kami proses.<br />Anda akan menerima email jika pembayaran telah terkonfirmasi.</p>
                          <p>Lanjut berjelajah di <a href="/elibrary"><b><u>PiBo eLibrary</u></b></a></p>
                          <p><i>Jika masih belum bisa mengakses penuh, mohon tunggu beberapa saat sampai pembayaran terkonfirmasi.</i></p>
                          <p>atau</p>
                          <p>Lanjut berbelanja di <a href="/shop"><b><u>PiBo Shop</u></b></a></p>
                        </div>
                      </div>
                    )}
                  />
                  <PrivateRoute
                    exact
                    path="/pilih_akun"
                    component={props => <ChooseProfile {...props} />}
                  />
                  <PrivateRoute
                    exact
                    path="/family"
                    component={props => <Profile {...props} />}
                  />
                  <PrivateRoute
                    exact
                    path="/dashboard_anak"
                    component={props => <DashboardAnak {...props} />}
                  />
                  <PrivateRoute
                    exact
                    path="/dashboard_murid"
                    component={props => <DashboardMurid {...props} />}
                  />
                  <PrivateRoute
                    exact
                    path="/dashboard_admin_sekolah"
                    component={props => <DashboardAdminSekolah {...props} />}
                  />
                  <PrivateRoute
                    exact
                    path="/dashboard_guru"
                    component={props => <DashboardGuru {...props} />}
                  />
                  <PrivateRoute
                    exact
                    path="/change-password"
                    component={props => <ChangePassword {...props} />}
                  />
                  <Route
                    exact
                    path="/shop"
                    render={props => <Shop {...props} />}
                  />
                  <PrivateRoute
                    exact
                    path="/cart"
                    component={props => <Cart {...props} />}
                  />
                  <PrivateRoute
                    exact
                    path="/checkout"
                    component={props => <Checkout {...props} />}
                  />
                  <Route
                    exact
                    path="/elibrary"
                    render={props => <ELibrary {...props} />}
                  />
                  <Route
                    exact
                    path="/search"
                    render={props => <Search {...props} />}
                  />
                  <Route
                    exact
                    path="/subscribe"
                    render={props => <Subscribe {...props} />}
                  />
                  <PrivateRoute
                    exact
                    path="/checkout-subscription"
                    component={props => <CheckoutSubscription {...props} />}
                  />
                  <Route
                    exact
                    path="/promo"
                    render={props => <Promo {...props} />}
                  />
                  <Route
                    exact
                    path="/promolib"
                    render={props => <PromoLib {...props} />}
                  />
                  <Route
                    exact
                    path="/verify-email"
                    render={props => <VerifyEmail {...props} />}
                  />
                  <Route
                    exact
                    path="/reset-password"
                    render={props => <ResetPassword {...props} />}
                  />
                  <Route
                    exact
                    path="/book_detail"
                    render={props => <BookDetail {...props} />}
                  />
                  <Route
                    exact
                    path="/about"
                    render={() => <About/>}
                  />
                  <Route
                    exact
                    path="/page"
                    render={props => <Page {...props} />}
                  />
                  <Route
                    path="*"
                    render={() => (
                      <div
                        style={{
                          height: '100%',
                          width: '100%',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                          fontSize: '32px',
                        }}
                      >
                        Page Not Found!
                      </div>
                    )}
                  />
                </Switch>
              </div>
              <Footer />
            </div>
          </BrowserRouter>
        </Suspense>
        <ReaderModal />
      </>
    );
  }
}

const Main = inject('userStore', 'cartStore')(
  observer((props: { userStore?: UserStore; cartStore?: CartStore }) => {
    return (
      <FetchComponent
        doBeforeRender={async () => {
          try {
            await props.userStore!.readTokenFromLocal();
            if (props.userStore!.token) await props.cartStore!.getCount();
            return {};
          } catch (error) {
            throw error;
          }
        }}
      >
        <App {...props} />
      </FetchComponent>
    );
  })
);

export default hot(() => (
  <Provider {...stores}>
    <Main />
  </Provider>
));
